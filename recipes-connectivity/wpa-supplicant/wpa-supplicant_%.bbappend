
SUMMARY = "Replacement recipe"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://murata_config/"

#FILES_${PN} += "/home/root/*"

do_configure_append () {
	#Enabling wi-fi direct. We need to uncomment line CONFIG_P2P=y from kernel config file
	sed -i s/#CONFIG_P2P=y/CONFIG_P2P=y/ wpa_supplicant/.config
}


do_install_append() {
    cp -r ${WORKDIR}/murata_config/* ${D}
}
