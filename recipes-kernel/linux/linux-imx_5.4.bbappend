# This file was derived from the linux-yocto-custom.bb recipe in
# oe-core.
#
# linux-yocto-custom.bb:
#
#   A yocto-bsp-generated kernel recipe that uses the linux-yocto and
#   oe-core kernel classes to apply a subset of yocto kernel
#   management to git managed kernel repositories.
#
# Warning:
#
#   Building this kernel without providing a defconfig or BSP
#   configuration will result in build or boot errors. This is not a
#   bug.
#
# Notes:
#
#   patches: patches can be merged into to the source git tree itself,
#            added via the SRC_URI, or controlled via a BSP
#            configuration.
#
#   example configuration addition:
#            SRC_URI += "file://smp.cfg"
#   example patch addition:
#            SRC_URI += "file://0001-linux-version-tweak.patch
#   example feature addition:
#            SRC_URI += "file://feature.scc"
#

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://jack_defconfig"
SRC_URI += "file://0001-DTS-support-for-Jack-audio-board.patch"
SRC_URI += "file://0003-PF1550-PMIC-support.patch"
SRC_URI += "file://0004-Add-support-for-DA7218-Audio-CODEC.patch"
SRC_URI += "file://0005-Add-JACk-dts-and-defconfig.patch"
SRC_URI += "file://0006-Charger-Support.patch"
  
do_copy_defconfig_append () {
       cp ${WORKDIR}/jack_defconfig ${WORKDIR}/defconfig          
       cp ${WORKDIR}/jack_defconfig ${WORKDIR}/build/.config
}


