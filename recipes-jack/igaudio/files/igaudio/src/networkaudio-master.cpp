/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"
#include "timer.h"
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/errqueue.h>
#include <linux/sockios.h>
#include <sys/ioctl.h>
#include <linux/socket.h>
#include <linux/types.h>

using namespace std;

/*
    Open the socket connection to the given remote IP.
    Sets nA->network.socket_fd (the socket filehandle) and
    nA->network.server_address (the IP address, as a 32 bit int).
*/    
int master_open_socket(network_audio_t *nA)
{
    struct hostent *slave_hostinfo, *pc_hostinfo;
    int sockfd, sockpcfd;

    // lookup hostname/IP info
    slave_hostinfo = gethostbyname(nA->network.remote_ip);
    if (slave_hostinfo == NULL)
    {
        DEBUG_LOG(ERROR, MASTER_HOST_LOOKUP, "No such host: %s", nA->network.remote_ip);        
        exit(0);
    }

    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if( sockfd < 0) 
    {
        DEBUG_LOG(ERROR, MASTER_SOCKET_ERROR, "Error opening socket: %s", strerror(errno));
        close(sockfd);
    }

    struct sockaddr_in *slave_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
    bzero((char *) slave_addr, sizeof(slave_addr));
    slave_addr->sin_family = AF_INET;
    slave_addr->sin_port = htons(nA->network.port_number);
    bcopy((char *)slave_hostinfo->h_addr, (char *)&(slave_addr->sin_addr.s_addr), slave_hostinfo->h_length);

    // set up the message header for sendmsg()
    nA->network.message_header = get_message_header();
    nA->network.message_header->msg_name = (void*)slave_addr;
    nA->network.message_header->msg_namelen = sizeof(struct sockaddr_in);

    int x = fcntl(sockfd, F_GETFL, 0);          // Get socket flags
    fcntl(sockfd,F_SETFL, x | O_NONBLOCK);   // Add non-blocking flag    
    enable_timestamping_high_priority(sockfd, 1);

    /* double socket workaround */
    /*if(nA->network.using_broadcast) 
    {
        pc_hostinfo = gethostbyname("192.168.0.13");
        sockpcfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        struct sockaddr_in *pc_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
        bzero((char *) pc_addr, sizeof(pc_addr));
        pc_addr->sin_family = AF_INET;
        pc_addr->sin_port = htons(nA->network.port_number);
        bcopy((char *)pc_hostinfo->h_addr, (char *)&(pc_addr->sin_addr.s_addr), pc_hostinfo->h_length);

        x = fcntl(sockpcfd, F_GETFL, 0);          // Get socket flags
        fcntl(sockpcfd,F_SETFL, x | O_NONBLOCK);   // Add non-blocking flag    

        nA->network.pc_address = *pc_addr;
        nA->network.socket_pc_fd = sockpcfd;
    }*/

    /* "real" broadcast mode */
    if(nA->network.using_broadcast)
    {
        // Enable broadcast
        int using_broadcast = 1;
        int disable_udp_checksum = 1;
        setsockopt( sockfd,			        // socket affected
                    SOL_SOCKET,				//
                    SO_BROADCAST,			// name of option
                    &using_broadcast,			//
                    sizeof(using_broadcast) );
        // Disable UDP checksum
        setsockopt( sockfd,			        // socket affected
                    SOL_SOCKET,				//
                    SO_NO_CHECK,			// name of option
                    &disable_udp_checksum,	//
                    sizeof(disable_udp_checksum) );
        DEBUG_LOG(STATUS, MASTER_SOCKET_BROADCAST, "Master socket enabled for broadcast");        
    }
     
    DEBUG_LOG(STATUS, MASTER_SOCKET_OK, "Master socket opened");
    nA->network.remote_address = *slave_addr;
    nA->network.socket_fd = sockfd;   
    return 1;
}

static int sock_flags = 0;// MSG_CONFIRM | MSG_DONTROUTE ;

int transmit_raw_packet(network_audio_t *nA, unsigned char* txbuf, unsigned int txbuf_len, int to_pc) {
    int result = 0; 
    if(!to_pc)
        result = sendto(nA->network.socket_fd, txbuf, txbuf_len, sock_flags, (struct sockaddr*)&(nA->network.remote_address), sizeof(struct sockaddr));
    else
        result = sendto(nA->network.socket_fd, txbuf, txbuf_len, sock_flags, (struct sockaddr*)&(nA->network.pc_address), sizeof(struct sockaddr));
    if(result > 0) 
    {
        nA->stats.packets_sent++;
        return 1;
    }


    DEBUG_LOG(EVENT, SOCK_UNAVAIL, "Send failed: %d / %s", result, strerror(errno));
    nA->stats.network_send_errors++;
    return 0;
}

void *master_thread(void *ptr) 
{
    network_audio_t *nA = (network_audio_t *) ptr;
    nA->packet_number = 1;
    master_open_socket(nA);

    double loop_time, audio_time, transmit_time;

    unsigned audio_size_bytes = alsa_period_buffer_size_bytes(nA);
    unsigned tx_len = RAW_HDR + audio_size_bytes;
    unsigned char *tx_buffer = (unsigned char*)malloc(tx_len);
    unsigned char *btx_buffer = (unsigned char*)malloc(tx_len * 2);

    AudioPacketHeader *pkt = (AudioPacketHeader*)tx_buffer;
    pkt->magic_number = IA_MSG_ID;
    pkt->data_size = audio_size_bytes;
    pkt->packet_length = tx_len;
    pkt->packet_number = nA->packet_number;

    buffer_type *frame_buffer = (buffer_type*)(tx_buffer + RAW_HDR);

    struct timespec wait_time, loop_start_time;
#ifdef DEBUG
    struct timespec packet_interval = { 0 };
#endif

    int err = 0, first = 1;
    snd_pcm_state_t state;
    snd_pcm_sframes_t avail;
    snd_pcm_uframes_t frames_copied = 0;
    unsigned char *frame_buffer_2 = (unsigned char*)frame_buffer;

    clock_gettime(CLOCK_MONOTONIC, &loop_start_time);

    while(nA->running)
    {
        T_START(loop_time);

        T_START(audio_time);
        
        // check PCM state is OK
        state = snd_pcm_state(nA->audio.pcm_handle);
        //DEBUG_LOG(EVENT, PCM_STATE, "%d", state);
        if(state == SND_PCM_STATE_XRUN || state == SND_PCM_STATE_SUSPENDED) 
        {
            nA->stats.pcm_failures++;
            err = alsa_xrun_recovery(nA->audio.pcm_handle, err);
        }

        // now check frames available in buffer
        avail = snd_pcm_avail_update(nA->audio.pcm_handle);
        //DEBUG_LOG(EVENT, AVAIL_UPDATE, "%d", avail);
        if(avail < 0) 
        {
            // error...
            nA->stats.pcm_failures++;
            err = alsa_xrun_recovery(nA->audio.pcm_handle, avail);
            if(err < 0) 
            {
                fprintf(stderr, "avail_update: %s\n", snd_strerror(err));
            }
            first = 1;
            continue;
        }
        // check if there are enough frames to capture and send in a new packet
        if(avail < (int)nA->audio.alsa_period_size) 
        {
            if(first) 
            {
                first = 0;
                err = snd_pcm_start(nA->audio.pcm_handle);
                if(err < 0) 
                {
                    fprintf(stderr, "start error: %s\n", snd_strerror(err));
                    exit(-1);
                }
            } 
            else 
            {
                clock_gettime(CLOCK_MONOTONIC, &wait_time);
                // wait for enough frames to accumulate
                err = snd_pcm_wait(nA->audio.pcm_handle, -1);
                //DEBUG_LOG(EVENT, WAIT_TIME, "%ld us", timer_end(wait_time));
                if(err < 0) 
                {
                    if((err = alsa_xrun_recovery(nA->audio.pcm_handle, err)) < 0) 
                    {
                        fprintf(stderr, "pcm_wait: %s\n", snd_strerror(err));
                        exit(-1);
                    }
                    first = 1;
                }
            }
            continue;
        }

        // capture into the packet buffer
        err = snd_pcm_readi(nA->audio.pcm_handle, frame_buffer_2, nA->audio.alsa_period_size);

        if(err != (int)nA->audio.alsa_period_size)
        {
            DEBUG_LOG(EVENT, READI, "err = %d/%d", err, (int)nA->audio.alsa_period_size);
            nA->stats.record_alsa_errors++;
        } 
        else
        {
            frames_copied = err;
        }

        T_END(audio_time);                
        update_average_tracker(&(nA->stats.record_time), audio_time);

        if(frames_copied == nA->audio.alsa_period_size) 
        {
            if(nA->debug.test_mode) 
                nA->debug.test_signal(frame_buffer, nA->audio.alsa_period_size);   

            T_START(transmit_time);
            // send packet and update stats
            if(transmit_raw_packet(nA, tx_buffer, pkt->packet_length, 0)) 
            {
#ifdef DEBUG
                long gap = timer_end(packet_interval);
                DEBUG_LOG(EVENT, SENT, "packet %d @ %ld us", nA->packet_number - 1, gap);
                if(gap > 10000 && nA->packet_number > 0)
                    fprintf(stderr, "*** %d @ %ld\n", nA->packet_number - 1, gap);
                clock_gettime(CLOCK_MONOTONIC, &packet_interval);
#endif
            }
            T_END(transmit_time);
            update_average_tracker(&(nA->stats.transmit_time), transmit_time);

            if(nA->network.using_broadcast) 
            {
                if(nA->packet_number % 2 == 0) 
                {
                    // copy current packet into double packet buffer and then 
                    // send both of them to the PC app
                    memcpy(btx_buffer + tx_len, tx_buffer, tx_len);
                    transmit_raw_packet(nA, btx_buffer, tx_len * 2, 1);
                } 
                else
                {
                    // copy current packet into double packet buffer for future
                    // transmission
                    memcpy(btx_buffer, tx_buffer, tx_len);
                }
            }

            // update these even if packet sending failed, makes things a bit
            // easier on the slave end if the packet numbering is linked to the
            // timing of the packets rather than how many of them have been
            // successfully sent
            nA->frame_number++;
            pkt->packet_number++;
            nA->packet_number++;
        }
        
        T_END(loop_time);
        update_average_tracker(&(nA->stats.master_loop_time), loop_time);
    }

    free(tx_buffer);
    //close(nA->network.socket_fd);
    fprintf(stderr, "MASTER THREAD EXITING\n");
	return NULL;
}

