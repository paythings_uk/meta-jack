/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <sys/fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "i2c-dev.h"

#include "networkaudio-i2c.h"

int open_i2c_device(void) {
    int err, fd;
    long param;
    
    fd = open("/dev/i2c-0", O_RDWR);
    if(fd < 0) {
        fprintf(stderr, "Failed to open /dev/i2c-0 (kernel CONFIG_I2C_CHARDEV support missing?)\n");
        return -1;
    }

    // activate 10-bit addressing mode
    param = 1;
    err = ioctl(fd, I2C_TENBIT, param);
    if(err < 0) {
        close(fd);
        fprintf(stderr, "Failed to set 10-bit addressing: %s\n", strerror(errno));
        exit(0);
    }

    // set the I2C chip address (_FORCE means it'll work even
    // if the device is already "in use")
    param = 0x94;
    err = ioctl(fd, I2C_SLAVE_FORCE, param);
    if(err < 0) {
        close(fd);
        fprintf(stderr, "Failed to set I2C slave address: %s\n", strerror(errno));
        exit(0);
    }
    return fd;
}

int read_i2c_register(int fd, unsigned short reg) {
    return i2c_smbus_read_byte_data(fd, reg);
}

int write_i2c_register(int fd, unsigned short reg, unsigned char val) {
    return i2c_smbus_write_byte_data(fd, reg, val);
}

int close_i2c_device(int fd) {
    return close(fd);
}
