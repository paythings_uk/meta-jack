/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef _NETWORKAUDIO_I2C_H_
#define _NETWORKAUDIO_I2C_H_

int open_i2c_device(void);
int read_i2c_register(int fd, unsigned short reg);
int write_i2c_register(int fd, unsigned short reg, unsigned char val);
int close_i2c_device(int fd);

#endif
