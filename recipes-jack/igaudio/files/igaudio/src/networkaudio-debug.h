/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef NETWORKAUDIO_DEBUG_H
#define NETWORKAUDIO_DEBUG_H

typedef struct networkaudio_debug_entry 
{
    unsigned packets_received;
    unsigned last_packet;
    unsigned last_packet_gap;
    unsigned underruns; 
    unsigned short queue_latency;
    unsigned short audio_state;
    unsigned rms;
    unsigned short cur_buffers;
    unsigned short reduce_to_buffers;
    unsigned short buffers_added;
    unsigned short buffers_removed;
    long last_glitch_time;
} networkaudio_debug_entry;

#endif
