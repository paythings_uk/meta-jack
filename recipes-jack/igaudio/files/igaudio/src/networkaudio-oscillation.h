/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef OSCILLATION_H
#define OSCILLATION_H

#define MAX_OSC_LEN 5000
#include "networkaudio-ringbuffer.h"
#include "networkaudio-buffer.h"

class Oscillation
{
public:
    Oscillation(int buffer_size);
    ~Oscillation();

    RingBuffer *pos_zc, *neg_zc;
    buffer_type *osc1;
    buffer_type *osc2;

    buffer_type *last_full_osc ;
    buffer_type *cur_osc ;

    int prevBufferNo;
    int len_last_full_osc;
    int len_cur_osc;
    int bufferSize;
    bool discardCurOsc; // used for when an oscillation is interrupted. ie missing buffer. then throw away

    buffer_type prev_val;

    bool curIsOsc1 ;

    void AddBuffer(buffer_type *new_buff, int buffer_no);
    void AddZeroCrossing(int bufferNo, int bufferPos, bool isPos);

    long GetPosVariance();
    long GetNegVariance();

    int GetPrevOsc(buffer_type val, float deriv, buffer_type *prev_osc);

private:
    int sgn(buffer_type v);
};

#endif // OSCILLATION_H
