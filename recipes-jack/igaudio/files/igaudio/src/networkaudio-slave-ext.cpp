/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"

int put_received_packet(network_audio_t *nA, AudioPacketHeader *header, buffer_type *data) {
    return push_packetbuffer(nA->packetbuf, data, nA->audio.alsa_period_size, header->packet_number, 0, 0);           
}

int drop_packet(network_audio_t *nA) {
    return drop_packetbuffer(nA->packetbuf);           
}

int get_next_packet(network_audio_t *nA, buffer_type *buffer, unsigned *packet_number) {
    return pop_packetbuffer(nA->packetbuf, buffer, nA->audio.alsa_period_size, packet_number, NULL, 0);
}
