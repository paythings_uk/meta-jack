/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"
#include <getopt.h>
#include <sys/mman.h>
#include <signal.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/* Set the test tone to be used */
void set_test_tone(network_audio_t *nA, char *arg)
{
    // default is to be a sine
    nA->debug.test_signal = sine_signal;
	if(nA->debug.test_signal_name)
		free(nA->debug.test_signal_name);
    nA->debug.test_signal_name = strdup(arg);
    if(!strcmp(arg, "sine")) nA->debug.test_signal=sine_signal;
    if(!strcmp(arg, "square")) nA->debug.test_signal=square_signal;
    if(!strcmp(arg, "saw")) nA->debug.test_signal=saw_signal;
    if(!strcmp(arg, "wavm")) nA->debug.test_signal=memory_signal;    
}

/*
    Parse the command line options (both short form and long)
    and set the appropriate parameters
*/
void set_options(int argc, char **argv, network_audio_t *nA)
{
    
    static struct option long_options[] =
     {
       /* The option list */
        {"help",            no_argument,        0,                              'h' },               

        {"slave",           no_argument,        &nA->is_master,                 0   },
        {"master",          no_argument,        &nA->is_master,                 1   },
        {"broadcast",       no_argument,        &nA->network.using_broadcast,   1   },
        {"timelog",         no_argument,        &nA->debug.timelog,             1   },
        {"no-timelog",      no_argument,        &nA->debug.timelog,             0   },
        {"dummy-network",   no_argument,        &nA->debug.dummy_network,       1   },      
        {"force-network",   no_argument,        &nA->debug.dummy_network,       0   },      
        {"dummy-audio",     no_argument,        &nA->debug.dummy_audio,         1   },      
        {"force-audio",     no_argument,        &nA->debug.dummy_audio,         0   },      

        {"log-directory",   required_argument,  0,                              'd' },
        {"remote-ip",       required_argument,  0,                              'i' },
        {"port",            required_argument,  0,                              'p' },
        {"sample-rate",     required_argument,  0,                              'r' },
        {"test",            required_argument,  0,                              't' },
        {"period-size",     required_argument,  0,                              'z' },
        {"ramp-up",         required_argument,  0,                              'U' },
        {"ramp-down",       required_argument,  0,                              'D' },
        {"loss-rate",       required_argument,  0,                              'R' },
        {0, 0, 0, 0}
     };
             
     while(1)
     {
        int option_index = 0;
        int c = getopt_long (argc, argv, "msh?abnz:p:r:f:i:t:u:U:D:R:M:", long_options, &option_index);     
        
        if(c==long_options[option_index].val)
        {
            DEBUG_LOG(STATUS, IA_CMDLINE, "Parsing commandline option -%c/--%s (arg:%s)", c, long_options[option_index].name, optarg);
        }
        else
        {
            DEBUG_LOG(STATUS, IA_CMDLINE, "Parsing commandline option -%c (arg:%s)", c, optarg);
        }
        
        if(c==-1) break; // end of options                    
        switch(c)
        {
            case 0:
                // flag set, do nothing
                break;      
            case 'a':
                nA->adaptive_buffering = 1;
                break;
            case 'b':
                nA->network.using_broadcast = 1;
                break;
            case 'd':
                nA->debug.log_directory = strdup(optarg);
                break;
            case '?':
            case 'h':
                help();
                exit(0);
                break; 
            case 'i':
                nA->network.remote_ip = optarg;
                nA->is_master = 1;
                break;
            case 'f':
                nA->using_filtering = 1;
                nA->filter_order = check_integer(optarg);
                break;
            case 'm':
                nA->is_master = 1;
                break;
            case 'n':
                nA->debug.entries_index = 0;
                nA->debug.entries_length = 50;
                nA->debug.entries = (networkaudio_debug_entry*)calloc(nA->debug.entries_length, sizeof(networkaudio_debug_entry));
                break;
            case 'p':
                nA->network.port_number = check_integer(optarg);
                break;
            case 'r':
                nA->audio.format.sample_rate = check_integer(optarg);
                break;
            case 's':
                nA->is_master = 0;
                break;
            case 't':
                nA->debug.test_mode = 1;                      
                if(optarg) 
                    set_test_tone(nA, optarg);
                break;
            case 'u':
                nA->initial_buffers = check_integer(optarg);
                break;
            case 'z':
                nA->period_size = check_integer(optarg);                
                break;
            case 'D':
                nA->debug.simulated_drop_interval = check_integer(optarg);
                break;
            case 'M':
                nA->max_buffers = check_integer(optarg);
                break;
            case 'R':
                nA->debug.simulated_drop_rate = check_integer(optarg) / 100.0;
                break;
        }
     }            
}

/* 
    Print the command line help
*/    
void help()
{
    printf("Usage: igaudio [options]\n");
    printf("        --help or -h                        Show this help\n");
    printf("        --master or -m                      Start as master (i.e. transmitter)\n");
    printf("        --slave or -s                       Start as slave (i.e. receiver)\n");
    printf("        --broadcast/-b                      Enable broadcast\n");
    printf("        --period-size or -z <size>          Period size \n");    
    printf("        --port or -p <port>                 UDP port to use \n");
    printf("        --remote-ip or -i  <ip_addr>        IP address to connect to\n");
    printf("        --sample-rate or -r   <sr>          Sample rate to use\n");
    printf("        --test  or -t  [type]               Replace network signal with test tone\n");
    printf("                       [type] can be wavm|saw|square|sine\n");
    printf("        --timelog                           Log timestamps. \n");
    printf("        --log-directory <dir>               Specify timestamp log directory. Default is /ramtemp. \n");
    printf("        --no-timelog                        Force disable time stamp logging\n");
    printf("        --socket-buffer or -c <n>           Socket send/receive socket buffer size (packets)\n");
    printf("        --loss-rate or -R <1-100>           Set percentage simulated loss rate\n");
    printf("        --loss-interval or -D <n>           Simulate dropping every nth packet (slave only)\n");
    printf("        -f                                  Use filtering to cover glitches (slave only)\n");
    printf("        -n                                  Send debug stats to PC (slave only).\n");
    printf("        -a                                  Enable adaptive buffering (slave only)\n");
    printf("        -u <buffers>                        Number of initial buffers (slave only)\n");
    printf("        -M <buffers>                        Max number of buffers to allow (slave only)\n");
    printf("\n");   
}

/* Set the default values for the network audio settings structure */
void set_defaults(network_audio_t *nA)
{
    
    nA->is_master = 0;
    nA->network.remote_ip = "127.0.0.1";
    nA->network.port_number   = 1234; //49152;        
    nA->period_size = 256;
    nA->audio.format.sample_rate = 48000;
    nA->audio.format.sample_bytes = SAMPLE_BYTES;
    nA->audio.format.channels = 1;
    nA->network.using_broadcast = 0;
    nA->debug.test_mode = 0;    
    nA->debug.test_signal_name = NULL;
    nA->debug.dummy_network = 0;
    nA->debug.timelog = 0;
    nA->debug.sleeps=0;
    nA->debug.log_directory="/tmp";
    nA->debug.dummy_audio = 0;
    nA->audio.alsa_buffer_size = 0;
    nA->audio.alsa_period_size = 0;
    nA->debug.entries = NULL;
    nA->frame_number = 0;
    nA->debug.simulated_drop_rate = 0;
    nA->debug.simulated_drop_interval = 0;
    nA->adaptive_buffering = 0;
    nA->initial_buffers = 0;
    nA->max_buffers = 10;
    nA->reduce_to_buffers = 6;
    nA->buffers_added = nA->buffers_removed = 0;
    nA->using_filtering = 0;
    nA->filter_order = 0;
    set_test_tone(nA, "saw");    
    nA->start_time = current_time();
    DEBUG_LOG(STATUS, NA_DEFAULTS, "Network audio defaults set");

}

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

/*
    Handle each section read from the ini file, which will be a name, value pair.
*/    
static int config_handler(void* user, const char* section, const char* name,
                   const char* v)
                   
{
    network_audio_t *nA = (network_audio_t*)user;
    char *value = (char *)v;
    DEBUG_LOG(STATUS, IA_CONFIG_OPT, "Parsing ia_config option %s:%s=%s", section, name, v);
    if(MATCH("network", "port")) nA->network.port_number = check_integer(value);
    if(MATCH("network", "remoteip")) nA->network.remote_ip = value;
    if(MATCH("network", "broadcast")) nA->network.using_broadcast = is_true(value);
    if(MATCH("general", "master")) nA->is_master=is_true(value);    
    if(MATCH("general", "slave")) nA->is_master=!is_true(value);    
    if(MATCH("general", "test_mode")) nA->debug.test_mode = is_true(value);
    if(MATCH("general", "test_tone")) set_test_tone(nA, value);
    if(MATCH("audio", "period_size")) nA->period_size = check_integer(value);                                
    if(MATCH("debug", "timelog")) nA->debug.timelog = is_true(value);      
    if(MATCH("debug", "dummy_network")) nA->debug.dummy_network = is_true(value);     
    if(MATCH("debug", "dummy_audio")) nA->debug.dummy_audio = is_true(value);     
    if(MATCH("debug", "log_directory")) nA->debug.log_directory = value;
    if(MATCH("debug", "sleeps")) nA->debug.sleeps = is_true(value);
  
    return 1;
}

/*
    Set the defaults based upon the config file /etc/ia_config, if it exists
*/
void load_config(network_audio_t *nA)
{
    if(ini_parse("/etc/ia_config", config_handler, nA)<0)
    {
        DEBUG_LOG(STATUS, IA_CONFIG, "No /etc/ia_config; using built in defaults instead");
    }
    else
    {
       DEBUG_LOG(STATUS, IA_CONFIG, "Loaded /etc/ia_config");
    }

}



#define fprint_iattr(F,X) (fprintf(F, "    %s: %d\n", #X, (int)(nA->X)))
#define fprint_dattr(F,X) (fprintf(F, "    %s: %.2f\n", #X ,(double)(nA->X)))
#define fprint_sattr(F,X) (fprintf(F, "    %s: \"%s\"\n", #X, nA->X))
/* Write a YAML form of the network audio structure to the given file descriptor */
void yaml_network_audio(FILE *fd, network_audio_t *nA)
{    
    fprintf(fd, "config: \n");    
    
    fprintf(fd, "  network:\n");
    fprint_sattr(fd, network.remote_ip);
    fprint_iattr(fd, network.port_number);
    fprint_iattr(fd, network.using_broadcast);
    
    fprintf(fd, "  audio:\n");
    fprint_iattr(fd, audio.alsa_buffer_size);
    fprint_iattr(fd, audio.alsa_period_size);

    fprintf(fd, "  sample format:\n");
    fprint_iattr(fd, audio.format.sample_rate);
    fprint_iattr(fd, audio.format.channels);
    fprint_iattr(fd, audio.format.sample_bytes);
    
    fprintf(fd, "  general:\n");
    fprintf(fd, "    compile_date: \"" __DATE__ " " __TIME__ "\"\n");
    fprint_iattr(fd, is_master);    
    fprint_iattr(fd, period_size);

    fprintf(fd, "  debug:\n");
    fprint_iattr(fd,debug.dummy_audio);
    fprint_iattr(fd,debug.sleeps);
    fprint_sattr(fd,debug.test_signal_name);
    fprint_iattr(fd,debug.test_mode);
    fprint_iattr(fd,debug.timelog);
    fprint_iattr(fd,debug.dummy_network);          
    fprint_sattr(fd,debug.log_directory);
        
}


/* Reset the stats block */
void reset_stats(network_audio_t *nA)
{
    timing_stats_t stats = {{0}};
    nA->stats = stats;
    nA->stats.period_start = current_time();
    nA->stats.packet_gaps_length = 50000;
    if(nA->stats.packet_gaps == NULL) 
	{
        nA->stats.packet_gaps = (int*)malloc(sizeof(int) * nA->stats.packet_gaps_length);
	}

    if(nA->stats.packet_numbers == NULL)
	{
        nA->stats.packet_numbers = (int*)malloc(sizeof(int) * nA->stats.packet_gaps_length);
	}
    memset(nA->stats.packet_numbers, 0, sizeof(int) * nA->stats.packet_gaps_length);
    memset(nA->stats.packet_gaps, 0, sizeof(int) * nA->stats.packet_gaps_length);
}

/* 
    Print out the current stats
*/
void print_timing_stat(FILE *fd, char *msg, average_tracker_t *tracker)
{
    double std = sqrt(tracker->M2/(tracker->count-1));
    fprintf(fd, "    %s:\n        events:%d\n        mean:%.3f\n        jitter:%.3f\n", msg, tracker->count, tracker->mean*1000.0, std*1000.0);
}

void print_var_stat(FILE *fd, char *msg, average_tracker_t *tracker)
{
    double std = sqrt(tracker->M2/(tracker->count-1));
        fprintf(fd, "    %s:\n        events:%d\n        mean:%.3f\n        jitter:%.3f\n", msg, tracker->count, tracker->mean, std);
}

#define print_istat(X) printf(#X": %d\n", nA->stats.X)
#define print_tstat(X) print_timing_stat(stdout, #X, &(nA->stats.X))
#define print_vstat(X) print_var_stat(stdout,#X, &(nA->stats.X))

#define fprint_istat(F,X) fprintf(F,"    "#X": %d\n", nA->stats.X)
#define fprint_tstat(F,X) print_timing_stat(F,#X, &(nA->stats.X))
#define fprint_vstat(F,X) print_var_stat(F,#X, &(nA->stats.X))

void yaml_network_stats(FILE *fd, network_audio_t *nA)
{    
    fprintf(fd, "stats:\n");
    fprintf(fd, "  period_start: %.4f\n", (nA->stats.period_start-nA->start_time));
    fprintf(fd, "  period_end: %.4f\n", (current_time()-nA->start_time));
    if(nA->is_master)
    {
        fprintf(fd, "  master:\n");
        
        fprint_istat(fd,packets_sent);
        fprint_istat(fd,record_overruns);
        fprint_istat(fd, record_incomplete_buffers);
        fprint_istat(fd, record_alsa_errors);
        fprint_istat(fd, network_send_errors);        
        fprint_istat(fd, pcm_missed_buffers);        
             
        fprint_tstat(fd, record_time);
        fprint_tstat(fd, transmit_time);   
        fprint_tstat(fd, master_loop_time);
    }    
    else
    {
       fprintf(fd, "  slave:\n");
       fprint_istat(fd,packets_received);
       fprint_istat(fd, buffers_played);           
       
       fprint_istat(fd,play_underruns);
       fprint_istat(fd,out_of_sequence_packets);
       fprint_istat(fd,duplicate_packets);
       fprint_istat(fd,late_packets);           
       fprint_istat(fd,receive_errors);
       fprint_istat(fd,invalid_packets);
       fprint_istat(fd,buffers_decoded);
       fprint_istat(fd,undecodeable_buffers);
       fprint_istat(fd,nonstream_packets);
       fprint_istat(fd,buffers_interpolated);
       fprint_istat(fd,pcm_failures);
       fprint_istat(fd,pcm_interval_checks_failed);
       
       fprint_tstat(fd,receive_time);
       fprint_tstat(fd,play_time);            
       fprint_tstat(fd,slave_loop_time);
       fprint_vstat(fd,queue_length);
    }        
}    

void log_yaml_network_stats(network_audio_t *nA, int reset)
{
    char yaml_file[2048];
    FILE *yaml_fd;
    sprintf(yaml_file, "%s/stats.yaml", nA->debug.opened_log_directory);
    yaml_fd = fopen(yaml_file, "a");
    if(!yaml_fd)
    {
        fprintf(stderr, "Could not open YAML file for logging");
        return;
    }        
    fprintf(yaml_fd, "---\n");
    yaml_network_stats(yaml_fd, nA);
    fclose(yaml_fd);
    if(reset)
        reset_stats(nA);           
}


void log_yaml_config(network_audio_t *nA)
{
    char yaml_file[2048];
    FILE *yaml_fd;
    sprintf(yaml_file, "%s/config.yaml", nA->debug.opened_log_directory);
    yaml_fd = fopen(yaml_file, "w");
    if(!yaml_fd)
    {
        DEBUG_LOG(ERROR, YAML_CONFIG_FAIL, "Could not open YAML file for logging");
        return;
    }    
    yaml_network_audio(yaml_fd, nA);
    fclose(yaml_fd);    
}


network_audio_t *global_nA;
FILE *debug_fd;

/* Print out the current stats */
void print_stats(int signum)
{
    network_audio_t *nA = global_nA;
    yaml_network_stats(stdout, nA);          
}


/* Shutdown all debugging logs cleanly, closing files etc. */
void close_debug(int signum)
{
    DEBUG_LOG(EVENT, CLOSE_DEBUG, "CLOSE_DEBUG");
    printf("\n\nShutting down...\n");
	fprintf(stderr, "close_debug\n");
    global_nA->running = 0;
}

/* Open the debug log */
void open_debug_log(network_audio_t *nA)
{    
    char fname[2048];
    char debug_fname[2048];
    char basename[1024];
    int ctr = 0;
    //int found = 0;
    
    const char *base_dir = nA->debug.log_directory;
    
    int err = access(base_dir, F_OK);
    if(err<0)
    {
        fprintf(stderr, "Cannot open debug base directory: %s\n", base_dir);
        exit(1);
    }
    
    // XXX temp
    /*while(!found)
    {
        sprintf(basename, "log_%04d",  ctr);
        sprintf(fname, "%s/%s", base_dir, basename);
        int err = access(fname, F_OK);                
        if(err<0 && (errno==ENOENT || errno==ENOTDIR))
            break;
        ctr++;
    }*/
    sprintf(basename, "log_%04d",  ctr);
    sprintf(fname, "%s/%s", base_dir, basename);


    mkdir(fname, 0777);
    sprintf(debug_fname, "%s/trace.log", fname);    
    // now switch to tracing to the debug file
    debug_fd = fopen(debug_fname, "w");
    if(!debug_fd)
    {
        fprintf(stderr, "Cannot open debug log: %s\n", debug_fname);
        exit(1);
    }

    nA->debug.opened_log_directory = strdup(fname);    
    printf("\nLog directory: %s\n", fname);
    
    {
        sprintf(debug_fname, "%s/stats.yaml", fname);
        unlink(debug_fname);
    }
}

// Network audio main function
int main(int argc, char *argv[])
{
    network_audio_t network_audio;
    network_audio_t *nA = &network_audio;
    global_nA = nA;
    debug_fd = stderr;    
    DEBUG_LOG(STATUS, COMPILE_DATE, "Compiled on " __DATE__ " " __TIME__);
    
    // seed the RNG
    struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);  
    srand(start_time.tv_sec ^ start_time.tv_nsec);    
    
    memset(nA, 0, sizeof(network_audio_t));
    set_defaults(nA);
 
    // register signal handler
    signal(SIGINT, close_debug); 
    signal(SIGTERM, close_debug); 
                           
    load_config(nA);
    set_options(argc, argv, nA);    

    open_debug_log(nA);

    alsa_init(nA);

    // Lock the memory for this process
    mlockall(MCL_CURRENT | MCL_FUTURE);

    yaml_network_audio(stdout, nA);    
    reset_stats(nA);
    
    // print the configuration/state
    log_yaml_config(nA);
    
    // fairly arbitrary numbers here...
    nA->stats.packet_thresholds[0] = (long)(0.00067 * nA->audio.period_ns);
    nA->stats.packet_thresholds[1] = (long)(0.00088 * nA->audio.period_ns);
    nA->stats.packet_thresholds[2] = (long)(0.00112 * nA->audio.period_ns);
    nA->stats.packet_thresholds[3] = (long)(0.00133 * nA->audio.period_ns);

    /*int zero = 0;
    nA->low_latency_fd = fopen("/dev/cpu_dma_latency", "wb");
    if(nA->low_latency_fd)
        fwrite(&zero, sizeof(zero), 1, nA->low_latency_fd);
    */
    nA->running = 1;
    
    // initalise LPC stuff
    if(!nA->is_master && nA->using_filtering)
        hybrid_init(nA->filter_order, nA->audio.alsa_period_size, nA->audio.alsa_period_size);
    
    if(nA->is_master) // this is the master
	{	
        DEBUG_LOG(STATUS, MASTER_START, "Starting master loop")
        realtime_priority(50, SCHED_FIFO);
        master_thread(nA);
	}
    
	else // this is the slave
	{
        // save the last 10s of audio in here
        bool audio_debug = false;
        if(audio_debug) {
            int len = 10;
            nA->debug.last_audio = (buffer_type*)malloc(sizeof(buffer_type) * nA->audio.format.sample_rate * len);
            nA->debug.last_audio_index = 0;
            nA->debug.last_audio_len = len * nA->audio.format.sample_rate;
            nA->debug.last_recv = (buffer_type*)malloc(sizeof(buffer_type) * nA->audio.format.sample_rate * len);
            nA->debug.last_recv_index = 0;
            nA->debug.last_recv_len = len * nA->audio.format.sample_rate;
            fprintf(stderr, "Saving last %d seconds of audio\n", len);
        } else {
            nA->debug.last_audio = NULL;
            nA->debug.last_recv = NULL;
        }
        DEBUG_LOG(STATUS, SLAVE_START, "Starting slave loop")
        slave_thread_blocking(nA);
        fprintf(stderr, "Network thread exited\n");

        dump_packet_gap_stats(nA);

        fprintf(stderr, "Late packets: %d/%d  %.2f%%\n", nA->stats.late_packets, nA->stats.packets_received, 100 * (nA->stats.late_packets / (nA->stats.packets_received * 1.0)));
        fprintf(stderr, "Lost packets: %d/%d  %.2f%%\n", nA->stats.missed_packets, nA->frame_number, 100 * (nA->stats.missed_packets / (nA->frame_number * 1.0)));
        fprintf(stderr, "Underruns: %d\n", nA->stats.play_underruns);

        double period_time = current_time() - nA->start_time;
        fprintf(stderr, "Packets per second: %.3f\n", nA->packet_number / period_time);

        if(audio_debug) {
            fprintf(stderr, "Writing last audio... (%d, %d)\n", nA->debug.last_audio_index, nA->debug.last_audio_len);
            FILE* la = fopen("lastaudio.raw", "wb");
            int chunk = nA->debug.last_audio_len - nA->debug.last_audio_index;
            fprintf(stderr, "> %d samples\n", fwrite(nA->debug.last_audio, sizeof(buffer_type), chunk, la));
            fprintf(stderr, "> %d samples\n", fwrite(nA->debug.last_audio, sizeof(buffer_type), nA->debug.last_audio_len - chunk, la));
            fclose(la);

            fprintf(stderr, "Writing last recv... (%d, %d)\n", nA->debug.last_recv_index, nA->debug.last_recv_len);
            la = fopen("lastrecv.raw", "wb");
            chunk = nA->debug.last_recv_len - nA->debug.last_recv_index;
            fprintf(stderr, "> %d samples\n", fwrite(nA->debug.last_recv, sizeof(buffer_type), chunk, la));
            fprintf(stderr, "> %d samples\n", fwrite(nA->debug.last_recv, sizeof(buffer_type), nA->debug.last_recv_len - chunk, la));
            fclose(la);

            free(nA->debug.last_audio);
            free(nA->debug.last_recv);
        }
	}
    
    if(!nA->is_master && nA->using_filtering)
        hybrid_cleanup();

    print_stats(0);    
    alsa_close(nA);

    log_yaml_network_stats(global_nA, 0);  
    printf("Log files in: %s\n\n", global_nA->debug.opened_log_directory);    
    fflush(debug_fd);
    fclose(debug_fd);
    printf("Closed debug FD\n");
    shutdown(global_nA->network.socket_fd, 2);
    close(global_nA->network.socket_fd);
    printf("Closed socket\n");

    // cleanup the network_audio struct
    if(nA->debug.test_signal_name) 
        free(nA->debug.test_signal_name);
    if(nA->debug.opened_log_directory) 
        free(nA->debug.opened_log_directory);
    free(nA->network.message_header->msg_iov[0].iov_base);
    free(nA->network.message_header->msg_iov);
    free(nA->network.message_header->msg_control);
    free(nA->network.message_header->msg_name);
    free(nA->network.message_header);
    free(nA->stats.packet_gaps);
    free(nA->stats.packet_numbers);

    printf("igaudio exiting cleanly\n");
	return 0;
}





