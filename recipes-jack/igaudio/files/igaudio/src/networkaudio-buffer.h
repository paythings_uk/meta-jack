/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef _NETWORKAUDIO_BUFFER_H_
#define _NETWORKAUDIO_BUFFER_H_
#include <stdint.h>

typedef double filter_type;

// 16-bit
//#define SAMPLE_BYTES    2
//typedef int16_t buffer_type;
//#define ALSA_PCM_FORMAT SND_PCM_FORMAT_S16_LE

// 24-bit (true)
//#define SAMPLE_BYTES    3
//typedef void buffer_type;

// 32-bit (or 24-bit packed into 32-bit) 
#define SAMPLE_BYTES    4
typedef int32_t buffer_type;
#define ALSA_PCM_FORMAT SND_PCM_FORMAT_S32_LE

// 32-bit float (for Opus)
//#define SAMPLE_BYTES 4
//typedef float buffer_type;
//#define ALSA_PCM_FORMAT SND_PCM_FORMAT_FLOAT_LE

#endif
