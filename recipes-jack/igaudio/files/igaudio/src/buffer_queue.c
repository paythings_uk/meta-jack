/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "buffer_queue.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

/*  Simple deque of fixed size buffers. There is a pool of unused buffers, ready to be used
    and a set of active buffers. The pool can be expanded as required. 
    
    The two key operations are enque() and deque().
    
    enque() takes a buffer from the pool and inserts it at the head of the active
    and returns a pointer to the data block in that buffer; this can then be filled with data.
    
    deque() takes a buffer from the end of the active buffer and returns a pointer to
    the data inside, which can then be passed to the audio device. The buffer is returned to the
    pool.
  
*/           

SGLIB_DEFINE_DL_LIST_PROTOTYPES(dq, DQ_COMPARATOR, prev, next);
SGLIB_DEFINE_DL_LIST_FUNCTIONS(dq, DQ_COMPARATOR,  prev, next);

/* Create a buffer queue with a given size */
buffer_q_t *init_buffer_q(int buffer_size, int initial_pool)
{
    buffer_q_t *q = malloc(sizeof(*q));
    q->buffer_size = buffer_size;    
    q->activeq = NULL;    
    q->poolq = NULL;  
    q->id_ctr = 0;
    expandpool_buffer_q(q, initial_pool);
    return q;
}

/* Free a buffer queue structure */
void free_buffer_q(buffer_q_t *q)
{
    clear_buffer_q(q);
    free(q);       
}

/* Clear and free the entire queue */
void clear_buffer_q(buffer_q_t *q)
{    
    while(sglib_dq_len(q->activeq)!=0)
    {
        free(q->activeq->buffer);       
        sglib_dq_delete(&q->activeq, q->activeq);                
    }
        
    while(sglib_dq_len(q->poolq)!=0)
    {
        free(q->poolq->buffer);
        sglib_dq_delete(&q->poolq, q->poolq);               
    }
}

/* (re)set the size of each buffer in the queue, clearing the
   queue if needed */
void set_buffer_q_size(buffer_q_t *q, int size)
{
    int total_pool = sglib_dq_len(q->poolq) + sglib_dq_len(q->activeq);
    // If size changed, clear the whole queue
    if(size!=q->buffer_size)
    {
        q->buffer_size = size;
        clear_buffer_q(q);        
        // make sure total number of buffers does not change
        expandpool_buffer_q(q, total_pool); 
    }           
}

/* Expand the pool by n buffers */
void expandpool_buffer_q(buffer_q_t *q, int n)
{
    int i;
    for(i=0;i<n;i++)
    {
        addpool_buffer_q(q);        
    }
}

/* Push a newly allocated buffer onto the pool */
void addpool_buffer_q(buffer_q_t *q)
{
    dq *entry;
    
    entry = malloc(sizeof(*entry));   
    entry->id = q->id_ctr++;
    entry->gain = 0;
    entry->interpolated = BUFFER_INVALID;
    entry->ptr = 0;
    entry->buffer = calloc(q->buffer_size, SAMPLE_BYTES);       
    dq *first;
    first = sglib_dq_get_first(q->poolq);
    sglib_dq_add_before(&first, entry);    
    q->poolq = first;       
}


/* Return the last buffer pushed onto the active queue */
buffer_type *last_added_buffer_q(buffer_q_t *q)
{
    dq *entry;
    entry = sglib_dq_get_first(q->activeq);
    if(entry)   
        return entry->buffer;    
    
    // active queue exhausted, try the head of the pool queue
    entry = sglib_dq_get_first(q->poolq);
    if(entry)
        return entry->buffer;
        
    // nothing at all in any queue -- should *never* happen
    return NULL;
}



/* Return the ix'th buffer that previously came off the active queue;
  that is the ix'th buffer of the pool queue, starting from the head.
  0 is the last buffer played; 1 is the 2nd last, and so on.
  Returns NULL if no such buffer.
  */
buffer_type *previous_buffer_q(buffer_q_t *q, int ix)
{
    int i;
    dq *entry;
    entry = sglib_dq_get_first(q->poolq);
    if(entry!=NULL)
    {
        for(i=0;i<ix;i++)
        {
            entry = entry->next;
            if(!entry) break;
        }
    }
    if(!entry) return NULL;
    return entry->buffer;
}

/* Return the length of the active queue */
int activeq_length_buffer_q(buffer_q_t *q)
{
    return sglib_dq_len(q->activeq);
}

/* Move a pool buffer into the head of the active queue. Return the buffer of that queue element. */
buffer_type *enque_buffer_q(buffer_q_t *q)
{
    dq *entry;
    entry = sglib_dq_get_last(q->poolq);    
    if(!entry)
    {
        // Pool queue exhausted; add another element
        addpool_buffer_q(q);
        entry = sglib_dq_get_last(q->poolq);    
    }        
    
    // all entries pushed onto the active queue should have ptr == 0    
    if(entry->ptr!=0)    
        fprintf(stderr, "Incomplete enque. Buffer pushed onto active queue with ptr!=0.\n");    
        
    sglib_dq_delete(&q->poolq, entry);           
    dq *first;
    first = sglib_dq_get_first(q->activeq);    
    sglib_dq_add_before(&first, entry);        
    q->activeq = first;
    return entry->buffer;
}


/* Enque from a buffer of a given size, feeding partial
   buffers as required. This copies the entire buffer
   into the active queue (partial remnants remain in the pool)
*/
void push_buffer_q(buffer_q_t *q, buffer_type *in, int in_length)
{
    int to_write = in_length;
    int frame_offset = 0;
    int frames;
    
    while(to_write>0)
    {
        dq *entry = sglib_dq_get_last(q->poolq);
        // get frames that can be copied into the last entry in the pool
        frames = fmin(q->buffer_size - entry->ptr, to_write);
        // copy frames from the buffer and advance
        memcpy(entry->buffer + entry->ptr, in + frame_offset, frames * SAMPLE_BYTES);
        entry->ptr = (entry->ptr + frames) % q->buffer_size;
        frame_offset += frames;
        entry->interpolated = BUFFER_RECORDED;
        to_write -= frames;
        // enque full frames
        if(entry->ptr==0) {
            enque_buffer_q(q);              
        }
    }    
}

/* Deque into a buffer of a given size, feeding partial
   buffers as required. May leave a partial buffer in the active queue.
   Returns the number of bytes writen.
*/
int pop_buffer_q(buffer_q_t *q, buffer_type *out, int out_length)
{
    int to_write = out_length;
    int frame_offset = 0;
    int frames;
    int written = 0;

    while(to_write>0)
    {
        dq *entry = sglib_dq_get_last(q->activeq);
        if(!entry) return written;
        // get frames that can be copied
        frames = fmin(q->buffer_size - entry->ptr, to_write);
        // copy frames from the buffer
        memcpy(out + frame_offset, entry->buffer + entry->ptr, frames*SAMPLE_BYTES);
        // advance the pointer
        frame_offset += frames;
        entry->ptr = (entry->ptr + frames) % q->buffer_size;
        to_write -= frames;
        written += frames;
        // deque full frames
        if(entry->ptr==0) {
            deque_buffer_q(q);                
        }
    }        
    return written;
}


/* Return the next *complete* (i.e. ptr==0) buffer from the active queue,
   or NULL, if there is no such one. */

buffer_type *pop_complete_buffer_q(buffer_q_t *q)
{
    dq *entry;
    entry = sglib_dq_get_last(q->activeq);
    if(entry && entry->ptr==0) {
        return deque_buffer_q(q);
    } else    
        return NULL; 
}

/* Pop the tail of the queue. Return the buffer it points to. */
buffer_type *deque_buffer_q(buffer_q_t *q)
{
    dq *entry;
    buffer_type *buf;
    
    entry = sglib_dq_get_last(q->activeq);
    if(entry)
    {
        buf=entry->buffer;
        sglib_dq_delete(&q->activeq, entry);        
        if(entry->ptr!=0)    
            fprintf(stderr, "Incomplete deque. Buffer removed from active queue with ptr!=0.\n");        
        dq *first;
        first = sglib_dq_get_first(q->poolq);    
        sglib_dq_add_before(&first, entry);        
        q->poolq = first;   
    }
    else    
    {        
        buf = NULL;
        // Active queue exhausted
    }        
    return buf;
}


/* Undo an enque */
void undo_enque_buffer_q(buffer_q_t *q)
{
    dq *entry = sglib_dq_get_first(q->activeq);
    if(entry)
    {
        sglib_dq_delete(&q->activeq, entry);        
        dq *last;
        last = sglib_dq_get_last(q->poolq);    
        sglib_dq_add_after(&last, entry);        
    }
 
}

/* Return the gain of the last buffer on the active queue. */
double last_gain_buffer_q(buffer_q_t *q)
{
    dq *first;
    first = sglib_dq_get_first(q->activeq);
    if(first)
        return first->gain;
    else
        // i.e. silent
        return -200.0;
}

/* Return the latency of the queue (i.e. length of the active q, in samples) */
int latency_buffer_q(buffer_q_t *q)
{
    //return sglib_dq_len(q->activeq)*q->buffer_size;
    dq *foo = sglib_dq_get_first(q->activeq);
    if(!foo) return 0;
    int latency_samples = 0;
    while(foo) {
        // foo->ptr = how much has been dequeued so far, ie ptr==0 means full buffer
        latency_samples += q->buffer_size - foo->ptr;
        foo = foo->next;
    }
    return latency_samples;
}


/* Add a buffer to the queue, setting the interpolated state, the frame number
    and the gain of the buffer. */
void add_buffer_q(buffer_q_t *q, buffer_type *in, int interpolated, int frame_number, int gain)
{
    enque_buffer_q(q);
    dq *entry = sglib_dq_get_first(q->activeq);
    if(entry)
    {
        entry->interpolated = interpolated;
        entry->frame_number = frame_number;
        memcpy(entry->buffer, in, SAMPLE_BYTES*q->buffer_size);
        //if(gain)
        //    entry->gain = compute_gain(entry->buffer, q->buffer_size);        
        //else
            entry->gain = -3.0;        // default gain is -3dB
        entry->ptr = 0;
    }
}

/*
    Stuff an uninitialised buffer into the queue 
*/
void stuff_buffer_q(buffer_q_t *q)
{
    enque_buffer_q(q);
    dq *entry = sglib_dq_get_first(q->activeq);
    if(entry)
    {
        entry->interpolated = BUFFER_INITIALIZED;
        entry->gain = -200.0;
        entry->frame_number = 0;
        entry->ptr = 0;
    }
}


/* Print the active q, including the gains, and whether or not packets were received. */
void print_activeq(buffer_q_t *q)
{
    dq *l;
    printf("In >>> ");
    for(l=sglib_dq_get_first(q->activeq); l!=NULL; l=l->next) 
    {   
        if(l->interpolated==BUFFER_INTERPOLATED)
            printf(">>%d<< ", l->frame_number);
        if(l->interpolated==BUFFER_INTERPOLATED_SEQ)
            printf("< %d > ", l->frame_number);
        else if(l->interpolated==BUFFER_RECEIVED)
        {
            printf("| %d | ", l->frame_number);            
        }        
        else if(l->interpolated==BUFFER_RECEIVED_COMPRESSED)
        {
            printf("[=%d=] ", l->frame_number);            
        }
        else if(l->interpolated==BUFFER_RECORDED)
        {
            printf("* %d * ", l->id);            
        }
        else if(l->interpolated==BUFFER_INITIALIZED)
            printf("|  -   |");
        else if(l->interpolated==BUFFER_TEST_SIGNAL)
            printf("( %3.f ) ", l->gain);
        else
            printf("!XXXXXX!");
    }
    printf(" >>> Out \n");
}
/* Print out the state of the queue (for debugging only) */
void print_buffer_q(buffer_q_t *q)
{
    printf("Buffer size: %d\n", q->buffer_size);
    printf("Pool elements: %d\n", sglib_dq_len(q->poolq));
    printf("Active elements: %d\n", sglib_dq_len(q->activeq));
    dq *l;
    
    printf(" just played -> ");
    for(l=sglib_dq_get_first(q->poolq); l!=NULL; l=l->next) printf("%4d %4d |", l->id, l->ptr);
    printf(" <- to be read \n");
    printf(" just read   -> ");
    for(l=sglib_dq_get_first(q->activeq); l!=NULL; l=l->next) printf("%4d %4d |", l->id, l->ptr);
    printf(" <- to be played \n");
   
}
/*
void test_partial_buffer_q()
{
    buffer_q_t *q;
    buffer_type buf[2048];
    q = init_buffer_q(480, 12);
    enque_buffer_q(q);
    enque_buffer_q(q);
    enque_buffer_q(q);
    enque_buffer_q(q);    
    push_buffer_q(q, buf, 1024);
    print_buffer_q(q);
    push_buffer_q(q, buf, 1024);
    print_buffer_q(q);
    pop_buffer_q(q, buf, 1024);
    print_buffer_q(q);    
    pop_buffer_q(q, buf, 1024);
    print_buffer_q(q);       
    pop_buffer_q(q, buf, 48);
    print_buffer_q(q);       
}



void test_buffer_q()
{
    buffer_q_t *q;
    q = init_buffer_q(256, 4);
    print_buffer_q(q);
    enque_buffer_q(q);
    print_buffer_q(q);    
    enque_buffer_q(q);
    print_buffer_q(q);
    deque_buffer_q(q);
    print_buffer_q(q);
    deque_buffer_q(q);
    print_buffer_q(q);
    set_buffer_q_size(q, 512);
    addpool_buffer_q(q);
    addpool_buffer_q(q);
    addpool_buffer_q(q);    
    print_buffer_q(q);
    enque_buffer_q(q);
    print_buffer_q(q);    
    enque_buffer_q(q);
    print_buffer_q(q);
    deque_buffer_q(q);
    deque_buffer_q(q);
    print_buffer_q(q);
}*/

/*
int main(int argc, char** argv) {
    buffer_q_t *q = init_buffer_q(128, 8);
    buffer_type *buffer = malloc(SAMPLE_BYTES * 128);
    for(int i=0;i<128;i++)
        buffer[i] = i;
    add_buffer_q(q, buffer, 0, 0, 0);
    print_buffer_q(q);
    int result = pop_buffer_q(q, buffer, 152);
    printf("Result: %d\n", result);
    print_buffer_q(q);
}
*/
