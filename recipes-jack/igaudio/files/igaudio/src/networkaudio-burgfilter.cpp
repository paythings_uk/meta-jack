/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio-burgfilter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define FIND_LPC_COND_FAC	5.999999848427251E-5f

static filter_type *C_first_row = NULL;
static filter_type *C_last_row = NULL;
static filter_type *CAf = NULL;
static filter_type *CAb = NULL;
static filter_type *Af = NULL;
static filter_type *coefficients = NULL;
static filter_type *hanning_window = NULL;

static buffer_type *prev_frame = NULL;
static buffer_type *cur_frame = NULL;
static buffer_type *ds_prev_frame = NULL;
static buffer_type *ds_cur_frame = NULL;

static unsigned buffer_size_bytes, autocorr_buffer_size, order, filter_size;

static bool last_buffer_missing = false;
    
static int STEP = 4;
static int ds_buffer_size;
static int ds_max_autocorr_buffer_size ;

void burg_init(unsigned _order, unsigned _filter_size, unsigned _autocorr_buffer_size)
{
    filter_size = _filter_size;
    buffer_size_bytes = _filter_size * sizeof(buffer_type);
    autocorr_buffer_size = _autocorr_buffer_size;
    order = _order;

    ds_buffer_size = 1 + (filter_size/STEP);
    ds_max_autocorr_buffer_size = ds_buffer_size * 20;
    fprintf(stderr, "DS BUFFER: %d\n", ds_buffer_size);

    coefficients = (filter_type*)malloc(sizeof(filter_type) * (order + 1));
    prev_frame = (buffer_type*)malloc(sizeof(buffer_type) * filter_size);
    cur_frame = (buffer_type*)malloc(sizeof(buffer_type) * (order + filter_size));

    C_first_row = (filter_type*)malloc(sizeof(filter_type) * order);
    C_last_row = (filter_type*)malloc(sizeof(filter_type) * order);
    CAf = (filter_type*)malloc(sizeof(filter_type) * (order + 1));
    CAb = (filter_type*)malloc(sizeof(filter_type) * (order + 1));
    Af = (filter_type*)malloc(sizeof(filter_type) * order);
    hanning_window = (filter_type*)malloc(sizeof(filter_type) * filter_size);

    for (unsigned i = 0; i < filter_size; i++)
        hanning_window[i] = (filter_type) sqrt(0.5 * (1 - cos(2 * M_PI*i / (filter_type)(filter_size - 1))));

    ds_prev_frame = (buffer_type*)malloc(sizeof(buffer_type) * ds_buffer_size);
    ds_cur_frame = (buffer_type*)malloc(sizeof(buffer_type) * (order + ds_buffer_size));

    last_buffer_missing = false;

    memset(prev_frame, 0, buffer_size_bytes);
    memset(ds_prev_frame, 0, ds_buffer_size * sizeof(buffer_type));
}

void burg_cleanup()
{
    free(C_first_row);
    free(C_last_row);
    free(CAf);
    free(CAb);
    free(Af);
    free(coefficients);
    free(cur_frame);
    free(prev_frame);
    free(ds_prev_frame);
    free(ds_cur_frame);
}

void burg_downsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned out_buffer_size, unsigned step) 
{
    int counter = 0;
    for(unsigned i=0;i<buffer_size;i+=step,counter++)
        outbuf[counter] = inbuf[i];
    outbuf[counter] = inbuf[buffer_size - 1];
}

void burg_upsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned out_buffer_size, unsigned step) 
{
    filter_type diff;
    int out_pos; 

    for(unsigned i=0; i < buffer_size - 1; i++) 
    {
        diff = ((filter_type)inbuf[i+1] - (filter_type)inbuf[i]) / step;
        out_pos = i * step;
        for(unsigned j=0; j < step; j++)
        {
            outbuf[out_pos + j] = (buffer_type)(inbuf[i] + (j * diff));
        }
    }

    unsigned left_over = (out_buffer_size - 1) % step;
    if(left_over > 0)
    {
        diff = ((filter_type)inbuf[buffer_size - 1] - (filter_type)inbuf[buffer_size - 2]) / left_over;
        for(unsigned j=1; j <= left_over; j++) 
        {
            outbuf[out_buffer_size - step + j] = (buffer_type)(inbuf[buffer_size - 2] + diff * j);
        }
    }
    else
    {
        outbuf[out_buffer_size - 1] = inbuf[buffer_size - 1];
    }
}

int burg_process_buffer(buffer_type *buffer, unsigned buffer_size, int buffer_ready)
{
    int path = 0;

    if (buffer_ready)
    {
        // TODO this can go in the first if() only...
        burg_downsample(buffer, filter_size, ds_cur_frame, ds_buffer_size, STEP);
        if (last_buffer_missing == false)
        {   // this packet+last packet both arrived OK
            silk_burg_modified_FLP(coefficients,
                                         ds_cur_frame,
                                         0.0001,
                                         ds_buffer_size,
                                         1,
                                         order);

            memcpy(prev_frame, buffer, buffer_size_bytes);
            memcpy(ds_prev_frame, ds_cur_frame, sizeof(buffer_type) * ds_buffer_size);
            //memcpy(cur_frame + order, buffer, buffer_size_bytes);
            last_buffer_missing = false;
            path = 1;

            return 1;
        }
        else
        {   // this packet arrived OK, last packet didn't

            for(unsigned m = 0; m < order; m++) 
                ds_cur_frame[m] = ds_prev_frame[ds_buffer_size - order + m];

            for(unsigned i = order; i < order + ds_buffer_size; i++)
            {
                double tmp = 0.0;
                for(unsigned j = 0; j < order; j++)
                {
                    tmp += coefficients[j] * ds_cur_frame[(i - 1 - j)];
                }
                ds_cur_frame[i] = (buffer_type)tmp;
            }

            burg_upsample(ds_cur_frame + order, ds_buffer_size, cur_frame + order, filter_size, STEP);

            unsigned cross_fade_samps = 64;	// must be a factor of the buffer_size
            float cross_fade_prop = filter_size / cross_fade_samps; // proportion of the buffer used to cross fade
            int ind_scale = cross_fade_prop / 2;

            // crossfade predicted->real
            for (unsigned i = 0; i < cross_fade_samps; i++)
            {
                filter_type h = hanning_window[ind_scale*i];
                filter_type v1 = h * buffer[i];
                filter_type v2 = cur_frame[order+i] * (1.0 - h);

                cur_frame[order+i] = (buffer_type)(v1 + v2);
            }

            // fill in the rest with new data
            for (unsigned i = cross_fade_samps; i < filter_size; i++)
                cur_frame[order+i] = buffer[i];
            // TODO memcpy(&(cur_frame[cross_fade_samps]), &(buffer[cross_fade_samps]), sizeof(buffer_type) * (filter_size - cross_fade_samps));

        /*FILE* foo = fopen("out.dat", "ab");
        fwrite(buffer, sizeof(buffer_type), filter_size, foo);
        fwrite(cur_frame + order, sizeof(buffer_type), filter_size, foo);
        fclose(foo);*/

            last_buffer_missing = false;
            path = 2;
            memcpy(prev_frame, cur_frame+order, buffer_size_bytes);
            burg_downsample(cur_frame + order, filter_size, ds_prev_frame, ds_buffer_size, STEP);
        }
    }
    else
    {
        // crossfade last (real) packet into new predicted packet

        for(unsigned i = 0; i < order; i++) 
            ds_cur_frame[i] = ds_prev_frame[ds_buffer_size - order + i];

        for(unsigned i = order; i < ds_buffer_size + order; i++)
        {
            double tmp = 0.0;
            for(unsigned j = 0; j < order; j++)
            {
                tmp += coefficients[j] * ds_cur_frame[i - 1 - j];
            }
            ds_cur_frame[i] = (buffer_type)tmp;
        }

        memset(cur_frame, 0, sizeof(buffer_type) * (order + filter_size));
        burg_upsample(ds_cur_frame + order, ds_buffer_size, cur_frame + order, filter_size, STEP);

        /*
        FILE* foo = fopen("foo.dat", "wb");
        fwrite(coefficients, sizeof(filter_type), order + 1, foo);
        fwrite(buffer, sizeof(buffer_type), filter_size, foo);
        fwrite(prev_frame, sizeof(buffer_type), filter_size, foo);
        fwrite(cur_frame, sizeof(buffer_type), filter_size + order, foo);
        fwrite(ds_prev_frame, sizeof(buffer_type), ds_buffer_size, foo);
        fwrite(ds_cur_frame, sizeof(buffer_type), ds_buffer_size + order, foo);
        long p = ftell(foo);
        fwrite(&p, sizeof(long), 1, foo);
        fclose(foo);
        */
        //exit(0);
      
        /*FILE* foo = fopen("out.dat", "ab");
        fwrite(buffer, sizeof(buffer_type), filter_size, foo);
        fwrite(cur_frame + order, sizeof(buffer_type), filter_size, foo);
        fclose(foo);*/

        last_buffer_missing = true;
        path = 3;
        memcpy(prev_frame, cur_frame+order, buffer_size_bytes);
        memcpy(ds_prev_frame, ds_cur_frame + order, ds_buffer_size * sizeof(buffer_type));
    }

    memcpy(buffer, cur_frame+order, buffer_size_bytes);

    return 1;
}

#define MAX_FRAME_SIZE              384 /* subfr_length * nb_subfr = ( 0.005 * 16000 + 16 ) * 4 = 384*/

/* Compute reflection coefficients from input signal */
filter_type silk_burg_modified_FLP(          /* O    returns residual energy                                     */
    filter_type          A[],                /* O    prediction coefficients (length order)                      */
    const buffer_type    x[],                /* I    input signal, length: nb_subfr*(D+L_sub)                    */
    const filter_type    min_inv_gain,         /* I    minimum inverse prediction gain                             */
    const int      subfr_length,       /* I    input signal subframe length (incl. D preceding samples)    */
    const int      nb_subfr,           /* I    number of subframes stacked in x                            */
    const int      D                   /* I    order                                                       */
    )
{
    int         k, n, s, reached_max_gain;
    double           C0, inv_gain, num, nrg_f, nrg_b, rc, Atmp, tmp1, tmp2;
    const buffer_type *x_ptr;
    //double           C_first_row[MAX_ORDER_LPC], C_last_row[MAX_ORDER_LPC];
    //double           CAf[MAX_ORDER_LPC + 1], CAb[MAX_ORDER_LPC + 1];
    //double           Af[MAX_ORDER_LPC];

    //silk_assert(subfr_length * nb_subfr <= MAX_FRAME_SIZE);

    /* Compute autocorrelations, added over subframes */
    C0 = energy_FLP(x, nb_subfr * subfr_length);
    memset(C_first_row, 0, order * sizeof(double));
    for (s = 0; s < nb_subfr; s++) {
        x_ptr = x + s * subfr_length;
        for (n = 1; n < D + 1; n++) {
            C_first_row[n - 1] += inner_product_FLP(x_ptr, x_ptr + n, subfr_length - n);
        }
    }
    memcpy(C_last_row, C_first_row, order * sizeof(double));

    /* Initialize */
    CAb[0] = CAf[0] = C0 + FIND_LPC_COND_FAC * C0 + 1e-9f;
    inv_gain = 1.0f;
    reached_max_gain = 0;
    for (n = 0; n < D; n++) {
        /* Update first row of correlation matrix (without first element) */
        /* Update last row of correlation matrix (without last element, stored in reversed order) */
        /* Update C * Af */
        /* Update C * flipud(Af) (stored in reversed order) */
        for (s = 0; s < nb_subfr; s++) {
            x_ptr = x + s * subfr_length;
            tmp1 = x_ptr[n];
            tmp2 = x_ptr[subfr_length - n - 1];
            for (k = 0; k < n; k++) {
                C_first_row[k] -= x_ptr[n] * x_ptr[n - k - 1];
                C_last_row[k] -= x_ptr[subfr_length - n - 1] * x_ptr[subfr_length - n + k];
                Atmp = Af[k];
                tmp1 += x_ptr[n - k - 1] * Atmp;
                tmp2 += x_ptr[subfr_length - n + k] * Atmp;
            }
            for (k = 0; k <= n; k++) {
                CAf[k] -= tmp1 * x_ptr[n - k];
                CAb[k] -= tmp2 * x_ptr[subfr_length - n + k - 1];
            }
        }
        tmp1 = C_first_row[n];
        tmp2 = C_last_row[n];
        for (k = 0; k < n; k++) {
            Atmp = Af[k];
            tmp1 += C_last_row[n - k - 1] * Atmp;
            tmp2 += C_first_row[n - k - 1] * Atmp;
        }
        CAf[n + 1] = tmp1;
        CAb[n + 1] = tmp2;

        /* Calculate nominator and denominator for the next order reflection (parcor) coefficient */
        num = CAb[n + 1];
        nrg_b = CAb[0];
        nrg_f = CAf[0];
        for (k = 0; k < n; k++) {
            Atmp = Af[k];
            num += CAb[n - k] * Atmp;
            nrg_b += CAb[k + 1] * Atmp;
            nrg_f += CAf[k + 1] * Atmp;
        }
        //silk_assert(nrg_f > 0.0);
        //silk_assert(nrg_b > 0.0);

        /* Calculate the next order reflection (parcor) coefficient */
        rc = -2.0 * num / (nrg_f + nrg_b);
        //silk_assert(rc > -1.0 && rc < 1.0);

        /* Update inverse prediction gain */
        tmp1 = inv_gain * (1.0 - rc * rc);
        if (tmp1 <= min_inv_gain) {
            /* Max prediction gain exceeded; set reflection coefficient such that max prediction gain is exactly hit */
            rc = sqrt(1.0 - min_inv_gain / inv_gain);
            if (num > 0) {
                /* Ensure adjusted reflection coefficients has the original sign */
                rc = -rc;
            }
            inv_gain = min_inv_gain;
            reached_max_gain = 1;
        }
        else {
            inv_gain = tmp1;
        }

        /* Update the AR coefficients */
        for (k = 0; k < ((n + 1) >> 1); k++) {
            tmp1 = Af[k];
            tmp2 = Af[n - k - 1];
            Af[k] = tmp1 + rc * tmp2;
            Af[n - k - 1] = tmp2 + rc * tmp1;
        }
        Af[n] = rc;

        if (reached_max_gain) {
            /* Reached max prediction gain; set remaining coefficients to zero and exit loop */
            for (k = n + 1; k < D; k++) {
                Af[k] = 0.0;
            }
            break;
        }

        /* Update C * Af and C * Ab */
        for (k = 0; k <= n + 1; k++) {
            tmp1 = CAf[k];
            CAf[k] += rc * CAb[n - k + 1];
            CAb[n - k + 1] += rc * tmp1;
        }
    }

    if (reached_max_gain) {
        /* Convert to silk_float */
        for (k = 0; k < D; k++) {
            A[k] = (float)(-Af[k]);
        }
        /* Subtract energy of preceding samples from C0 */
        for (s = 0; s < nb_subfr; s++) {
            C0 -= energy_FLP(x + s * subfr_length, D);
        }
        /* Approximate residual energy */
        nrg_f = C0 * inv_gain;
    }
    else {
        /* Compute residual energy and store coefficients as silk_float */
        nrg_f = CAf[0];
        tmp1 = 1.0;
        for (k = 0; k < D; k++) {
            Atmp = Af[k];
            nrg_f += CAf[k + 1] * Atmp;
            tmp1 += Atmp * Atmp;
            A[k] = (float)(-Atmp);
        }
        nrg_f -= FIND_LPC_COND_FAC * C0 * tmp1;
    }

    /* Return residual energy */
    return (float)nrg_f;
}


/* sum of squares of a SKP_float array, with result as double */
filter_type energy_FLP(
    const buffer_type *data,
    int             dataSize
    )
{
    int  i, dataSize4;
    double   result;

    /* 4x unrolled loop */
    result = 0.0f;
    dataSize4 = dataSize & 0xFFFC;
    for (i = 0; i < dataSize4; i += 4) {
        result += data[i + 0] * data[i + 0] +
            data[i + 1] * data[i + 1] +
            data[i + 2] * data[i + 2] +
            data[i + 3] * data[i + 3];
    }

    /* add any remaining products */
    for (; i < dataSize; i++) {
        result += data[i] * data[i];
    }

    //SKP_assert(result >= 0.0);
    return result;
}

/* inner product of two SKP_float arrays, with result as double     */
filter_type inner_product_FLP(      /* O    result              */
    const buffer_type     *data1,         /* I    vector 1            */
    const buffer_type     *data2,         /* I    vector 2            */
    int             dataSize        /* I    length of vectors   */
    )
{
    int  i, dataSize4;
    double   result;

    /* 4x unrolled loop */
    result = 0.0f;
    dataSize4 = dataSize & 0xFFFC;
    for (i = 0; i < dataSize4; i += 4) {
        result += data1[i + 0] * data2[i + 0] +
            data1[i + 1] * data2[i + 1] +
            data1[i + 2] * data2[i + 2] +
            data1[i + 3] * data2[i + 3];
    }

    /* add any remaining products */
    for (; i < dataSize; i++) {
        result += data1[i] * data2[i];
    }

    return result;
}

