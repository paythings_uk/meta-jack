/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"

#ifdef OPUS
#include "opus_defines.h"
#include "opus_private.h"

void opus_error(int err)
{
    fprintf(stderr, "Opus error code: %s\n", opus_strerror(err));
}

#define opus_ectl_check(X,Y) err=opus_encoder_ctl(X,Y); if(err!=OPUS_OK) {opus_error(err); printf("%s\n", #Y);}

/* Set up the opus encoder */
void setup_opus_encoder(network_audio_t *nA)
{
    int err;
    nA->opus.encoder = opus_encoder_create (nA->audio.format.sample_rate, nA->audio.format.channels, OPUS_APPLICATION_AUDIO, &err);
    if(err!=OPUS_OK)
        opus_error(err);
        
    // Configure for musical use, 16 bit pcm input, inband FEC, variable bit rate
    opus_ectl_check(nA->opus.encoder, OPUS_SET_BANDWIDTH(OPUS_BANDWIDTH_FULLBAND));        
    opus_ectl_check(nA->opus.encoder, OPUS_SET_APPLICATION(OPUS_APPLICATION_AUDIO));
    //opus_ectl_check(nA->opus.encoder, OPUS_SET_INBAND_FEC(1)); (this is apparently disabled for frame sizes <10ms anyway)
    opus_ectl_check(nA->opus.encoder, OPUS_SET_LSB_DEPTH(16));    
    opus_ectl_check(nA->opus.encoder, OPUS_SET_SIGNAL(OPUS_SIGNAL_MUSIC));
    opus_ectl_check(nA->opus.encoder, OPUS_SET_VBR(1));
    
    //opus_ectl_check(nA->opus.encoder, OPUS_SET_FORCE_MODE(MODE_CELT_ONLY));
    //opus_ectl_check(nA->opus.encoder, OPUS_SET_FORCE_CHANNELS(1));

    // Set the expected loss percentage
    opus_ectl_check(nA->opus.encoder, OPUS_SET_PACKET_LOSS_PERC(nA->opus.expected_loss_percentage));
    
    // Set bitrate. Set to max if kbps=0
    if(nA->opus.kbps != 0)
    {
        opus_ectl_check(nA->opus.encoder, OPUS_SET_BITRATE(nA->opus.kbps*1024));
    }
    else
    {
       opus_ectl_check(nA->opus.encoder, OPUS_SET_BITRATE(OPUS_BITRATE_MAX));
    }   

    // Store the total delay of this encoder
    opus_ectl_check(nA->opus.encoder, OPUS_GET_LOOKAHEAD(&nA->opus.lookahead_samples));  
    fprintf(stderr, "Opus encoder setup completed (%d)\n", nA->opus.lookahead_samples);
}


/* Encode a single packet, from a PCM buffer to compressed bytes 
   max_buffer_size limits the size of the buffer which will be written
*/
int opus_encode_packet(network_audio_t *nA, buffer_type *pcm, unsigned char *to_buffer, int max_buffer_size)
{
    if(!nA->opus.encoder) error("Encoder not initialised!");
    int err;
    //err = opus_encode_float(nA->opus.encoder, pcm, nA->audio.alsa_period_size, to_buffer, max_buffer_size);
    err = opus_encode(nA->opus.encoder, pcm, nA->audio.alsa_period_size, to_buffer, max_buffer_size);
    if(err<0)
        opus_error(err);    
    return err;
}


/* Decode a single packet from compressed bytes to a PCM buffer. */
int opus_decode_packet(network_audio_t *nA, buffer_type *pcm, unsigned char *opus_data, int opus_data_len_bytes)
{
    if(!nA->opus.decoder) error("Decoder not initialised!");
    int err;
    //err = opus_decode_float(nA->opus.decoder, from_buffer,  buffer_len, pcm, nA->audio.alsa_period_size, 0);
    err = opus_decode(nA->opus.decoder, opus_data, opus_data_len_bytes, pcm, nA->audio.alsa_period_size, 0); 
    if(err<0)
        opus_error(err);    
    return err==nA->audio.alsa_period_size;
}

/* Interpolate a missing packet into a PCM buffer. */
int opus_interpolate_packet(network_audio_t *nA, buffer_type *pcm)
{
    if(!nA->opus.decoder) error("Decoder not initialised!");
    int err;
    //err = opus_decode_float(nA->opus.decoder, NULL, 0, pcm, nA->audio.alsa_period_size, 0);
    err = opus_decode(nA->opus.decoder, NULL, 0, pcm, nA->audio.alsa_period_size, 0);
    if(err<0)
        opus_error(-err);    
    return err==nA->audio.alsa_period_size;
}

#define opus_dctl_check(X,Y) err=opus_decoder_ctl(X,Y); if(err!=OPUS_OK) opus_error(err);

/* Set up the opus decoder */
void setup_opus_decoder(network_audio_t *nA)
{
    int err;
    nA->opus.decoder = opus_decoder_create (nA->audio.format.sample_rate, nA->audio.format.channels, &err);
    if(err!=OPUS_OK)
        opus_error(err);
        
    // no valid value for the lookahead (doesn't make sense on the decoder side)
    nA->opus.lookahead_samples = -1;    
}

#endif

