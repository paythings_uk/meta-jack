/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "timer.h"
#include "networkaudio.h"
#include <sched.h>

/* 
    Return a log file name, using the current date,
    specified log type, and essential parameters of the
    configuration (period size, number of buffers, sleep status,
    network enabled, blocking status)     
*/
char *timelog_fname(int type, struct network_audio_t *nA)
{
    char *logtype;
    char *networking, *sleeps;
    time_t now;
    char *blocks;
    struct tm *today;  
    char filename[1024];
    char date[64];

    //get current date  
    time(&now);  
    today = localtime(&now);
    
    switch(type)
    {
        case LOG_TYPE_MASTER:
            logtype="master";
            break;
        case LOG_TYPE_SLAVE_NETWORK:
            logtype="slave_network";
            break;
        case LOG_TYPE_SLAVE_AUDIO:
            logtype="slave_audio";
            break;
        default:
            logtype="unknown";
            break;            
    }
    
    if(!nA->debug.dummy_network) networking="networking"; else networking="dummy";
    if(nA->debug.sleeps) sleeps="sleeps"; else sleeps="nosleeps";
    blocks="nonblock";
    
    strftime(date, 64, "%Y_%m_%d_%H_%M_%S", today);
    
    sprintf(filename, "%s/%s_%s_p%d_n%d_%s_%s_%s.csv",  nA->debug.log_directory, logtype, date, nA->period_size, 1, networking, sleeps, blocks);
    return strdup(filename);    
}

/* Return the current time as a timespec */
struct timespec timer_start(){
    struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    return start_time;
}

/* Return the microseconds that have elapsed since the given start_time */
long timer_end(struct timespec start_time){
    struct timespec end_time;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    long diffInMicros = (end_time.tv_nsec - start_time.tv_nsec)/1000;
    diffInMicros += (end_time.tv_sec - start_time.tv_sec)*1000000;
    return diffInMicros;
}

long timer_end_ms(struct timespec start_time) {
    struct timespec end_time;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    long diffInMillis = (end_time.tv_nsec - start_time.tv_nsec)/1000000;
    diffInMillis += (end_time.tv_sec - start_time.tv_sec)*1000;
    return diffInMillis;
}

/* Create a new timer object */
ustimer_t *open_ustimer(char *fname)
{    
    ustimer_t *timer = (ustimer_t*)malloc(sizeof(*timer));
    timer->logfile = fopen(fname, "w");
    /*if(timer->logfile)
        printf("Opened %s for time logging.\n", fname);*/
    timer->start_time = timer_start();
    timer->last_time = timer_start();
    timer->writes_since_flush = 0;
    return timer;
}

/*  Time one loop (e.g. in the packet send loop). Write 
    the total elapsed time, and the time of this loop, to the
    log file. */
void loop_ustimer(ustimer_t *timer)
{
    long loop_time, t;
    loop_time = timer_end(timer->last_time);
    t = timer_end(timer->start_time);
    if(timer->logfile)
    {
        fprintf(timer->logfile, "%ld, %ld\n", t, loop_time);
        
        timer->writes_since_flush++;
        if(timer->writes_since_flush>5000)
        {
            fflush(timer->logfile);
            timer->writes_since_flush=0;
        }
    }    
}

#define NSEC_PER_SEC    1000000000
/* Normalise a timespec */
void tsnorm(struct timespec *ts) {
    while (ts->tv_nsec >= NSEC_PER_SEC) {
        ts->tv_nsec -= NSEC_PER_SEC;
        ts->tv_sec++;
    }
}

/* Close the timer log file, and free the timer structure */
void close_ustimer(ustimer_t *timer)
{
    fclose(timer->logfile);
    free(timer);
}

/* Return the current time in floating seconds */
double current_time()
{
    struct timespec start_time;
    double time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    time = start_time.tv_sec + start_time.tv_nsec*1e-9;
    return time;
}

