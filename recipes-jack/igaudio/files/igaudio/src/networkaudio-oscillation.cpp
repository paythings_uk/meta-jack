/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "networkaudio-oscillation.h"

Oscillation::Oscillation(int buffer_size)
{
    pos_zc = new RingBuffer(10, buffer_size);
    neg_zc = new RingBuffer(10, buffer_size);
    osc1 = (buffer_type*)malloc(sizeof(buffer_type) * MAX_OSC_LEN);   
    osc2 = (buffer_type*)malloc(sizeof(buffer_type) * MAX_OSC_LEN);   
    bufferSize = buffer_size;
    prevBufferNo = -1;
    discardCurOsc = false;
    curIsOsc1 = true;
    prev_val = 0;
    len_cur_osc = 0;
    len_last_full_osc = 0;
}

Oscillation::~Oscillation()
{
    delete pos_zc;
    delete neg_zc;
    free(osc1);
    free(osc2);
}

void Oscillation::AddBuffer(buffer_type *new_buff, int buffer_no)
{
    buffer_type *osc;
    int pos_zc_ind = -1;
    int neg_zc_ind = -1;
    int copyFromInd = 0;

    if(curIsOsc1)
        osc = osc1;
    else
        osc = osc2;

    if(buffer_no - prevBufferNo > 2)
        discardCurOsc = true; // then we're missing a buffer so don't save this current oscillation. Still need to read to the end however

    for(int i = 0; i < bufferSize; i++)
    {
        if(prev_val <= 0 && new_buff[i] > 0)
        {
            pos_zc->AddVals(buffer_no, i);
            pos_zc_ind = i;

            // then new oscillation
            if(len_cur_osc + pos_zc_ind > MAX_OSC_LEN) // then osc too long so discard
                discardCurOsc = true;

            if(!discardCurOsc)
            {
                memcpy(&(osc[len_cur_osc]), &(new_buff[copyFromInd]), sizeof(buffer_type)*(pos_zc_ind - copyFromInd));
                len_last_full_osc = len_cur_osc + pos_zc_ind - copyFromInd;

                curIsOsc1 = !curIsOsc1;

                if(curIsOsc1)
                    osc = osc1;
                else
                    osc = osc2;
            }
            else
                ; // discard the osciallation as something went wrong and have run out of space

            len_cur_osc = 0;
            copyFromInd = i;
            discardCurOsc = false;

        }
        else if(prev_val <= 0 && new_buff[i] >0)
        {
            neg_zc->AddVals(buffer_no, i);
            neg_zc_ind = i;
        }

        prev_val = new_buff[i];
    }

    // reached the end of the buffer so store the incomplete oscillation
    memcpy(&(osc[len_cur_osc]), &(new_buff[copyFromInd]), sizeof(buffer_type)*(bufferSize -1 - copyFromInd));
    len_cur_osc += bufferSize -1 - copyFromInd;
    prevBufferNo = buffer_no;
    prev_val = new_buff[bufferSize-1];
}

long Oscillation::GetPosVariance()
{
    return pos_zc->GetVariance();
}

long Oscillation::GetNegVariance()
{
    return neg_zc->GetVariance();
}

int Oscillation::GetPrevOsc(buffer_type val, float deriv, buffer_type *prev_osc)
{
    buffer_type closest = 32000, dist;
    int closest_ind = 0;

    if(len_last_full_osc == 0)
    {
        memset(prev_osc, 0, sizeof(buffer_type) * 100);
        return 100;
    }

    buffer_type *full_osc;
    if(curIsOsc1) // then last full osc is 2
        full_osc = osc2;
    else
        full_osc = osc1;

    for(int i = 1; i < len_last_full_osc; i++)
    {
        // look for the best match
        if(sgn(deriv) == sgn(full_osc[i]-full_osc[i-1]))
        {
            dist = abs(val-full_osc[i]);
            if(abs(dist) <= closest) // then a new best match
            {
                closest = abs(dist);
                closest_ind = i;
            }
        }

    }
    memcpy(prev_osc, &(full_osc[closest_ind]), sizeof(buffer_type) * (len_last_full_osc-closest_ind));
    memcpy(&(prev_osc[len_last_full_osc-closest_ind]), full_osc, sizeof(buffer_type) * (closest_ind));
    
    return len_last_full_osc;
}

int Oscillation::sgn(buffer_type val)
{
    // eenie meenie mynie mo..... map the 0 case to positive.
    if(val < 0)
        return -1;
    else
        return 1;
}
