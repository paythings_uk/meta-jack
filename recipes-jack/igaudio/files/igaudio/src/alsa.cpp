/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"
    
static buffer_type *wavData = NULL;

/*
    Check if err<0; if so, there is an error.
    Print the given error message, and the string representation of the message
    and then exit with an error code.
*/
void alsa_check_error(int err, char *msg)
{
    if(err<0)
    {
        DEBUG_LOG(ERROR, ALSA_ERROR, "ALSA:%s error code:%s\n", msg, snd_strerror(err));
        exit(1);
    }
}

unsigned alsa_period_buffer_size_bytes(network_audio_t *nA) 
{
    return nA->audio.alsa_period_size * nA->audio.format.channels * SAMPLE_BYTES;
}

/* Close the ALSA device */
void alsa_close(network_audio_t *nA)
{   
    int err;

    err = snd_pcm_drop(nA->audio.pcm_handle);
    alsa_check_error(err, "Error stopping stream");

    err = snd_pcm_close(nA->audio.pcm_handle);
    alsa_check_error(err, "Error closing stream");

    if(wavData)
        free(wavData);
}

int alsa_xrun_recovery(snd_pcm_t *handle, int err) 
{
    if(err == -EPIPE) 
    {
        err = snd_pcm_prepare(handle);
        if(err < 0)
            fprintf(stderr, "Can't recover from underrun: %s\n", snd_strerror(err));
        return 0;
    } 
    else if(err == -ESTRPIPE) 
    {
        while((err = snd_pcm_resume(handle)) == -EAGAIN) 
        {
            usleep(1);
        }
        if(err < 0) 
        {
            err = snd_pcm_prepare(handle);
            if(err < 0)
                fprintf(stderr, "Can't recover from suspend: %s\n", snd_strerror(err));
        }
        return 0;
    }
    return err;
}

/* 
    Set the software parameters for the ALSA device.
    Only needed for playback, not capture.
*/
void alsa_set_sw_params(network_audio_t *nA)
{
    snd_pcm_sw_params_t *sw_params;
    int err;
    
	err = snd_pcm_sw_params_malloc (&sw_params);
    alsa_check_error(err, "Cannot allocate software parameters structure");
    	
	err = snd_pcm_sw_params_current (nA->audio.pcm_handle, sw_params);
    alsa_check_error(err, "Cannot initialise software parameters structure");
    	
    // this sets the minimum number of frames of PCM data that must be available for the 
    // ALSA device to report itself as available
	err = snd_pcm_sw_params_set_avail_min (nA->audio.pcm_handle, sw_params, nA->period_size); 
    alsa_check_error(err,"Cannot set minimum available count");
		
    // this sets a threshold for the number of frames that must be available before
    // audio playback is started
	err = snd_pcm_sw_params_set_start_threshold (nA->audio.pcm_handle, sw_params, 2*nA->period_size); // 0);
    alsa_check_error(err,"Cannot set start mode");

	// Set sw Params
	err = snd_pcm_sw_params (nA->audio.pcm_handle, sw_params);
    alsa_check_error(err,"Cannot set software parameters");

    {
        unsigned long val;
        err = snd_pcm_sw_params_current(nA->audio.pcm_handle, sw_params);
        alsa_check_error(err, "Cannot get software parameters");

        err = snd_pcm_sw_params_get_boundary(sw_params, &val);
        alsa_check_error(err, "Cannot get boundary");

        err = snd_pcm_sw_params_set_stop_threshold(nA->audio.pcm_handle, sw_params, val);
        alsa_check_error(err, "Cannot set stop threshold");

        err = snd_pcm_sw_params_set_silence_threshold(nA->audio.pcm_handle, sw_params, 0);
        alsa_check_error(err, "Cannot set silence threshold");

        err = snd_pcm_sw_params_set_silence_size(nA->audio.pcm_handle, sw_params, val);
        alsa_check_error(err, "Cannot set silence size");
        
    	// Set sw Params
	    err = snd_pcm_sw_params (nA->audio.pcm_handle, sw_params);
        alsa_check_error(err,"Cannot set software parameters");
    }

    snd_pcm_sw_params_free(sw_params);
}

/* 
    Initialise the ALSA audio device.
    PCM device is nA->audio.pcm_handle     
*/
void alsa_init(network_audio_t *nA)
{
	snd_pcm_hw_params_t *hw_params;
    int err, mode;
    snd_pcm_stream_t type;
    snd_pcm_access_t access_type;
    
    // master = capture, slave = playback
    type = (nA->is_master) ? SND_PCM_STREAM_CAPTURE : SND_PCM_STREAM_PLAYBACK;

    // master = nonblocking, slave = ? blocking?
    mode = (nA->is_master) ? SND_PCM_NONBLOCK : 0; //SND_PCM_NONBLOCK;

    access_type = SND_PCM_ACCESS_RW_INTERLEAVED;

    err = snd_pcm_open (&nA->audio.pcm_handle, nA->is_master ? "default" : "default", type, mode); 
    alsa_check_error(err, "Cannot open audio device");    

	// Allocate hardware parameters
	err = snd_pcm_hw_params_malloc (&hw_params);
    alsa_check_error(err, "Cannot allocate hardware parameter structure");
    
    err = snd_pcm_hw_params_any (nA->audio.pcm_handle, hw_params);
    alsa_check_error(err, "Cannot initialize hardware parameter structure");

    // Set access type
    err = snd_pcm_hw_params_set_access (nA->audio.pcm_handle, hw_params, access_type);
    alsa_check_error(err,"Cannot set access type");

	// Restrict configuration space to contain only real hardware rates
	err = snd_pcm_hw_params_set_rate_resample(nA->audio.pcm_handle, hw_params, 0);
    alsa_check_error(err,"Re-sampling setup failed for play back");
    
	// Set sample format (NOTE: needs changed if buffer type changed!)
	err = snd_pcm_hw_params_set_format(nA->audio.pcm_handle, hw_params, ALSA_PCM_FORMAT);
    alsa_check_error(err,"Cannot set sample format");
    
	// Set sample rate
	err = snd_pcm_hw_params_set_rate_near (nA->audio.pcm_handle, hw_params, &(nA->audio.format.sample_rate), 0);
    alsa_check_error(err,"Cannot set sample rate");    

	// Set channel count
    //nA->audio.format.channels = 2;
	err = snd_pcm_hw_params_set_channels (nA->audio.pcm_handle, hw_params, nA->audio.format.channels);
    alsa_check_error(err, "Cannot set channel count\n");
    
    // Setup by configuring buffer_time & period_time
    // Buffer_size is set automatically as result of buffer_time setting
    unsigned int period_time, buffer_time; 	// period time in us       
    period_time = (unsigned int)(1e6 * (nA->period_size/(1.0 * nA->audio.format.sample_rate)));        
    if(nA->is_master)
        buffer_time = 16 * period_time; // buffer size irrelevant for capture, so make it big to avoid overruns
    else
        buffer_time = 2 * period_time; 
    fprintf(stderr, "Configuring buffer and period times as %uus and %uus...\n", period_time, buffer_time);

    int dir = 0;
    
    // Set the period time
    dir = 0;
    err = snd_pcm_hw_params_set_period_time_near(nA->audio.pcm_handle, hw_params, &period_time, &dir);
    alsa_check_error(err, "Cannot set period time");        

    // Set the buffer time
    err = snd_pcm_hw_params_set_buffer_time_near (nA->audio.pcm_handle, hw_params, &buffer_time, &dir );
    alsa_check_error(err, "Cannot set buffer time");
    
    fprintf(stderr, "Period time is %uus, buffer time is %uus\n", period_time, buffer_time);

	// Set HW parameters
	err = snd_pcm_hw_params (nA->audio.pcm_handle, hw_params);
    alsa_check_error(err,"Cannot set parameters");

    // free the HW parameter structure
    snd_pcm_hw_params_free (hw_params);

	// Prepare audio interface for use
	//err = snd_pcm_prepare (nA->audio.pcm_handle);
    //alsa_check_error(err, "Cannot prepare audio interface for use");    
  
    // Set software parameters for the slave mode only
    if(!nA->is_master)
        alsa_set_sw_params(nA);
    
    // Prepare    
	err = snd_pcm_prepare (nA->audio.pcm_handle);
    alsa_check_error(err, "Cannot prepare audio interface for use");	
    DEBUG_LOG(STATUS, ALSA_OK, "ALSA device opened succesfully");

    err = snd_pcm_reset(nA->audio.pcm_handle);
    alsa_check_error(err, "Cannot reset PCM");

    // store the true buffer/period size
    err = snd_pcm_get_params(nA->audio.pcm_handle, &nA->audio.alsa_buffer_size, &nA->audio.alsa_period_size	); 	    
    alsa_check_error(err, "Cannot get params");
    fprintf(stderr, "*** ALSA PERIOD SIZE = %d frames, BUFFER SIZE = %d frames\n", (unsigned int)nA->audio.alsa_period_size, (unsigned int)nA->audio.alsa_buffer_size);

    nA->audio.period_ns = (long)((nA->audio.alsa_period_size / (1.0 * nA->audio.format.sample_rate)) * 1e9);

    snd_config_update_free_global();
}

// NOTE: test funcs assume simple sample formats and 1 channel

// NOTE: This is apparently too slow to run properly on the boards, not
// much use for testing as it can produce audible artifacts as a side-effect
void sine_signal(buffer_type *buffer, int buffer_len)
{
#if SAMPLE_BYTES != 3
    static int phase = 0;
    int i;    
    for(i=0;i<buffer_len;i++)
        buffer[i] = sin((2*M_PI*440.0*phase++)/48000.0) * 32767;    
#endif
}

void square_signal(buffer_type *buffer, int buffer_len) {
#if SAMPLE_BYTES != 3
    static int phase = 0;
    if(++phase % 2 == 0)
        memset(buffer, 0xAA, SAMPLE_BYTES * buffer_len);
    else
        memset(buffer, 0x11, SAMPLE_BYTES * buffer_len);
#endif
}

void saw_signal(buffer_type *buffer, int buffer_len)
{
#if SAMPLE_BYTES != 3
    static int phase=0;
    int i;
    for(i=0;i<buffer_len;i++)
    {    
        buffer[i] = (phase<<(6+((phase>>14)&3)));
        phase++;        
    }
#endif
}

void memory_signal(buffer_type *buffer, int buffer_len) {
#if SAMPLE_BYTES != 3
    static int dataLen = 0;
    static int offset = 0;

    if(wavData == NULL) {
        // this file is just raw PCM data, no WAV header
        FILE* f = fopen("/usr/share/wrapsine.wav", "rb");
        fseek(f, 0, SEEK_END);
        long sz = ftell(f);
        fseek(f, 0, SEEK_SET);
        wavData = (buffer_type*)malloc(sz);
        fprintf(stderr, "wav file is %ld bytes long\n", sz);
        int samples_to_read = sz / SAMPLE_BYTES;
        int samples_read = 0;
        while(samples_to_read > 0) {
            samples_read += fread(wavData + samples_read, SAMPLE_BYTES, samples_to_read, f);
            samples_to_read -= samples_read;
            fprintf(stderr, "Read %d %d samples of wave data\n", samples_read, samples_to_read);
        }
        fclose(f);
        dataLen = sz / SAMPLE_BYTES;
    }

    if(offset + buffer_len < dataLen) {
        memcpy(buffer, wavData + offset, buffer_len * SAMPLE_BYTES);
        offset += buffer_len;
    } else {
        int leftover = (dataLen - offset);
        memcpy(buffer, wavData + offset, leftover * SAMPLE_BYTES);
        if(buffer_len - leftover > 0)
            memcpy(buffer + leftover, wavData, (buffer_len - leftover) * SAMPLE_BYTES);
        offset = (buffer_len - leftover);
    }
#endif
}

