/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void event_log(FILE *fd, double time, const char *event, const char *msg,int state, char *format, ...)
{    
    va_list vargs;
    va_start(vargs, format);
    fprintf(fd, "%f, \"%s\", \"%s\", %d, \"{", time, event, msg, state);
    if(format)
    {
        vfprintf(fd, format, vargs);
    }    
    va_end(vargs, format);        
    fprintf(fd, "}\"\n");   
}

#define LOG_BASIC_EVENT(F,T,E) event_log(F,T,E,"",0,NULL);
#define LOG_CODED_EVENT(F,T,E,M) event_log(F,T,E,M,0,NULL);
#define LOG_EVENT(F,T,E,M,I,FMT,...) event_log(F,T,E,M,I,FMT,__VA_ARGS__);


