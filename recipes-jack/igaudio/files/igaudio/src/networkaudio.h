/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <iostream>
using namespace std;
#include <math.h>
#include <sched.h>
// Use the newer ALSA API
#define ALSA_PCM_NEW_HW_PARAMS_API
#define ALSA_PCM_NEW_SW_PARAMS_API
#include <alsa/asoundlib.h>
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/timeb.h>
#include <netinet/tcp.h>
#include <time.h>
#include <string.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>

#include "inih/ini.h"
#include "average.h"

#include "networkaudio-buffer.h"

struct audio_packet_header_t {
    unsigned int magic_number;          // IA_MSG_ID defined below
    unsigned int packet_number;         // sequential packet number
    unsigned int packet_length;         // total length (inc header) in bytes
    unsigned int data_size;             // audio data size (bytes)
};

#define RAW_HDR (sizeof(struct audio_packet_header_t))
typedef struct audio_packet_header_t AudioPacketHeader;

struct audio_packet_t {
    AudioPacketHeader header;
    buffer_type *frames;
};
typedef struct audio_packet_t AudioPacket;

extern "C" {
    #include "networkaudio-i2c.h"
    #include "networkaudio-ringbuffer.h"
}
#include "networkaudio-packetbuffer.h"
#include "networkaudio-hybridfilter.h"
#include "networkaudio-debug.h"

#define LIMIT(X,MIN,MAX) (((X)<(MIN))?(MIN):((X)>(MAX)?(MAX):(X)))

// magic number for the messages
const unsigned int IA_MSG_ID    = 0x01A4561D;

//#define DEBUG 1
#ifdef DEBUG
#define DEBUG_LOG_2(T,C,X, ...)  {if(nA->packet_number > 0) {fprintf(debug_fd, #T" "#C" %f: ",current_time()); fprintf(debug_fd, X, ##__VA_ARGS__); fprintf(debug_fd, "\n");}}
#define DEBUG_LOG(T,C,X, ...)  {fprintf(debug_fd, #T" "#C" %f: ",current_time()); fprintf(debug_fd, X, ##__VA_ARGS__); fprintf(debug_fd, "\n"); }//fflush(debug_fd);}
#else
#define DEBUG_LOG(T,C,X, ...) {}
#define DEBUG_LOG_2(T,C,X, ...) {}
#endif

extern FILE *debug_fd;

#define I2C_MUTE    { if(nA->audio.i2c_fd != -1) write_i2c_register(nA->audio.i2c_fd, 0x0d, nA->audio.i2c_reg | 0x03); }
#define I2C_UNMUTE  { if(nA->audio.i2c_fd != -1) write_i2c_register(nA->audio.i2c_fd, 0x0d, nA->audio.i2c_reg & 0xfc); }

// max size of one UDP packet (for allocating buffers)
#define MAX_PACKET_SIZE 2048

/*
    Collected statistics on the running of the process. These are
    never used for processing, merely for tracking perfomance during
    running. kill -HUP will print these out while the process is running.
*/
typedef struct timing_stats_t
{
    average_tracker_t slave_loop_time;  // average timing of the whole slave loop
    average_tracker_t master_loop_time; // average timing of the master transmit loop
    
    average_tracker_t receive_time;     // average timing of packet reading   
    average_tracker_t play_time;        // average timing of the PCM playback 
    average_tracker_t record_time;      // average timing of the PCM read 
    average_tracker_t transmit_time;    // average timing of the packet transmission 

    average_tracker_t transit_time;
    
    average_tracker_t queue_length;     // tracks the average length of the buffer queue
    int max_queue_latency;
    
    int packets_received;               // number of packets received over network
    int packets_sent;                   // number of packets transmitted over network
    int buffers_decoded;                // number of period_size buffers decoded
    int undecodeable_buffers;           // number of period_size undecodeable buffers
    int buffers_interpolated;           // number of period_size buffers interpolated
    
    int buffers_recorded;               // number of alsa_period_size frames recorded
    int buffers_played;                 // number of alsa_period_size frames played
    int network_send_errors;            // number of socket errors on transmission
        
    int record_overruns;                // total number of recording overruns
    int play_underruns;                 // total number of playback underruns
    int record_incomplete_buffers;      // total number of incomplete recorded buffers
    int record_alsa_errors;             // total number of other ALSA errors while recording
    
    int missed_packets;
    int out_of_sequence_packets;        // number of non-consecutive packets received 
    int duplicate_packets;              // number of duplicated packets received
    int duplicate_frames;               // number of duplicate codec frames received
    int receive_errors;                 // number of socket errors while reading
    int late_packets;                   // number of packets that came in late
    int invalid_packets;                // number of corrupted/undecodeable packets
    int nonstream_packets;              // number of packets received with non-matching stream_id
    
    int record_overtime;                // number of times the record loop took longer than scheduled
    int pcm_missed_buffers;             // number of times the PCM device was not ready when it should have been
    int pcm_failures;                   // number of other ALSA errors      
    
    double period_start;                // time interval at which the stats were reset

    int pcm_interval_checks_failed; 

    int packet_gaps_length;
    int *packet_gaps;
    int *packet_numbers;
    int packet_thresholds[4];
    int packet_bins[5];
} timing_stats_t;


/* Structure for holding all of the timestamps from a packet */
typedef struct packet_timestamp
{
    struct timespec packet_timestamp;
    struct timespec hw_raw_timestamp;
    struct timespec hw_sys_timestamp;
    struct timespec sw_timestamp;
} packet_timestamp;

/* Debugging states */
typedef struct debug_state_t
{
    double simulated_drop_rate;         // if nonzero, will simulate dropping that percentage of incoming packets
    int simulated_drop_interval;        // if nonzero, will drop every nth packet
    int timelog;                        // true if timing should be logged to a file
    int dummy_network;                  // true if networking disabled; only audio used
    int dummy_audio;                    // true if audio disabled
    char *test_signal_name;             // name of the used test signal
    char *log_directory;                // directory to write the logs to
    int sleeps;                         // if true, additional sleeps are used to try and improve timing performance
    void (*test_signal)(buffer_type *, int); // the test tone signal
    int test_mode;                      // if 1, send a test signal, not the real data
    char *opened_log_directory;         // the sub-directory which contains the .log and .yaml files
    buffer_type *last_audio; 
    unsigned last_audio_index;
    unsigned last_audio_len;
    buffer_type *last_recv;
    unsigned last_recv_index;
    unsigned last_recv_len;

    networkaudio_debug_entry *entries;
    unsigned entries_length;
    unsigned entries_index;
} debug_state_t;

typedef struct audio_format_t {
    unsigned int sample_rate;
    unsigned int channels;
    unsigned int sample_bytes;
} audio_format;

/* Audio configuration */
typedef struct audio_state_t
{
    snd_pcm_t *pcm_handle;              // PCM handle

    audio_format format;              // sample format settings

    int configure_buffer_time;          // if 1, configure ALSA using time, not sample size
    snd_pcm_uframes_t alsa_buffer_size; // buffer size ALSA actually gave us
    snd_pcm_uframes_t alsa_period_size; // period size ALSA actually gave us

    long period_ns;
    
    int i2c_fd;
    unsigned char i2c_reg;

    unsigned playback_packet;
} audio_state_t;

/* Network configuration */
typedef struct network_state_t
{
    char *remote_ip;                     // IP address of remote machine, as a string
    int using_broadcast;                // If true, use broadcasting
    int port_number;                        // UDP port
    int socket_fd;                         // Slave socket handle
    int socket_pc_fd;
    struct sockaddr_in remote_address;       // Slave IP address
    struct sockaddr_in pc_address;       // Slave IP address
    struct msghdr *message_header;      // message header structure for datagrams
} network_state_t;

/* Structure holding all parameters and persistent variables */
typedef struct network_audio_t
{
        
    unsigned packet_number;                 // sequential network packet number
    unsigned frame_number;                  // sequential codec frame number
    int period_size;                   // size of one audio frame

    int is_master;                      // if true, is the master                   
    
    audio_state_t audio;                // State of the ALSA related components
    network_state_t network;            // State of the socket related components
    timing_stats_t stats;               // Timing statistics
    debug_state_t debug;                // Debugging functionality    
    double start_time;                  // time the structure was created
    int running;                        // used to exit when Ctrl-C pressed
    FILE *low_latency_fd;               // used to ensure no processor low-power state switches happen (probably not needed)
    //na_ringbuffer *ringbuf;             // ringbuffer for slave to pass audio data around
    packetbuffer *packetbuf;

    // adaptive buffering stuff
    int adaptive_buffering;
    unsigned initial_buffers;
    unsigned max_buffers;
    unsigned reduce_to_buffers;
    unsigned cur_buffers;
    unsigned us_since_last_glitch;
    struct timespec last_glitch_time;
    unsigned short buffers_added;
    unsigned short buffers_removed;
    struct timespec last_remove_time;
    unsigned muted_run;

    int using_filtering;
    unsigned filter_order;
    unsigned filter_size;
    unsigned filter_autocorr_size;
} network_audio_t;

/* ALSA functions */
unsigned alsa_period_buffer_size_bytes(network_audio_t *nA);
void alsa_check_error(int err, char *msg);
void alsa_init(network_audio_t *nA);
void alsa_close(network_audio_t *nA);

int alsa_xrun_recovery(snd_pcm_t *handle, int err);

/* Utility functions */
void error(char *msg);
void warn(char *msg);

void help(void);
double uniform_double(void);

void *master_thread(void *ptr);

void *slave_thread_blocking(void *ptr);
void *slave_audio_thread_rw(void *ptr);

int put_received_packet(network_audio_t *nA, AudioPacketHeader *header, buffer_type *buffer);
int get_next_packet(network_audio_t *nA, buffer_type *buffer, unsigned *packet_number);
int drop_packet(network_audio_t *nA);

/* Test tones */
void sine_signal(buffer_type *buffer, int buffer_len);
void square_signal(buffer_type *buffer, int buffer_len);
void saw_signal(buffer_type *buffer, int buffer_len);
void memory_signal(buffer_type *buffer, int buffer_len);

/* Return a timespec as floating point seconds */
double float_time(struct timespec *t);
double current_time();

/* 
    Create a receive buffer to receive a packet
    and set the message_header structure to write into
    that packet 
*/
struct msghdr *get_message_header();


/* Add a control block to the message header structure, for
   receiving ancilliary packet information such as timestamps */
void add_control_header(struct msghdr *message_header);

unsigned network_packet_size(network_audio_t *nA);

/*  
    Turn on high precision timestamping on the socket, and
    force the socket to be high priority.
*/
void enable_timestamping_high_priority(int sockfd, int is_master);


/* Return the timestamp (as a timespec) of the last packet recieved via ioctl */
void last_packet_timestamp(int sockfd, struct timespec *ts);

/* 
    Parse control messages in a message header and fill the passed
   timestamp structure. Returns the software time (SO_TIMESTAMPNS),
   and the hardware raw/converted times (SO_TIMESTAMPING)
 */
void timestamp_from_cmsg(struct msghdr *msg, packet_timestamp *timestamp);


/* Compute the CRC32 of a memory block */
unsigned long crc32(unsigned long inCrc32, const void *buf, size_t bufLen );

void record_packet_gap(network_audio_t *nA, int packet_number, long gap);
void dump_packet_gap_stats(network_audio_t *nA);

/* Set the current process priority and scheduler to realtime */
void realtime_priority(int priority, int schedtype);
void setup_priority(void);

/* 
    Try and parse a string as an integer. If it cannot be 
    parsed, exit with an error. Otherwise, return the parsed
    integer.
*/    
int check_integer(char *str);

/*
    Return 1 if the string is "truthy", otherwise 0
*/
int is_true(char *str);

/* 
   Return a random number on the interval [0,1].
   Not a very good random number, but probably OK for 
   simple uses.
*/
double uniform_double(void);
