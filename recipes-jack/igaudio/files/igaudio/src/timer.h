/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>



/* Structure for a loop timer logger */
typedef struct ustimer_t
{
    FILE *logfile;
    struct timespec start_time, last_time;
    int writes_since_flush;
} ustimer_t;

/* Timer functions */
struct timespec timer_start();
long timer_end(struct timespec start_time);
long timer_end_ms(struct timespec start_time);

ustimer_t *open_ustimer(char *fname);
void loop_ustimer(ustimer_t *timer);
void close_ustimer(ustimer_t *timer);  

void tsnorm(struct timespec *ts);

/* Return the current time in floating seconds */
double current_time();


char *timelog_fname(int type, struct network_audio_t *nA);

#define T_START(T) T=current_time()
#define T_END(T) T=current_time()-T

#define LOG_TYPE_MASTER 0
#define LOG_TYPE_SLAVE_NETWORK 1
#define LOG_TYPE_SLAVE_AUDIO 2

