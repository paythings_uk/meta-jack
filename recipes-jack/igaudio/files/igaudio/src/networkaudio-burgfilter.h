/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef BURGFILTER_H
#define BURGFILTER_H

#include "networkaudio-buffer.h"

void burg_init(unsigned order, unsigned filter_size, unsigned autocorr_buffer_size);
void burg_cleanup();

int burg_process_buffer(buffer_type *buffer, unsigned buffer_size, int buffer_ready);

filter_type energy_FLP(const buffer_type *data, int data_size);
filter_type inner_product_FLP(const buffer_type *data1, const buffer_type *data2, int data_size);
filter_type silk_burg_modified_FLP(          /* O    returns residual energy                                     */
        filter_type     A[],                /* O    prediction coefficients (length order)                      */
        const buffer_type x[],                /* I    input signal, length: nb_subfr*(D+L_sub)                    */
        const filter_type min_inv_gain,         /* I    minimum inverse prediction gain                             */
        const int      subfr_length,       /* I    input signal subframe length (incl. D preceding samples)    */
        const int      nb_subfr,           /* I    number of subframes stacked in x                            */
        const int      D                   /* I    order                                                       */
);

void burg_downsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned ds_buffer_size, unsigned step);
void burg_upsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned ds_buffer_size, unsigned step);

#endif // BURGFILTER_H
