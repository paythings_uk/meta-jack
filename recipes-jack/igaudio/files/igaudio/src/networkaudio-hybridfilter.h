/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef HYBRIDFILTER_H
#define HYBRIDFILTER_H

#include "networkaudio-buffer.h"
#include "networkaudio-oscillation.h"


void hybrid_init(unsigned order, unsigned samplesPerBuffer, unsigned autoCorrBufferSize);
void hybrid_cleanup();

int hybrid_process_buffer(buffer_type *buffer, unsigned buffer_size, int buffer_ready);

int hybrid_merge_buffer(buffer_type *buffer, unsigned buffer_size, int buffer_ready);
int hybrid_ramp_to_zero(buffer_type *buffer, unsigned buffer_size);

    filter_type energy_FLP(const buffer_type *data, int   dataSize);
    filter_type inner_product_FLP(const buffer_type *data1, const buffer_type *data2, int dataSize);
    filter_type silk_burg_modified_FLP(          /* O    returns residual energy                                     */
        filter_type     A[],                /* O    prediction coefficients (length order)                      */
        const buffer_type x[],                /* I    input signal, length: nb_subfr*(D+L_sub)                    */
        const filter_type minInvGain,         /* I    minimum inverse prediction gain                             */
        const int      subfr_length,       /* I    input signal subframe length (incl. D preceding samples)    */
        const int      nb_subfr,           /* I    number of subframes stacked in x                            */
        const int      D                   /* I    order                                                       */
        );

void hybrid_downsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned ds_buffer_size, unsigned step);
void hybrid_upsample(buffer_type *inbuf, unsigned buffer_size, buffer_type *outbuf, unsigned ds_buffer_size, unsigned step);

#endif // HYBRIDFILTER_H
