/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef NETWORKAUDIO_PACKETBUFFER_H
#define NETWORKAUDIO_PACKETBUFFER_H

#include "sglib.h"
#include <stdlib.h>
#include <stdio.h>

#include "networkaudio-buffer.h"
#include "networkaudio-burgfilter.h"

typedef struct pbslot {
        buffer_type *buffer;     
        unsigned packet_number;
        int used;
        unsigned extra_size;
        struct pbslot *next, *prev;
} jbslot;

#define PB_COMPARATOR(e1, e2) (e1->packet_number-e2->packet_number)

typedef struct packetbuffer
{
    pbslot *activeq;
    pbslot *poolq;
    int buffer_size;        // total buffer size
    int audio_size;         // size used by audio data
    int id_ctr;
} packetbuffer;

/* Create a buffer queue with a given size */
packetbuffer *init_packetbuffer(int buffer_size_samples, int audio_size_samples, int initial_pool);

/* Free a buffer queue structure */
void free_packetbuffer(packetbuffer *buffer);

/* Clear and free the entire queue */
void clear_packetbuffer(packetbuffer *buffer);

/* (re)set the size of each buffer in the queue, clearing the
   queue if needed */
void set_packetbuffer_size(packetbuffer *buffer, int size, int audio_size);

/* Push a newly allocated buffer onto the pool */
void addpool_packetbuffer(packetbuffer *buffer);

/* Move a pool buffer into the head of the active queue. Return the buffer of that queue element. */
buffer_type *enque_packetbuffer(packetbuffer *buffer);

/* Undo an enque */
void undo_enque_packetbuffer(packetbuffer *buffer);

/* Pop the tail of the queue. Return the buffer it points to. */
buffer_type *deque_packetbuffer(packetbuffer *buffer);

/* Expand the pool by n buffers */
void expandpool_packetbuffer(packetbuffer *buffer, int n);

/* Print out the state of the queue */
void print_packetbuffer(packetbuffer *buffer);

/* Return the last buffer pushed onto the active queue */
buffer_type *last_added_packetbuffer(packetbuffer *buffer);

/* Return the length of the active q */
int activeq_length_packetbuffer(packetbuffer *buffer);

/* Return the ix'th buffer that previously came off the active queue;
  that is the ix'th buffer of the pool queue, starting from the head.
  0 is the last buffer played; 1 is the 2nd last, and so on.
  Returns NULL if no such buffer.
  */
buffer_type *previous_packetbuffer(packetbuffer *buffer, int ix);

unsigned latency_packetbuffer_periods(packetbuffer *buffer);
/* Return the latency of the queue (i.e. length of the active q, in samples) */
unsigned latency_packetbuffer(packetbuffer *buffer);

/* Print the active q, including the gains, and whether or not packets were received. */
void print_activeq(packetbuffer *buffer);

/* Enque from a buffer of a given size, feeding partial
   buffers as required. This copies the entire buffer
   into the active queue (partial remnants remain in the pool).
   Should *not* be mixed with raw en/deque_packetbuffer() calls unless you 
   are sure what you are doing!.
*/
int push_packetbuffer(packetbuffer *buffer, buffer_type *in, int in_length, unsigned packet_number, int has_extra, unsigned extra_size);

/* Deque into a buffer of a given size, feeding partial
   buffers as required. May leave a partial buffer in the active queue.
   Returns the number of bytes writen, which wil == in_length if there
   were enough buffers in the active queue to satisfy the request.
   Should *not* be mixed with raw en/deque_packetbuffer() calls unless you 
   are sure what you are doing!.
*/
int pop_packetbuffer(packetbuffer *buffer, buffer_type *out, int out_length, unsigned *packet_number, void *extra, unsigned *extra_size);

int drop_packetbuffer(packetbuffer *buffer);

/* 
   Add a new frame to the buffer, copying the data and setting the interpolated state,
   the frame number and the gain
*/
void add_packetbuffer(packetbuffer *buffer, buffer_type *in, unsigned packet_number);

/*
    Stuff an uninitialised buffer into the queue 
*/
void stuff_packetbuffer(packetbuffer *buffer);

/* Return the next *complete* (i.e. ptr==0) buffer from the active queue,
   or NULL, if there is no such one. */
buffer_type *pop_complete_packetbuffer(packetbuffer *buffer);

void test_packetbuffer();
void test_partial_packetbuffer();

#endif
