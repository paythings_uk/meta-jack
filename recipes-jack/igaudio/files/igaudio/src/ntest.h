/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <getopt.h>
#include <sys/mman.h>
#include <signal.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#include <math.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/timeb.h>
#include <netinet/tcp.h>
#include <time.h>
#include <string.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/socket.h>
#include <linux/sockios.h>
#include <linux/ioctl.h>
#include <asm/socket.h>
#include <asm-generic/socket.h>
#include <asm-generic/sockios.h>
#include <linux/sockios.h>
#include <sys/ioctl.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
//#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/sockios.h>
#include <sched.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <time.h>

#define MAX_PACKETS 400000

double current_time();
void help(void);

/* Structure for holding all of the timestamps from a packet */
typedef struct packet_timestamp
{
    struct timespec packet_timestamp;
    struct timespec hw_raw_timestamp;
    struct timespec hw_sys_timestamp;
    struct timespec sw_timestamp;
    struct timespec sent_timestamp;
} packet_timestamp;


typedef struct packet_info
{
    double time;
    double send_time;
    double dt;
    int id;
} packet_info;

typedef struct network_test_t 
{
    int is_recv;        // true if receiving packets
    char *remote_ip;    // address we are sending to
    int frame_size;     // size of each frame, in bytes
    double rate;        // rate of transmission, in packets/second
    int kbps;           // total data rate in kbps
    double loop_time;   // expected time for each loop
    int sockfd;         // socket handle
    int blocking;       // true if blocking
    struct msghdr *message_header;
    struct sockaddr_in server_address;       // Slave IP address
    int port;
    int packet_number;          // current packet_number
    int n_packets;              // number of packets to transmit
    int seconds;                // duration of test, in seconds
    packet_info *packets;
    double timestamp;           // timestamp of last packet
    int n_packets_recorded;     // number of packets recorded
    int packets_lost;           // number of packets lost
    int duplicate_packets;      // number of duplicate packets
    int timing_set;             // if timing is set for this recv loop
    char *output_dir;
    char *output_tag; 
    int broadcast;
	int ignore;
} network_test_t;
