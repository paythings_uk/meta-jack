/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"
#include "timer.h"
#include <sched.h>
#include <poll.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <linux/if_packet.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include "networkaudio-hybridfilter.h"

using namespace std;

// states that the slave can be in, depending on the behaviour 
// of the incoming packet stream
enum states {
    STATE_START,            // waiting for first packets to arrive
    STATE_NORMAL,           // normal playback; packets arriving as expected
    STATE_MISSED_PACKET,    // latest packet failed to arrive; soft mute instead
    STATE_MUTED,            // totally muted playback (2+ consecutive packets missed)
};
 
// adaptive buffer constants:
// - minimum time in ms that must elapse between adding new buffers to the queue
#define ADD_BUFFER_INTERVAL_MS      5000
// - minimum time in ms that must elapse between removing buffers from the queue
#define REMOVE_BUFFER_INTERVAL_MS   30000
// - minimum time in ms that must elapse without glitches before buffers can be
//  removed from the queue
#define GLITCH_INTERVAL_MS          30000

/*
    Open the socket connection to the given remote IP.
    Sets nA->network.socket_fd (the socket filehandle) and   
    nA->network.server_address (the IP address, as a 32 bit int).
    
    If <nonblocking> is nonzero creates a nonblocking socket.
*/ 

// AC - 12/02/17 Adding var to track the last packet played
unsigned long last_pckt_played = -1;

static void slave_open_socket(network_audio_t *nA, int nonblocking)
{   
    int sockfd;
    struct sockaddr_in  remote_addr;
    // new UDP socket
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    // setup ancilliary headers for timestamps etc
    nA->network.message_header = get_message_header();
    add_control_header(nA->network.message_header);
   
    if (sockfd < 0)
       DEBUG_LOG(ERROR, SLAVE_SOCKET_ERROR, "Error opening socket: %s", strerror(errno));		

    if(nonblocking)
        fcntl(sockfd, F_SETFL, O_NONBLOCK);
    enable_timestamping_high_priority(sockfd, nA->is_master);

	// set a shortish timeout on the socket so we don't get stuck in
	// recvmsg() calls when Ctrl-C is pressed (easier to tidy up this way)
	struct timeval timeout = { 0 };
	timeout.tv_sec = 0;
	timeout.tv_usec = 500000;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (void*)&timeout, sizeof(struct timeval));

    bzero((char *) &remote_addr, sizeof(remote_addr));
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_addr.s_addr = INADDR_ANY;
    remote_addr.sin_port = htons(nA->network.port_number);

    if (bind(sockfd, (struct sockaddr *) &remote_addr, sizeof(remote_addr)) < 0)		
        DEBUG_LOG(ERROR, SLAVE_BIND_ERROR, "Error on binding to address: %s", strerror(errno));		

    nA->network.remote_address = remote_addr;
    nA->network.socket_fd = sockfd;
    DEBUG_LOG(STATUS, SLAVE_SOCKET_OK, "Slave socket opened succesfully.");
}

static int read_raw_packet(network_audio_t *nA, packet_timestamp *packet_timing)
{
    ssize_t n = -1;
    int no_packet = 1;
    AudioPacketHeader *audio_packet = (AudioPacketHeader*)(nA->network.message_header->msg_iov[0].iov_base);
    
    n = recvmsg(nA->network.socket_fd, nA->network.message_header, 0);
    no_packet =  (n==-1) && (errno==EAGAIN || errno==EWOULDBLOCK  || errno == EINTR);            
    
    if (!no_packet && n < 0)
    {
        // this is an error condition *other* than no packet waiting
        DEBUG_LOG(ERROR, PACKET_RECEIVE_ERROR, "recv() failed: %s", strerror(errno));
        return 0;
    }
    if(n > 16)
    {   
        timestamp_from_cmsg(nA->network.message_header, packet_timing);
        //update_average_tracker_delta(&(nA->stats.receive_time), float_time(&(packet_timing->sw_timestamp)));

        if(n!=(int)(audio_packet->packet_length))
        {
            DEBUG_LOG(WARN, PACKET_WRONG_LEN, "Packet with wrong length %d (received %d total)", audio_packet->packet_length, n);
            return 0;
        }
    }   

    if(no_packet)
        return 0;

    return 1;
}

inline long ts_sub(struct timespec *s, struct timespec *e) 
{
    return ((e->tv_sec - s->tv_sec) * 1000000) + ((e->tv_nsec - s->tv_nsec) / 1000);
}

unsigned last_packet = 0;
long last_packet_gap = 0;

// this is the function for receiving packets from the master device.
// launches a new thread to handle audio playback once packets start arriving.
void *slave_thread_blocking(void *ptr)
{
    network_audio_t *nA = (network_audio_t *)ptr;  

    // set scheduler and priority
    realtime_priority(50, SCHED_FIFO);

    // setup a blocking UDP socket
    slave_open_socket(nA, 0); 

    nA->packet_number = 0;

    int packetbuf_size = nA->audio.alsa_period_size;
    nA->packetbuf = init_packetbuffer(packetbuf_size, nA->audio.alsa_period_size, 64);
    
    // setup pthread variables ready to launch the audio thread
    pthread_t audio_thread;
    pthread_attr_t audio_attr;
    pthread_attr_init(&audio_attr);
    pthread_attr_setinheritsched(&audio_attr, PTHREAD_INHERIT_SCHED);

	packet_timestamp packet_timing;
    struct msghdr *hdr = nA->network.message_header;

    // pointer to start of packet data in the receive buffer
    AudioPacketHeader *header = (AudioPacketHeader*)(hdr->msg_iov[0].iov_base);
    // pointer to start of audio data in the receiver buffer
    buffer_type *frame_buffer = (buffer_type*)((char*)hdr->msg_iov[0].iov_base + RAW_HDR);

    int have_packet = 0;
    struct timespec packet_gap = { 0 };

    pthread_create(&audio_thread, &audio_attr, slave_audio_thread_rw, (void*)nA);

    while(nA->running) 
    {
        // wait for a packet to arrive
        have_packet = read_raw_packet(nA, &packet_timing);

        if(have_packet) 
        {
            // record gap from last packet (for debugging only)
            if(packet_gap.tv_nsec != 0) 
            {
                last_packet_gap = ((packet_timing.packet_timestamp.tv_sec - packet_gap.tv_sec) * 1000000)
                            + ((packet_timing.packet_timestamp.tv_nsec - packet_gap.tv_nsec) / 1000.0);
                //DEBUG_LOG(TIMING, SWTIME, "%d @ %ld us", header->packet_number, diff);
                record_packet_gap(nA, header->packet_number, last_packet_gap);
            }
            packet_gap.tv_sec = packet_timing.packet_timestamp.tv_sec;
            packet_gap.tv_nsec = packet_timing.packet_timestamp.tv_nsec;

            if(nA->debug.last_recv) {
                if(nA->debug.last_recv_index + nA->audio.alsa_period_size > nA->debug.last_recv_len) {
                    int remaining = nA->debug.last_recv_len - nA->debug.last_recv_index;
                    memcpy(nA->debug.last_recv + nA->debug.last_recv_index, frame_buffer, remaining * sizeof(buffer_type));
                    memcpy(nA->debug.last_recv, frame_buffer + remaining, (nA->audio.alsa_period_size - remaining) * sizeof(buffer_type));
                    nA->debug.last_recv_index = nA->audio.alsa_period_size - remaining;
                } else {
                    memcpy(nA->debug.last_recv + nA->debug.last_recv_index, frame_buffer, nA->audio.alsa_period_size * sizeof(buffer_type));
                    nA->debug.last_recv_index += nA->audio.alsa_period_size;
                }
            }

            last_packet = header->packet_number;

            if(nA->stats.packets_received == 0)
                nA->audio.playback_packet = 0;

            // check if the audio stream is expecting a packet number greater than the one we
            // just received. If so, it's arrived too late and should be discarded... BUT if the
            // gap between expected and received is more than a few seconds worth of packets, it
            // almost certainly means the stream has been restarted or something similar, so 
            // resume playback from there if that seems to be the case.
            if(nA->audio.playback_packet > header->packet_number && (abs((long)(nA->audio.playback_packet - header->packet_number)) < 400))
            {
                //fprintf(stderr, "IGNORE %d > %d\n", nA->audio.playback_packet, header->packet_number);
                DEBUG_LOG(PACKET, IGNORE, "ignoring packet %u because audio thread expecting packet %u", header->packet_number, nA->audio.playback_packet);
            } 
            else if(nA->stats.packets_received < 100) 
            {
                DEBUG_LOG(PACKET, IGNORE, "ignoring packet received %d < 100", nA->stats.packets_received);
            }
            else
            {
                //DEBUG_LOG(PACKET, >>>, "inserting packet %u", header->packet_number);
                put_received_packet(nA, header, frame_buffer);
            }

            nA->packet_number = header->packet_number;
            nA->stats.packets_received++;
        } 
        else 
        {
            // something went wrong, bad packet length etc
            nA->stats.missed_packets++;
        }
    }
    printf("Network thread main loop done, waiting for audio thread...\n");
        pthread_join(audio_thread, NULL);
        printf("tryjoin = %d\n", pthread_tryjoin_np(audio_thread, NULL));
    printf("Audio thread joined\n");

    free_packetbuffer(nA->packetbuf);

    //close(nA->network.socket_fd);
    fprintf(stderr, "\n\n**** SLAVE THREAD EXITING ****\n\n");
	return NULL;
}

/* found online, apparently produces very fast ARM
 * code for integer square roots... */
#define iter1(N) \
    try_ = root + (1 << (N)); \
    if (n >= try_ << (N))   \
    {   n -= try_ << (N);   \
        root |= 2 << (N); \
    }

unsigned armsqrt (unsigned n)
{
    unsigned root = 0, try_;
    iter1 (15);    iter1 (14);    iter1 (13);    iter1 (12);
    iter1 (11);    iter1 (10);    iter1 ( 9);    iter1 ( 8);
    iter1 ( 7);    iter1 ( 6);    iter1 ( 5);    iter1 ( 4);
    iter1 ( 3);    iter1 ( 2);    iter1 ( 1);    iter1 ( 0);
    return root >> 1;
}

unsigned rms_buffer(buffer_type *buffer, unsigned buffer_length) {
    unsigned rms = 0;

    for(unsigned i=0;i<buffer_length;i++) 
        rms += (buffer[i] * buffer[i]);

    return armsqrt(rms);
}

int process_buffer_basic(network_audio_t *nA, int state, buffer_type *playback_buffer, int playback_buffer_size, int buffer_ready, unsigned packet_number) 
{
    //DEBUG_LOG(PACKET, PLAY, "buffer_ready = %d, read_packet = %u", buffer_ready, read_packet);
    switch(state) 
    {
        case STATE_NORMAL: // last packet arrived on time
            if(buffer_ready) 
            {
                // -> play buffer as normal
            }
            else
            {
                DEBUG_LOG(PACKET, MISSING, "packet %u is missing", nA->audio.playback_packet);
                state = STATE_MISSED_PACKET;
                nA->stats.play_underruns++;
            }
            break;
       case STATE_MISSED_PACKET: // last packet was missed
            if(buffer_ready)
            {    
                DEBUG_LOG(PACKET, FOUND, "packet %u found", packet_number);
                state = STATE_NORMAL;
            } 
            else
            {
                // (2 or more consecutive missed packets)
                DEBUG_LOG(PACKET, MUTING, "missed packet %u, now muting", nA->audio.playback_packet);
                // -> play silent buffer
                memset(playback_buffer, 0, playback_buffer_size);
                state = STATE_MUTED;
                nA->stats.play_underruns++;
            }
            break;
        case STATE_MUTED: // already playing silent buffers
            if(buffer_ready)
            {
                DEBUG_LOG(PACKET, UNMUTING, "found packet %u, resuming playback", packet_number);
                state = STATE_NORMAL;
            } 
            else
            {
                DEBUG_LOG(PACKET, MUTED, "missing packet %u, still muted", nA->audio.playback_packet);
                // -> continue playing the muted buffer
                nA->stats.play_underruns++;
                memset(playback_buffer, 0, playback_buffer_size);
            }
            break;
        case STATE_START:
            memset(playback_buffer, 0, playback_buffer_size);
            break;
        default:
            fprintf(stderr, "Unknown state %d\n", state);
            break;
    }
    return state;
}

int process_buffer_filter(network_audio_t *nA, int state, buffer_type *playback_buffer, int playback_buffer_size, int buffer_ready, unsigned packet_number) 
{
    if(buffer_ready)
        nA->muted_run = 0; // reset this every time a packet arrives

    switch(state) 
    {
        case STATE_NORMAL: // last packet arrived on time
            if(buffer_ready) 
            {
				//if( packet_number == last_pckt_played || last_pckt_played == -1) // then all is well
				//{
		            // -> play buffer as normal (this won't do any prediction or
		            // change the output)
		            //DEBUG_LOG(LPC, NORMAL_1, "nothing to do");
		            hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
					
					last_pckt_played = packet_number;
				//}
				//else // we've skipped a packet so merge
				//{
               	//	hybrid_merge_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);		
				//	last_pckt_played = packet_number;			
				//}
            }
            else
            {
                DEBUG_LOG(PACKET, MISSING, "packet %u is missing", nA->audio.playback_packet);
                state = STATE_MISSED_PACKET;
                nA->stats.play_underruns++;
                // this will attempt to fill in the missing data
                DEBUG_LOG(LPC, NORMAL_2, "filling in buffer");
                hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
            }
            break;
       case STATE_MISSED_PACKET: // last packet was missed
            if(buffer_ready)
            {    
                DEBUG_LOG(PACKET, FOUND, "packet %u found", packet_number);
                state = STATE_NORMAL;
                // this will attempt to crossfade the old predicted data from
                // the last frame into the newly arrived frame
                DEBUG_LOG(LPC, MISSED_1, "crossfade");
                hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
				
				last_pckt_played = packet_number;
            } 
            else
            {
                // (2 or more consecutive missed packets)
                // this will attempt to fill in missing data
                DEBUG_LOG(LPC, MISSED_2, "filling in buffer");
                hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
                state = STATE_MUTED;
                nA->muted_run = 1; // start of run of muted packets
                nA->stats.play_underruns++;
            }
            break;
        case STATE_MUTED: // already playing silent buffers
            if(buffer_ready)
            {
                state = STATE_NORMAL;
                // this will attempt to crossfade the old predicted data from
                // the last frame into the newly arrived frame
                DEBUG_LOG(LPC, MUTED_1, "crossfade");
                hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
				
				last_pckt_played = packet_number;
            } 
            else
            {
                nA->stats.play_underruns++;
                nA->muted_run++;
				if(nA->muted_run == 8)
				{				
					hybrid_ramp_to_zero(playback_buffer, nA->audio.alsa_period_size);
				}
				else if(nA->muted_run >= 8) // randomly chosen value
                {
                    // just keep muting the buffer until we get some real data again
                    DEBUG_LOG(LPC, MUTED_REAL, "really mute things %d consecutive", nA->muted_run);
                    memset(playback_buffer, 0, sizeof(buffer_type) * nA->audio.alsa_period_size);
                }
                else
                {
                    // this will attempt to fill in missing data
                    DEBUG_LOG(LPC, MUTED_2, "filling in buffers");
                    hybrid_process_buffer(playback_buffer, nA->audio.alsa_period_size, buffer_ready);
                }
            }
            break;
        case STATE_START:
            memset(playback_buffer, 0, playback_buffer_size);
            break;
        default:
            fprintf(stderr, "Unknown state %d\n", state);
            break;
    }
    return state;
}

// thread function for dealing with audio playback, launched from slave_thread_blocking 
void *slave_audio_thread_rw(void *ptr)
{
    network_audio_t *nA = (network_audio_t *)ptr;  

    // set scheduler and priority
    realtime_priority(50, SCHED_FIFO);

    double loop_time, audio_time;         
    int err = 0, first = 1;
    unsigned playback_buffer_size = alsa_period_buffer_size_bytes(nA);
    buffer_type *playback_buffer = (buffer_type*)malloc(playback_buffer_size);

    struct timespec audio_gap;
    // number of frames that should be available before playing a new buffer
    int avail_threshold = nA->audio.alsa_period_size; 

    int fprintfs = 0;

    struct timespec second_chance_time;
    second_chance_time.tv_sec = 0;
    second_chance_time.tv_nsec = (long)(((nA->audio.alsa_period_size * 0.75) / (nA->audio.format.sample_rate)) * 1000000000);
    fprintf(stderr, "SECOND CHANCE TIME: %ld\n", second_chance_time.tv_nsec);
    int second_chance_avail = (int)(nA->audio.alsa_period_size * 1.75);

    // current playback state
    int state = STATE_START;

    // latency debug
    int dbgsock = -1;
    struct sockaddr_in remote_addr;
    if(nA->debug.entries) 
    {
        dbgsock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        struct hostent *master_server = gethostbyname("192.168.0.13");
        int x = fcntl(dbgsock, F_GETFL, 0);
        fcntl(dbgsock,F_SETFL, x | O_NONBLOCK);   
        bzero((char *) &remote_addr, sizeof(remote_addr));
        remote_addr.sin_family = AF_INET;
        bcopy((char *)master_server->h_addr,
             (char *)&(remote_addr.sin_addr.s_addr),
             master_server->h_length);
        remote_addr.sin_port = htons(22222);
    }
    
    // start off with current buffer queue length == initial setting
    nA->cur_buffers = nA->initial_buffers;
    nA->reduce_to_buffers = nA->cur_buffers + 1;

    nA->last_glitch_time = timer_start();
    nA->last_remove_time = timer_start();
    nA->muted_run = 0; // number of consecutive muted buffers

    int last_q_len = nA->cur_buffers;

    fprintf(stderr, "Audio thread launching!\n");
    DEBUG_LOG(EVENT, PLAYBACK, "Audio thread launching playback");
    while(nA->running)
    {
        T_START(loop_time);

        err = snd_pcm_avail(nA->audio.pcm_handle);
        if(err < 0) 
        {
            DEBUG_LOG(WARN, AVAIL_ERROR, "%d %s", err, snd_strerror(err));
            nA->stats.pcm_failures++;
            err = alsa_xrun_recovery(nA->audio.pcm_handle, err);
            first = 1;
            if(err < 0)
                continue;
            err = snd_pcm_avail(nA->audio.pcm_handle);
        }

        //DEBUG_LOG(EVENT, AVAIL, "%d, STATE %d", err, snd_pcm_state(nA->audio.pcm_handle));
        if(err < avail_threshold)
        {
            if(first)
            {
                first = 0;
                fprintf(stderr, "restarting PCM\n");
                err = snd_pcm_start(nA->audio.pcm_handle);
                if(err < 0) 
                {
                    fprintf(stderr, "start err %s\n", snd_strerror(err));
                    exit(-1);
                }
            } 
            else 
            {
                err = snd_pcm_wait(nA->audio.pcm_handle, -1);
                if(err < 0) 
                {
                    fprintf(stderr, "xrun recover\n");
                    err = alsa_xrun_recovery(nA->audio.pcm_handle, err);
                    if(err < 0) 
                    {
                        fprintf(stderr, "start err %s\n", snd_strerror(err));
                        exit(-1);
                    }
                    first = 1;
                }
            }
            continue;
        }

        T_START(audio_time);
        unsigned read_packet = 0;
        int buffer_ready = 0;

        //DEBUG_LOG(LOOP, START, "___________________________ %d", state);
        
        // only start looking for packets when sufficient number buffered in the queue 
        // (and do this regardless of whether adaptive buffering is enabled)
        if(state == STATE_START || state == STATE_MUTED || state == STATE_MISSED_PACKET) 
        {
            unsigned limit = 1 + (nA->adaptive_buffering ? nA->cur_buffers : nA->initial_buffers);
            //DEBUG_LOG(LOOP, RECOVERY, "%u %u", latency_packetbuffer_periods(nA->packetbuf), limit);

            if(latency_packetbuffer_periods(nA->packetbuf) > limit)
            {
                if(fprintfs) fprintf(stderr, "FIRST\n");
                
                unsigned queue_len = latency_packetbuffer_periods(nA->packetbuf);
                DEBUG_LOG(RESUME, BUFLEN, "=%d/%d", queue_len, nA->cur_buffers);

                // occasionally (after a big glitch or something) might wind up with 
                // too many packets at this point, and if we don't do anything it'll 
                // end up adding dozens of buffers, which is not good. So make sure
                // any excess packets are dropped from the queue before continuing.
                if(queue_len > limit)
                {
                    for(unsigned i=0;i<queue_len - limit;i++) 
                    {
                        get_next_packet(nA, playback_buffer, &read_packet);
                        DEBUG_LOG(RESUME, DISCARDED, "packet %d discarded", read_packet);
                    }
                }
            }

            buffer_ready = get_next_packet(nA, playback_buffer, &read_packet); 
            if(buffer_ready) 
            {
                nA->audio.playback_packet = read_packet;
                DEBUG_LOG(PACKET, FIRSTAUDIO, "first audio packet = %u\n", read_packet);
                if(fprintfs) fprintf(stderr, "GOT %d from queue\n", read_packet);
                state = STATE_NORMAL;
            }
        }
        
        if(state == STATE_NORMAL && !buffer_ready)
        {
            // occasionally (after a big glitch or something) might wind up with 
            // too many packets at this point, and if we don't do anything it'll 
            // end up adding dozens of buffers, which is not good. So make sure
            // any excess packets are dropped from the queue before continuing.
            
            unsigned limit = 1 + (nA->adaptive_buffering ? nA->cur_buffers : nA->initial_buffers);
            unsigned queue_len = latency_packetbuffer_periods(nA->packetbuf);
            //DEBUG_LOG(LOOP, NORMAL, "%u %u", latency_packetbuffer_periods(nA->packetbuf), limit);
            
            if(queue_len > limit)
            {
                DEBUG_LOG(PLAY, BUFLEN, "=%d/%d", queue_len, nA->cur_buffers);
                for(unsigned i=0;i<queue_len - limit;i++) 
                {
                    get_next_packet(nA, playback_buffer, &read_packet);
                    DEBUG_LOG(RESUME, DISCARDED, "packet %d discarded", read_packet);

                }
            }

            buffer_ready = get_next_packet(nA, playback_buffer, &read_packet);
            //DEBUG_LOG(PACKET, <<<, "buffer = %d, packet = %u, err = %d", buffer_ready, read_packet, err);
            // can wait for some time here to see if the packet is just a little late, but not too long
            if(err < second_chance_avail && !buffer_ready) 
            { 
                clock_nanosleep(CLOCK_MONOTONIC, 0, &second_chance_time, NULL);
                buffer_ready = get_next_packet(nA, playback_buffer, &read_packet);
                DEBUG_LOG(PACKET, GET_X2, "buffer = %d, packet = %u", buffer_ready, read_packet);
            }
        }

        if(nA->debug.last_audio) {
            if(nA->debug.last_audio_index + nA->audio.alsa_period_size > nA->debug.last_audio_len) {
                int remaining = nA->debug.last_audio_len - nA->debug.last_audio_index;
                memcpy(nA->debug.last_audio + nA->debug.last_audio_index, playback_buffer, remaining * sizeof(buffer_type));
                memcpy(nA->debug.last_audio, playback_buffer + remaining, (nA->audio.alsa_period_size - remaining) * sizeof(buffer_type));
                nA->debug.last_audio_index = nA->audio.alsa_period_size - remaining;
            } else {
                memcpy(nA->debug.last_audio + nA->debug.last_audio_index, playback_buffer, nA->audio.alsa_period_size * sizeof(buffer_type));
                nA->debug.last_audio_index += nA->audio.alsa_period_size;
            }
        }

        int glitched = 0;

        if(buffer_ready && 
                ( (nA->debug.simulated_drop_interval > 0 && nA->audio.playback_packet % nA->debug.simulated_drop_interval == 0) 
                    || (nA->debug.simulated_drop_rate >= uniform_double())
                )
          )
        {
            buffer_ready = 0;
            DEBUG_LOG(DEBUG, DROP2, "Dropped packet");
        }
        
        // if not using filtering, do basic glitch handling (ie simple ramp/mute)
        // otherwise pass the buffer to the LPC code to do prediction if needed
        if(!nA->using_filtering) 
        {
            state = process_buffer_basic(nA, state, playback_buffer, playback_buffer_size, buffer_ready, read_packet);
        }
        else
        {
            DEBUG_LOG(PACKET, PLAY, "buffer_ready = %d, packet_number = %u vs %u | Q%u", buffer_ready, read_packet, nA->audio.playback_packet, latency_packetbuffer_periods(nA->packetbuf));
            
            struct timespec filtert = timer_start();
            state = process_buffer_filter(nA, state, playback_buffer, playback_buffer_size, buffer_ready, read_packet);
            DEBUG_LOG(SLAVE, LPC_PROC, "filter processing: %ldus", timer_end(filtert));
        }
        glitched = (state == STATE_MUTED || state == STATE_MISSED_PACKET);
        
        if(state != STATE_START && buffer_ready) 
        {    
            nA->audio.playback_packet++;
            //DEBUG_LOG(PACKET, PLAYBACK, "incrementing playback packet to %u", nA->audio.playback_packet);
        }

        // attempt to respond to glitches in the audio stream by conditionally
        // adding more buffering
        if(nA->adaptive_buffering && glitched) 
        {
            long last_glitch_ms = timer_end_ms(nA->last_glitch_time);
            DEBUG_LOG(GLITCH, TIME_SINCE, "%ld ms", last_glitch_ms);
            // the time elapsed could be huge on first glitch because the
            // variable isn't initialised, so check buffers_added 
            // to see if that's the case
            if(last_glitch_ms < ADD_BUFFER_INTERVAL_MS || nA->buffers_added == 0) {
                // add a new buffer if not at max limit
                DEBUG_LOG(GLITCH, ADD_BUFFER, "%d < %d", nA->cur_buffers, nA->max_buffers);
                if(nA->cur_buffers < nA->max_buffers) {
                    nA->cur_buffers++;
                    nA->buffers_added++;
                }
                DEBUG_LOG(GLITCH, CUF_BUFFERS, "=%d (added %d total)", nA->cur_buffers, nA->buffers_added);
            }
            nA->last_glitch_time = timer_start();
        }

        // calculate RMS for the buffer
        unsigned short bufrms = rms_buffer(playback_buffer, nA->audio.alsa_period_size);

        // actually play the buffer
        err = snd_pcm_writei(nA->audio.pcm_handle, playback_buffer, nA->audio.alsa_period_size);
        nA->stats.buffers_played++;
        
        // if we're playing normally...
        if(nA->adaptive_buffering && state == STATE_NORMAL && !glitched) {
            // check if we've added more buffers than is ideal, and if there
            // hasn't been a glitch for a while (30s?) and that the queue is 
            // actually as long as cur_buffers indicates (it probably won't be)
            if(nA->cur_buffers > nA->reduce_to_buffers && 
                    timer_end_ms(nA->last_glitch_time) >= GLITCH_INTERVAL_MS && 
                    latency_packetbuffer_periods(nA->packetbuf) >= nA->cur_buffers) {
                // if all that is true, check if the last time we removed a 
                // buffer was also 30s??? ago (don't want to remove buffers
                // very quickly!), and that the current buffer is "quiet"
                if(timer_end_ms(nA->last_remove_time) > REMOVE_BUFFER_INTERVAL_MS) { //XXX && bufrms < 100) {
                    // finally if we end up in here, remove an extra buffer from 
                    // the queue and increment the next expected packet (so it
                    // doesn't trigger another glitch + corresponding addition
                    // of a new buffer next time around.
                    drop_packet(nA);
                    nA->audio.playback_packet++;
                    
                    nA->cur_buffers--;
                    nA->buffers_removed++;
                    DEBUG_LOG(BUFFER, REMOVE, "Removing a buffer, %d -> %d, total removed %d, playback packet = %d", nA->cur_buffers+1, nA->cur_buffers, nA->buffers_removed, nA->audio.playback_packet);
                    nA->last_remove_time = timer_start();
                }
            }
        }

        clock_gettime(CLOCK_MONOTONIC, &audio_gap);

        T_END(audio_time);
        update_average_tracker(&(nA->stats.play_time), audio_time);

        T_END(loop_time);
        update_average_tracker(&(nA->stats.slave_loop_time), loop_time);

        if(nA->debug.entries) 
        {
            assert(nA->debug.entries_index < nA->debug.entries_length);
            networkaudio_debug_entry *entry = (nA->debug.entries + nA->debug.entries_index);
            entry->packets_received = nA->stats.packets_received;
            entry->queue_latency = latency_packetbuffer_periods(nA->packetbuf);
            entry->last_packet = last_packet;
            entry->last_packet_gap = last_packet_gap;
            entry->audio_state = state;
            entry->underruns = nA->stats.play_underruns;
            entry->rms = bufrms;
            entry->cur_buffers = nA->cur_buffers;
            entry->reduce_to_buffers = nA->reduce_to_buffers;
            entry->buffers_added = nA->buffers_added;
            entry->buffers_removed = nA->buffers_removed;
            entry->last_glitch_time = timer_end(nA->last_glitch_time);
            nA->debug.entries_index++;

            if(last_q_len != entry->queue_latency) {
                //DEBUG_LOG(QUEUE, CHANGE, "%d => %d", last_q_len, entry->queue_latency);
                last_q_len = entry->queue_latency;
            }
        }
        if(nA->debug.entries && nA->stats.buffers_played % nA->debug.entries_length == 0)
        {
            sendto(dbgsock, nA->debug.entries, sizeof(networkaudio_debug_entry) * nA->debug.entries_length, 0, (struct sockaddr*)&remote_addr, sizeof(struct sockaddr));

            nA->debug.entries_index = 0;
        } 
        first = 0;
    }
    free(playback_buffer);

    fprintf(stderr, "AUDIO THREAD EXITING\n");
	return NULL;
}

