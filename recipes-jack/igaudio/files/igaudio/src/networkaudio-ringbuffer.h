/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

class RingBuffer
{
public:
    RingBuffer(int n, int buff_size);
    ~RingBuffer();

    void AddVals(long buff_no, int buff_pos);
    long GetCurVal();
    long GetVariance();

protected:
    long *streamPos;
    long *diffs;

    int buffSize;
    int curInd;
    int len;
};

#endif // RINGBUFFER_H
