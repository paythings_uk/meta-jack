/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

typedef struct average_tracker_t
{
    int count;
    int bad_events;
    double mean;
    double M2;    
    double last;
    int inited;
} average_tracker_t;

void update_average_tracker(average_tracker_t *tracker, double t);
void update_average_tracker_delta(average_tracker_t *tracker, double t);
double get_mean(average_tracker_t tracker);
double get_std(average_tracker_t tracker);
