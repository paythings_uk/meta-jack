/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio.h"
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include <linux/net_tstamp.h>
#include <linux/errqueue.h>
#include <linux/socket.h>
#include <linux/sockios.h>
#include <linux/ioctl.h>
#include <asm/socket.h>
#include <asm-generic/socket.h>
#include <asm-generic/sockios.h>
#include <linux/sockios.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <netinet/ip.h>

typedef struct control_hdr {
     char control[512];
         struct cmsghdr cm;
} control_hdr;
    
/* 
    Create a receive buffer to receive a packet
    and set the message_header structure to write into
    that packet 
*/
struct msghdr *get_message_header()
{
    struct msghdr *message_header =  (struct msghdr *)malloc(sizeof(struct msghdr));
    struct iovec *iov = (struct iovec *)malloc(sizeof(struct iovec));    
        
    // allocate a receive buffer
    char *receive_buffer = (char*)malloc(MAX_PACKET_SIZE); 
    memset(message_header,   0, sizeof(*message_header));
    memset(iov, 0, sizeof(*iov));
    iov[0].iov_base = receive_buffer;
    iov[0].iov_len = MAX_PACKET_SIZE;
    message_header->msg_iov = iov;
    message_header->msg_iovlen = 1;    
    return message_header;
}

/* Add a control block to the message header structure, for
   receiving ancilliary packet information such as timestamps */
void add_control_header(struct msghdr *message_header)
{
    control_hdr *ctrl = (control_hdr *)malloc(sizeof(control_hdr));
    message_header->msg_control = ctrl;
    message_header->msg_controllen = sizeof(control_hdr);
}

unsigned network_packet_size(network_audio_t *nA) 
{
    return alsa_period_buffer_size_bytes(nA) + RAW_HDR;
}

/* 
    Parse control messages in a message header and fill the passed
   timestamp structure. Returns the software time (SO_TIMESTAMPNS),
   and the hardware raw/converted times (SO_TIMESTAMPING)
 */
void timestamp_from_cmsg(struct msghdr *msg, packet_timestamp *timestamp)
{
    struct cmsghdr *cmsg;    
    // iterate over control messages
    for (cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg))
    {   
        if(cmsg->cmsg_level==SOL_SOCKET)
        {
            // standard nanosecond accurate timestamp
            if(cmsg->cmsg_type==SO_TIMESTAMPNS)
            {
                timestamp->packet_timestamp = *((struct timespec *)CMSG_DATA(cmsg));
            }
            // hw timestamps from SO_TIMESTAMPING
            if(cmsg->cmsg_type==SO_TIMESTAMPING)
            {
                // this point to an array of 3 timespecs: sw, hw_raw, hw_sys
                struct timespec *stamp_ptr = (struct timespec *)CMSG_DATA(cmsg);
                timestamp->sw_timestamp = stamp_ptr[0];
                timestamp->hw_sys_timestamp = stamp_ptr[1];
                timestamp->hw_raw_timestamp = stamp_ptr[2];                                            
            }        
        }    
    }
} 

/* Return the timestamp (as a timespec) of the last packet recieved via ioctl */
void last_packet_timestamp(int sockfd, struct timespec *ts)
{
    ioctl(sockfd, SIOCGSTAMPNS, ts);
}


/* Return a timespec as floating point seconds */
double float_time(struct timespec *t)
{
    return (double)t->tv_sec + (double)(t->tv_nsec)/1e9;
}

/*  
    Turn on high precision timestamping on the socket, and
    force the socket to be high priority.
*/
void enable_timestamping_high_priority(int sockfd, int is_master)
{
    // configure and enable timestamping
    
    // TODO this doesn't work, ioctl fails 
    /*
    struct ifreq ifr;
    struct hwtstamp_config hwconfig;
    memset(&hwconfig, 0, sizeof(hwconfig));    
    strncpy(ifr.ifr_name, "eth0", sizeof(ifr.ifr_name));    
    hwconfig.tx_type = HWTSTAMP_TX_ON;
    hwconfig.rx_filter = HWTSTAMP_FILTER_PTP_V1_L4_EVENT; //HWTSTAMP_FILTER_PTP_V2_L2_EVENT;
    ifr.ifr_data = &hwconfig;
    if (ioctl(sockfd, SIOCSHWTSTAMP, &ifr) < 0)    
        DEBUG_LOG(WARN, HW_TIMESTAMP_IOCTL_FAIL, "Could not set hardware timestamping ioctl");    
    */
    
    // Enable ip low delay / high priority socket
    int ip_low_delay = 224; // IPTOS_LOWDELAY;
    //int socket_priority = 7; // 512; ??
    int timestamp_on = 1;
    //int timestamp_options = SOF_TIMESTAMPING_TX_HARDWARE | SOF_TIMESTAMPING_TX_SOFTWARE | SOF_TIMESTAMPING_SYS_HARDWARE | SOF_TIMESTAMPING_RAW_HARDWARE | SOF_TIMESTAMPING_SOFTWARE | SOF_TIMESTAMPING_RX_HARDWARE | SOF_TIMESTAMPING_RX_SOFTWARE;
    int timestamp_options = 
            SOF_TIMESTAMPING_SYS_HARDWARE       // return HW timestamp translated to system time
        |   SOF_TIMESTAMPING_RAW_HARDWARE       // return original HW timestamp
        |   SOF_TIMESTAMPING_SOFTWARE           // return software generated system timestamp
        |   SOF_TIMESTAMPING_RX_HARDWARE        // if possible, get original HW timestamps 
        |   SOF_TIMESTAMPING_RX_SOFTWARE        // if that fails or isn't possible, use SW timestamping instead
        ;

    int __attribute__((unused)) bufsize = 1024*1024;
    
    // don't bother setting these two options on master
    if(!is_master && setsockopt(sockfd, SOL_SOCKET, SO_TIMESTAMPNS, (int *) &timestamp_on, sizeof(timestamp_on))<0)
        DEBUG_LOG(WARN, TIMESTAMPING_FAIL, "Could not set nanosecond timestamping.");   
    if(!is_master && setsockopt(sockfd, SOL_SOCKET, SO_TIMESTAMPING, (int *) &timestamp_options, sizeof(timestamp_options))<0)
        DEBUG_LOG(WARN, TIMESTAMPING_CFG_FAIL, "Could not configure timestamping.");

    if(setsockopt(sockfd, IPPROTO_IP, IP_TOS, &ip_low_delay, sizeof(ip_low_delay))<0)
        DEBUG_LOG(WARN, TOS_LOWDELAY_FAIL, "Could not enable TOS low delay.");
    // think this is not needed if already setting IP TOS above
    //if(setsockopt(sockfd, IPPROTO_IP, SO_PRIORITY, &socket_priority, sizeof(socket_priority))<0)
    //    DEBUG_LOG(WARN, SO_PRIORITY_FAIL, "Could not enable SO_PRIORITY on the socket.");

    //fprintf(stderr, "Setting socket buffer size to %d bytes\n", bufsize);
    //if(setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &bufsize, sizeof(bufsize)) < 0)
    //    DEBUG_LOG(WARN, SO_SNDBUF_FAIL, "Could not set send buffer size");
    //if(setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &bufsize, sizeof(bufsize)) < 0)
    //    DEBUG_LOG(WARN, SO_RCVBUF_FAIL, "Could not set send buffer size");
}

/* Compute the CRC32 of a memory block */
unsigned long crc32(unsigned long inCrc32, const void *buf, size_t bufLen )
{
    static const unsigned long crcTable[256] = {
   0x9E6495A3,0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,
   0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,
   0xE7B82D07,0x90BF1D91,0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,
   0x6DDDE4EB,0xF4D4B551,0x83D385C7,0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,
   0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,0x3B6E20C8,0x4C69105E,0xD56041E4,
   0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,0x35B5A8FA,0x42B2986C,
   0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,0x26D930AC,
   0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,
   0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,
   0xB6662D3D,0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,
   0x9FBFE4A5,0xE8B8D433,0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,
   0x086D3D2D,0x91646C97,0xE6635C01,0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,
   0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,0x65B0D9C6,0x12B7E950,0x8BBEB8EA,
   0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,0x4DB26158,0x3AB551CE,
   0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,0x4369E96A,
   0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,
   0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,
   0xCE61E49F,0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,
   0xB7BD5C3B,0xC0BA6CAD,0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,
   0x9DD277AF,0x04DB2615,0x73DC1683,0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,
   0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,0xF00F9344,0x8708A3D2,0x1E01F268,
   0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,0xFED41B76,0x89D32BE0,
   0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,0xD6D6A3E8,
   0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,
   0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,
   0x4669BE79,0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,
   0x220216B9,0x5505262F,0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,
   0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,
   0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,0x95BF4A82,0xE2B87A14,0x7BB12BAE,
   0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,0x86D3D2D4,0xF1D4E242,
   0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,0x88085AE6,
   0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,
   0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,
   0x3E6E77DB,0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,
   0x47B2CF7F,0x30B5FFE9,0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,
   0xCDD70693,0x54DE5729,0x23D967BF,0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,
   0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D };
    unsigned long crc32;
    unsigned char *byteBuf;
    size_t i;

    /** accumulate crc32 for buffer **/
    crc32 = inCrc32 ^ 0xFFFFFFFF;
    byteBuf = (unsigned char*) buf;
    for (i=0; i < bufLen; i++) {
        crc32 = (crc32 >> 8) ^ crcTable[ (crc32 ^ byteBuf[i]) & 0xFF ];
    }
    return( crc32 ^ 0xFFFFFFFF );
}

void record_packet_gap(network_audio_t *nA, int packet_number, long gap) 
{
    int index = nA->stats.packets_received % nA->stats.packet_gaps_length;
    assert(index < 50000);
    nA->stats.packet_gaps[index] = gap;
    nA->stats.packet_numbers[index] =  packet_number;
    if(gap > nA->stats.packet_thresholds[2]) 
    {
        if(gap > nA->stats.packet_thresholds[3])
            nA->stats.packet_bins[4]++;
        else
            nA->stats.packet_bins[3]++;
    } else if (gap < nA->stats.packet_thresholds[1]) {
        if(gap < nA->stats.packet_thresholds[0])
            nA->stats.packet_bins[0]++;
        else
            nA->stats.packet_bins[1]++;
    } else {
        nA->stats.packet_bins[2]++;
    }
}

void dump_packet_gap_stats(network_audio_t *nA) 
{
    FILE* gapsf = fopen("gaps", "w");
    double avg = 0, stddev = 0;
    int maxp = nA->stats.packets_received > nA->stats.packet_gaps_length ? nA->stats.packet_gaps_length : nA->stats.packets_received;
    assert(maxp <= 50000);
    for(int i=0;i<maxp;i++)
    {
        fprintf(gapsf, "%d,%d\n", nA->stats.packet_gaps[i], nA->stats.packet_numbers[i]);
        avg += nA->stats.packet_gaps[i];
    }
    avg /= maxp;
    fprintf(stderr, "Packet mean (%d) = %.3f us\n", maxp, avg);
    for(int i=0;i<maxp;i++) 
    {
        stddev += pow((double)nA->stats.packet_gaps[i] - avg, 2);
    }
    stddev /= maxp;
    fprintf(stderr, "Packet stddev = %.3f us\n", pow(stddev, 0.5));
    fprintf(gapsf, "Packet stddev = %.3f us\n", pow(stddev, 0.5));

    fprintf(stderr, "Packet nA->stats.packet_bins  : [%d << %d < %d < %d << %d]\n", nA->stats.packet_bins[0], nA->stats.packet_bins[1], nA->stats.packet_bins[2], nA->stats.packet_bins[3], nA->stats.packet_bins[4]);
    fprintf(gapsf, "Packet nA->stats.packet_bins  : [%d << %d < %d < %d << %d]\n", nA->stats.packet_bins[0], nA->stats.packet_bins[1], nA->stats.packet_bins[2], nA->stats.packet_bins[3], nA->stats.packet_bins[4]);
    for(int i=0;i<5;i++)
        nA->stats.packet_bins[i] = (int)((nA->stats.packet_bins[i] / (double)nA->stats.packets_received) * 100.0);
    fprintf(stderr, "Packet nA->stats.packet_bins %%: [%d << %d < %d < %d << %d]\n", nA->stats.packet_bins[0], nA->stats.packet_bins[1], nA->stats.packet_bins[2], nA->stats.packet_bins[3], nA->stats.packet_bins[4]);
    fprintf(gapsf, "Packet nA->stats.packet_bins %%: [%d << %d < %d < %d << %d]\n", nA->stats.packet_bins[0], nA->stats.packet_bins[1], nA->stats.packet_bins[2], nA->stats.packet_bins[3], nA->stats.packet_bins[4]);
    fclose(gapsf);
}

/* Set the current process priority and scheduler type (SCHED_RR/SCHED_FIFO) */
void realtime_priority(int priority, int schedtype)
{
    struct sched_param schedparm;
    memset(&schedparm, 0, sizeof(schedparm));
    schedparm.sched_priority = priority; 
    sched_setscheduler(0, schedtype, &schedparm);
    setpriority(PRIO_PROCESS, 0, -17);
}

void setup_priority(void) {
    pthread_attr_t threadAttr;
    struct sched_param schedParam;
    pthread_attr_init(&threadAttr);
    pthread_attr_getschedparam(&threadAttr, &schedParam);
    schedParam.sched_priority = sched_get_priority_max(SCHED_RR);
    pthread_attr_setschedpolicy(&threadAttr, SCHED_RR);
    pthread_attr_setschedparam(&threadAttr, &schedParam);
}

/* Return the gain, in dB of signed 16 bit PCM buffer */
double compute_gain(buffer_type *buffer, int len)
{
    /*
    int prev=0;
    int total = 0;
    int i;
    for(i=0;i<len;i++)
    {    
        total += abs(buffer[i] - prev);
        prev = buffer[i];
    }
    // compute overall mean level in dB
    double gain = 20.0 * (log((double)(total/32768.0)/(double)(len)) / log(10.0));        
    return gain;
    */
    return 0;
}


/*
  Print error message and string corresponding to errno, then exit
*/
void error(char *msg)
{
	perror(msg);
	exit(0);
}


/*
    Print error message and string corresponding to errno
*/
void warn(char *msg)
{
	perror(msg);
}
/* 
    Try and parse a string as an integer. If it cannot be 
    parsed, exit with an error. Otherwise, return the parsed
    integer.
*/    
int check_integer(char *str)
{
    char *endptr;    
    int val = strtol(str, &endptr, 10);
    if(*endptr!='\0')
    {
        DEBUG_LOG(ERROR, ARGUMENT_NOT_INT, "Invalid argument %s: not an integer\n", str);
        exit(1);
    }
    return val;
}


/*
    Return 1 if the string is "truthy", otherwise 0
*/
int is_true(char *str)
{   
    if(!strcasecmp(str,"enabled") || !strcasecmp(str,"y") || !strcasecmp(str,"yes") || !strcasecmp(str,"true") || !strcasecmp(str,"on") || !strcasecmp(str,"1"))
        return 1;
    return 0;
}


/* 
   Return a random number on the interval [0,1].
   Not a very good random number, but probably OK for 
   simple uses.
*/
double uniform_double(void)
{
    return rand() / (double)RAND_MAX;
}
