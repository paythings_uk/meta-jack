/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#ifndef BUFFER_QUEUE_H
#define BUFFER_QUEUE_H

#include "sglib.h"
#include <stdlib.h>
#include <stdio.h>

#include "networkaudio-buffer.h"

#define BUFFER_INVALID -1
#define BUFFER_INITIALIZED 0
#define BUFFER_INTERPOLATED 1
#define BUFFER_INTERPOLATED_SEQ 6
#define BUFFER_RECEIVED 2
#define BUFFER_RECEIVED_COMPRESSED 3
#define BUFFER_TEST_SIGNAL 4
#define BUFFER_RECORDED 5

typedef struct dq {
        buffer_type *buffer;     
        int id;             // unique packet id
        int interpolated;   // state of this packet; is it received, received from a compressed source, 
                            // interpolated or just initialised?                
        int frame_number;
        double gain;        // overall level of this buffer, in dB
        int ptr;            // how much of this buffer has been dequeued, so far
        struct dq *next, *prev;
} dq;

#define DQ_COMPARATOR(e1, e2) (e1->id-e2->id)


typedef struct buffer_q_t
{
    dq *activeq;
    dq *poolq;
    int buffer_size;
    int id_ctr;
} buffer_q_t;

/* Create a buffer queue with a given size */
buffer_q_t *init_buffer_q(int buffer_len, int initial_pool);

/* Free a buffer queue structure */
void free_buffer_q(buffer_q_t *q);

/* Clear and free the entire queue */
void clear_buffer_q(buffer_q_t *q);

/* (re)set the size of each buffer in the queue, clearing the
   queue if needed */
void set_buffer_q_size(buffer_q_t *q, int size);

/* Push a newly allocated buffer onto the pool */
void addpool_buffer_q(buffer_q_t *q);

/* Move a pool buffer into the head of the active queue. Return the buffer of that queue element. */
buffer_type *enque_buffer_q(buffer_q_t *q);

/* Undo an enque */
void undo_enque_buffer_q(buffer_q_t *q);

/* Pop the tail of the queue. Return the buffer it points to. */
buffer_type *deque_buffer_q(buffer_q_t *q);

/* Expand the pool by n buffers */
void expandpool_buffer_q(buffer_q_t *q, int n);

/* Print out the state of the queue */
void print_buffer_q(buffer_q_t *q);

/* Return the last buffer pushed onto the active queue */
buffer_type *last_added_buffer_q(buffer_q_t *q);

/* Return the length of the active q */
int activeq_length_buffer_q(buffer_q_t *q);

/* Return the ix'th buffer that previously came off the active queue;
  that is the ix'th buffer of the pool queue, starting from the head.
  0 is the last buffer played; 1 is the 2nd last, and so on.
  Returns NULL if no such buffer.
  */
buffer_type *previous_buffer_q(buffer_q_t *q, int ix);

/* Return the latency of the queue (i.e. length of the active q, in samples) */
int latency_buffer_q(buffer_q_t *q);

/* Print the active q, including the gains, and whether or not packets were received. */
void print_activeq(buffer_q_t *q);

/* Return the gain of the last buffer on the active queue. */
double last_gain_buffer_q(buffer_q_t *q);


/* Enque from a buffer of a given size, feeding partial
   buffers as required. This copies the entire buffer
   into the active queue (partial remnants remain in the pool).
   Should *not* be mixed with raw en/deque_buffer_q() calls unless you 
   are sure what you are doing!.
*/
void push_buffer_q(buffer_q_t *q, buffer_type *in, int in_length);

/* Deque into a buffer of a given size, feeding partial
   buffers as required. May leave a partial buffer in the active queue.
   Returns the number of bytes writen, which wil == in_length if there
   were enough buffers in the active queue to satisfy the request.
   Should *not* be mixed with raw en/deque_buffer_q() calls unless you 
   are sure what you are doing!.
*/
int pop_buffer_q(buffer_q_t *q, buffer_type *in, int in_length);

/* 
   Add a new frame to the buffer, copying the data and setting the interpolated state,
   the frame number and the gain
*/
void add_buffer_q(buffer_q_t *q, buffer_type *in, int interpolated, int frame_number, int compute_gain);

/*
    Stuff an uninitialised buffer into the queue 
*/
void stuff_buffer_q(buffer_q_t *q);

/* Return the next *complete* (i.e. ptr==0) buffer from the active queue,
   or NULL, if there is no such one. */
buffer_type *pop_complete_buffer_q(buffer_q_t *q);

extern double compute_gain(buffer_type *buffer, int len);

void test_buffer_q();
void test_partial_buffer_q();

#endif
