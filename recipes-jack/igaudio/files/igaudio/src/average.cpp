/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <math.h>
#include <stdio.h>
#include "average.h"

/* Update an average tracker with a new value. Updates mean and variance */
void update_average_tracker(average_tracker_t *tracker, double t)
{
    double delta;
    tracker->count++;
    delta = t-tracker->mean;
    tracker->mean += delta/tracker->count;
    tracker->M2 += delta*(t-tracker->mean);    
    if(delta>tracker->mean)
    {
        tracker->bad_events++;
    }
    
}

/* Update an average tracker with a change in value since the last call. 
   If no previous call, the average is not updated.
   Updates mean and variance. */
void update_average_tracker_delta(average_tracker_t *tracker, double t)
{
    double dt;
    double delta;
    if(tracker->inited)
        dt = t - tracker->last;
    else
        {
            tracker->inited=1;
            tracker->last = t;
            return;            
        }    
    tracker->last = t;
    tracker->count++;
    delta = dt-tracker->mean;
    tracker->mean += delta/tracker->count;
    tracker->M2 += delta*(dt-tracker->mean);    
    if(delta>tracker->mean)
    {
        tracker->bad_events++;
    }
    
}
