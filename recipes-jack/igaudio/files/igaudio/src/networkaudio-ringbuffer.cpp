/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include <stdlib.h>
#include <string.h>
#include "networkaudio-ringbuffer.h"

RingBuffer::RingBuffer(int n, int buff_size)
{
    streamPos = (long *) malloc(sizeof(long) * n);
    diffs = (long *) malloc(sizeof(long) * n);

    buffSize = buff_size;
    curInd = 0;
    len = n;

    memset(streamPos, 0, sizeof(long) * n);
    memset(diffs, 0, sizeof(long) * n);
}

RingBuffer::~RingBuffer()
{
    free(streamPos);
    free(diffs);
}

void RingBuffer::AddVals(long buff_no, int buff_pos)
{
   curInd = (++curInd) % len;

   streamPos[curInd] = buff_no * buffSize+buff_pos;

   if(curInd>0)
       diffs[curInd] = streamPos[curInd] - streamPos[curInd-1];
   else
       diffs[curInd] = streamPos[curInd] - streamPos[len-1];

}

long RingBuffer::GetCurVal()
{
    return streamPos[curInd];
}

long RingBuffer::GetVariance()
{
    long mean = 0, tot = 0, err;
    for(int i = 0; i < len; i++)
        mean += diffs[i];
    mean /= len;

    for(int i = 0; i < len; i++)
    {
        err = diffs[i]-mean;
        tot += err*err;
    }

    return tot/len;
}


