/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

/* Network testing code.
   Sends a series of packets across the network in a loop, and verifies timing.
*/

#include "ntest.h"

#define MAX_PACKET_SIZE 65536
#define NSEC_PER_SEC    1000000000

//#define DEBUG 1

#ifdef DEBUG
#define DEBUG_LOG(T,C,X, ...)  {fprintf(stderr, #T" "#C" %f: ",current_time()); fprintf(stderr, X, ##__VA_ARGS__); fprintf(stderr, "\n");}
#else
#define DEBUG_LOG(T,C,X,...)
#endif

typedef struct control_hdr {
         char control[512];
         struct cmsghdr cm;
} control_hdr;
    
/* 
    Create a receive buffer to receive a packet
    and set the message_header structure to write into
    that packet 
*/
struct msghdr *get_message_header()
{
    struct msghdr *message_header =  (struct msghdr *)malloc(sizeof(struct msghdr));
    struct iovec *iov = (struct iovec *)malloc(sizeof(struct iovec));    
        
    // allocate a receive buffer
    char *receive_buffer = (char*)malloc(MAX_PACKET_SIZE); 
    memset(message_header,   0, sizeof(*message_header));
    memset(iov, 0, sizeof(iov));
    iov[0].iov_base = receive_buffer;
    iov[0].iov_len = MAX_PACKET_SIZE;
    message_header->msg_iov = iov;
    message_header->msg_iovlen = 1;    
    return message_header;
}

/* Add a control block to the message header structure, for
   receiving ancilliary packet information such as timestamps */
void add_control_header(struct msghdr *message_header)
{
    control_hdr *ctrl = (control_hdr *)malloc(sizeof(control_hdr));
    message_header->msg_control = ctrl;
    message_header->msg_controllen = sizeof(control_hdr);
}

/* Set the current process priority and scheduler to realtime */
void realtime_priority(int priority)
{
    // boost priority and switch to realtime
     struct sched_param schedparm;
     memset(&schedparm, 0, sizeof(schedparm));
     schedparm.sched_priority = priority; // lowest rt priority
     sched_setscheduler(0, SCHED_FIFO, &schedparm);    
     // Lock the memory for this process
     mlockall(MCL_CURRENT | MCL_FUTURE);
}

/* 
    Try and parse a string as an integer. If it cannot be 
    parsed, exit with an error. Otherwise, return the parsed
    integer.
*/    
int check_integer(char *str)
{
    char *endptr;    
    int val = strtol(str, &endptr, 10);
    if(*endptr!='\0')
    {
        DEBUG_LOG(ERROR, ARGUMENT_NOT_INT, "Invalid argument %s: not an integer\n", str);
        exit(1);
    }
    return val;
}


/*
    Return 1 if the string is "truthy", otherwise 0
*/
int is_true(char *str)
{   
    if(!strcasecmp(str,"enabled") || !strcasecmp(str,"y") || !strcasecmp(str,"yes") || !strcasecmp(str,"true") || !strcasecmp(str,"on") || !strcasecmp(str,"1"))
        return 1;
    return 0;
}


/* 
    Parse control messages in a message header and fill the passed
   timestamp structure. Returns the software time (SO_TIMESTAMPNS),
   and the hardware raw/converted times (SO_TIMESTAMPING)
 */
void timestamp_from_cmsg(struct msghdr *msg, packet_timestamp *timestamp)
{
    struct cmsghdr *cmsg;    
    // iterate over control messages
    for (cmsg = CMSG_FIRSTHDR(msg); cmsg; cmsg = CMSG_NXTHDR(msg, cmsg))
    {   
        if(cmsg->cmsg_level==SOL_SOCKET)
        {
            // standard nanosecond accurate timestamp
            if(cmsg->cmsg_type==SO_TIMESTAMPNS)
            {
                timestamp->packet_timestamp = *((struct timespec *)CMSG_DATA(cmsg));
            }
            // hw timestamps from SO_TIMESTAMPING
            if(cmsg->cmsg_type==SO_TIMESTAMPING)
            {
                // this point to an array of 3 timespecs: sw, hw_raw, hw_sys
                struct timespec *stamp_ptr = (struct timespec *)CMSG_DATA(cmsg);
                timestamp->sw_timestamp = stamp_ptr[0];
                timestamp->hw_sys_timestamp = stamp_ptr[1];
                timestamp->hw_raw_timestamp = stamp_ptr[2];                                            
            }        
        }    
    }
} 

/* Return the timestamp (as a timespec) of the last packet recieved via ioctl */
void last_packet_timestamp(int sockfd, struct timespec *ts)
{
    ioctl(sockfd, SIOCGSTAMPNS, ts);
}



/*  
    Turn on high precision timestamping on the socket, and
    force the socket to be high priority.
*/
void enable_timestamping_high_priority(int sockfd)
{
    // configure and enable timestamping
    struct ifreq ifr;
    struct hwtstamp_config hwconfig;
    memset(&hwconfig, 0, sizeof(hwconfig));    
    strncpy(ifr.ifr_name, "eth0", sizeof(ifr.ifr_name));    
    hwconfig.tx_type = HWTSTAMP_TX_ON;
    hwconfig.rx_filter = HWTSTAMP_FILTER_PTP_V2_L2_EVENT;
    ifr.ifr_data = &hwconfig;
    if (ioctl(sockfd, SIOCSHWTSTAMP, &ifr) < 0)    
        DEBUG_LOG(WARN, HW_TIMESTAMP_IOCTL_FAIL, "Could not set hardware timestamping ioctl");    
    
    
    // Enable ip low delay / high priority socket
    int ip_low_delay = IPTOS_LOWDELAY;
    int socket_priority = 7;
    int timestamp_on = 1;
    int timestamp_options = SOF_TIMESTAMPING_TX_HARDWARE | SOF_TIMESTAMPING_TX_SOFTWARE | SOF_TIMESTAMPING_SYS_HARDWARE | SOF_TIMESTAMPING_RAW_HARDWARE | SOF_TIMESTAMPING_SOFTWARE | SOF_TIMESTAMPING_RX_HARDWARE | SOF_TIMESTAMPING_RX_SOFTWARE;
    
    if(setsockopt(sockfd, SOL_SOCKET, SO_TIMESTAMPNS, (int *) &timestamp_on, sizeof(timestamp_on))<0)
        DEBUG_LOG(WARN, TIMESTAMPING_FAIL, "Could not set enabled timestamping.");   
    if(setsockopt(sockfd, SOL_SOCKET, SO_TIMESTAMPING, (int *) &timestamp_options, sizeof(timestamp_options))<0)
        DEBUG_LOG(WARN, TIMESTAMPING_CFG_FAIL, "Could not configure timestamping.");
    if(setsockopt(sockfd, IPPROTO_IP, IP_TOS, &ip_low_delay, sizeof(ip_low_delay))<0)
        DEBUG_LOG(WARN, TOS_LOWDELAY_FAIL, "Could not enable TOS low delay.");
    if(setsockopt(sockfd, IPPROTO_IP, SO_PRIORITY, &socket_priority, sizeof(socket_priority))<0)
        DEBUG_LOG(WARN, SO_PRIORITY_FAIL, "Could not enable SO_PRIORITY on the socket.");   
}



/* Normalise a timespec */
void tsnorm(struct timespec *ts) {
    while (ts->tv_nsec >= NSEC_PER_SEC) {
        ts->tv_nsec -= NSEC_PER_SEC;
        ts->tv_sec++;
    }
}

/* Return the current time in floating seconds */
double current_time()
{
    struct timespec start_time;
    double time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    time = start_time.tv_sec + start_time.tv_nsec/1e9;
    return time;
}


/*
    Parse the command line options (both short form and long)
    and set the appropriate parameters
*/
void set_options(int argc, char **argv, network_test_t *nT)
{
    
    static struct option long_options[] =
     {
       /* The option list */
       {"help", no_argument, 0, 'h'},               
       {"transmit",   required_argument,  0, 't'},
       {"listen",   no_argument,  0, 'l'},
       {"frame",   required_argument,  0, 'z'},
       {"rate",   required_argument,  0, 'r'},
       {"seconds",   required_argument,  0, 's'},
       {"output",   required_argument,  0, 'o'},
       {"non-blocking", no_argument, &nT->blocking, 0},
       {"blocking", no_argument, &nT->blocking, 1},     
       {"tag", required_argument, 0, 'g'},
       {"ignore", required_argument, 0, 'i'},
        {0, 0, 0, 0}
     };
             
     while(1)
     {
        int option_index = 0;
        int c = getopt_long (argc, argv, "t:l?hf:r:k:s:o:g:b:i",
                    long_options, &option_index);             
        if(c==-1) break; // end of options                    
        switch(c)
        {
            case 0:
                // flag set, do nothing
                break;      
            case 'b':
                nT->broadcast = 1;
                break;
            case 'o':
                nT->output_dir = optarg;
                break;
            case 't':
                nT->is_recv = 0;
                nT->remote_ip = optarg;
                break;
            case 'l':
                nT->is_recv = 1;
                break;
            case 's':
                nT->seconds = check_integer(optarg);
                break;
                
            case '?':
            case 'h':
                help();
                exit(0);
                break; 
            case 'f':
                nT->frame_size = check_integer(optarg);                
                break;
            case 'r':
                nT->rate = check_integer(optarg);            
                break;
            case 'k':
                nT->kbps  = check_integer(optarg);            
                break;
            case 'g':
                nT->output_tag = optarg;
                break;
            case 'i':
                nT->ignore = check_integer(optarg);
                break;
        }
     
     }            
}

/* 
    Print the command line help
*/    
void help()
{
    printf("Usage: ntest [options]\n");
    printf("        --help or -h                        Show this help\n");
    printf("        --transmit or -t <ip>               Send to this IP address\n");
    printf("        --listen or -l                      Receive\n");
    printf("        --frame or -f <n>                   Size of each frame in bytes\n");
    printf("        --rate or -r <n>                    Transmit rate, in packets/second \n");
    printf("        --kbps or -k <n>                    Transmit rate, in kbps/second. \n");
    printf("                                            Use only two of -f, -r or -k\n");
    printf("        --seconds or -s <n>                 Number of seconds for test. Default 60.\n");
    printf("        --blocking                          Use blocking sockets\n");
    printf("        --non-blocking                      Use non-blocking sockets\n");
    printf("        --output or -o <dirname>            Write CSV logfiles to the given directory\n");
    printf("        -b                                  Send packets in broadcast mode\n");
    printf("        --ignore or -i <n>                  Ignore the first n packets when calculating stats. \n");
    printf("\n");
    
}

/* Set the defaults */
void set_defaults(network_test_t *nT)
{
    nT->is_recv = 1;
    nT->remote_ip = "127.0.0.1";
    nT->frame_size = 0;
    nT->rate = 0;
    nT->kbps = 0;
    nT->port = 4800;
    nT->n_packets = 0;
    nT->seconds = 60;
    nT->packet_number = 0;
    nT->packets = (packet_info*)(malloc(sizeof(*nT->packets) * MAX_PACKETS));
    nT->packets_lost = 0;
    nT->duplicate_packets=0;
    nT->blocking = 1;
    nT->output_dir=NULL;
    nT->output_tag = NULL;
    nT->broadcast = 0;
	nT->ignore = 0;
}

#define fprint_iattr(F,X) (fprintf(F, "    %s: %d\n", #X, (int)(nT->X)))
#define fprint_dattr(F,X) (fprintf(F, "    %s: %.2f\n", #X ,(double)(nT->X)))
#define fprint_sattr(F,X) (fprintf(F, "    %s: \"%s\"\n", #X, nT->X))
/* Write a YAML form of the network audio structure to the given file descriptor */
void yaml_network_test(FILE *fd, network_test_t *nT)
{    
   
    fprintf(fd, "config: \n");    
    fprint_sattr(fd, remote_ip);
    fprint_iattr(fd, is_recv);
    fprint_dattr(fd, rate);
    fprint_iattr(fd, frame_size);
    fprint_iattr(fd, kbps);
    fprint_dattr(fd, loop_time);
    fprint_iattr(fd, port);
    fprint_iattr(fd, seconds);
    fprint_iattr(fd, n_packets);
    fprint_iattr(fd, blocking);   
    fprint_sattr(fd, output_dir);   
    fprint_sattr(fd, output_tag);

    fprintf(fd,"\n");
}


void initialise_network_test(network_test_t *nT)
{
    if(!nT->is_recv)
    {
        if(nT->rate==0 && nT->kbps==0)
        {
            DEBUG_LOG(ERROR, NOT_ENOUGH_OPTIONS, "Must specify at least two of frame, rate and kbps");
            exit(1);
        }
        if(nT->rate==0 && nT->frame_size==0)
        {
            DEBUG_LOG(ERROR, NOT_ENOUGH_OPTIONS, "Must specify at least two of frame, rate and kbps");
            exit(1);
        }
        if(nT->frame_size==0 && nT->kbps==0)
        {
            DEBUG_LOG(ERROR, NOT_ENOUGH_OPTIONS, "Must specify at least two of frame, rate and kbps");
            exit(1);
        }
        if(nT->rate!=0 && nT->kbps!=0 && nT->frame_size!=0)
        {
            DEBUG_LOG(ERROR, TOO_MANY_OPTIONS, "Cannot specify frame, rate and kbps at the same time. Use two of the three!");
            exit(1);
        }
        if(nT->rate==0)
        {
            nT->rate = nT->kbps / (nT->frame_size/1024.0);
        }
        if(nT->frame_size==0)
        {
            nT->frame_size = 1024 * (nT->kbps / (double)nT->rate);
        }
        if(nT->kbps==0)
        {
            nT->kbps = nT->rate * (nT->frame_size/1024.0);
        }
        
        nT->loop_time = 1e9 / nT->rate;
        nT->n_packets = nT->seconds * nT->rate + nT->ignore;
        if(nT->rate<1.0 || nT->rate>10000) 
        {
            DEBUG_LOG(ERROR, INVALID_RATE, "Invalid packet rate; must be >1 per second and <10000 per second");
            exit(1);
        }
        if(nT->frame_size>=50000 || nT->frame_size<24)
        {
            DEBUG_LOG(ERROR, INVALID_FRAME_SIZE, "Invalid frame size; must be >= 24 and < 50000 bytes");
            exit(1);
        }
        if(nT->n_packets>MAX_PACKETS)
        {
            DEBUG_LOG(ERROR, TOO_MANY_PACKETS, "Too many packets; must be < %d packets total", MAX_PACKETS);
            exit(1);
        
        }
    }
}



int transmit_open_socket(network_test_t *nT)
{
    struct hostent *master_server;
    struct sockaddr_in *serv_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
    int sockfd;
    // look up the IP of the host we are transmitting to
    master_server = gethostbyname(nT->remote_ip);
 
    //Open a UDP socket
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    nT->message_header = get_message_header();
   
    // Verify socket connection
    if( sockfd < 0 )
        DEBUG_LOG(ERROR, MASTER_SOCKET_ERROR, "Error opening socket: %s", strerror(errno));
        

    if (master_server == NULL)
    {
        DEBUG_LOG(ERROR, MASTER_HOST_LOOKUP, "No such host: %s", nT->remote_ip);        
        exit(0);
    }
    
    // init server address
    bzero((char *) serv_addr, sizeof(serv_addr));
    serv_addr->sin_family = AF_INET;
    bcopy((char *)master_server->h_addr,
         (char *)&(serv_addr->sin_addr.s_addr),
         master_server->h_length);
    serv_addr->sin_port = htons(nT->port);
    
    // set up the message header for sendmsg()
    nT->message_header->msg_name = (void*)serv_addr;
    nT->message_header->msg_namelen = sizeof(struct sockaddr_in);

    int x = fcntl(sockfd,F_GETFL,0);          // Get socket flags
    if(!nT->blocking)
        fcntl(sockfd,F_SETFL,x | O_NONBLOCK);   // Add non-blocking flag    

    enable_timestamping_high_priority(sockfd);
     
    if(nT->broadcast)
    {
        // Enable broadcast
        int using_broadcast = 1;
        int disable_udp_checksum = 1;
        setsockopt( sockfd,			        // socket affected
                    SOL_SOCKET,				//
                    SO_BROADCAST,			// name of option
                    &using_broadcast,			//
                    sizeof(using_broadcast) );
        // Disable UDP checksum
        setsockopt( sockfd,			        // socket affected
                    SOL_SOCKET,				//
                    SO_NO_CHECK,			// name of option
                    &disable_udp_checksum,	//
                    sizeof(disable_udp_checksum));
        fprintf(stderr, "Enabled broadcast mode\n");
    }

    DEBUG_LOG(STATUS, MASTER_SOCKET_OK, "Transmit socket opened");
    nT->server_address = *serv_addr;
    nT->sockfd = sockfd;   
    return 1;
}


/*

    Open the socket connection to the given remote IP.
  */ 
static void recv_open_socket(network_test_t *nT)
{   
        int sockfd;
        struct sockaddr_in  serv_addr;
        // new UDP socket
		sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        nT->message_header = get_message_header();
        add_control_header(nT->message_header);
       
	    if (sockfd < 0)
	       DEBUG_LOG(ERROR, SLAVE_SOCKET_ERROR, "Error opening socket: %s", strerror(errno));		

        // make non-blocking, if required
        if(!nT->blocking)
            fcntl(sockfd, F_SETFL, O_NONBLOCK);
            
        enable_timestamping_high_priority(sockfd);

		// create server details.
		bzero((char *) &serv_addr, sizeof(serv_addr));

	    serv_addr.sin_family = AF_INET;
	    serv_addr.sin_addr.s_addr = INADDR_ANY;
	    serv_addr.sin_port = htons(nT->port);

		if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)		
			DEBUG_LOG(ERROR, SLAVE_BIND_ERROR, "Error on binding to address: %s", strerror(errno));		

        nT->server_address = serv_addr;
        nT->sockfd = sockfd;
        DEBUG_LOG(STATUS, SLAVE_SOCKET_OK, "Recv socket opened succesfully.");
}


int read_packet(network_test_t *nT, packet_timestamp *packet_timing)
{
    int n;
    int no_packet;
    struct msghdr *hdr = nT->message_header;

    n = recvmsg(nT->sockfd, hdr, 0);
    no_packet =  (n==-1) && (errno==EAGAIN || errno==EWOULDBLOCK  || errno == EINTR);            
    if (!no_packet && n < 0)
    {
        // this is an error condition *other* than no packet waiting
        DEBUG_LOG(ERROR, PACKET_RECEIVE_ERROR, "recv() failed: %s", strerror(errno));
        return -1;
    }
    if(n>=16)
    {   
        unsigned int id, len, id2, mrate;
        // get the timestamps
        timestamp_from_cmsg(hdr, packet_timing);
        // packet is in the format
        // |uint32_t|  | uint32_t| |unsigned char* ......      |
        // | id|       | len|       | databytes| | id|
        unsigned char *packet_buf = (unsigned char *)hdr->msg_iov[0].iov_base;
        id = ntohl(*((uint32_t*)(packet_buf)));
        len = ntohl(*((uint32_t*)(packet_buf+4)));
        packet_timing->sent_timestamp.tv_sec = ntohl(*((uint32_t*)(packet_buf+8)));
        packet_timing->sent_timestamp.tv_nsec = ntohl(*((uint32_t*)(packet_buf+12)));
        
        mrate = ntohl(*((uint32_t*)(packet_buf+16)));
        if((nT->timing_set && nT->frame_size!=(int)len) || (nT->timing_set && len==0))
        {
            DEBUG_LOG(STATUS, FRAME_SIZE_TERMINATED, "Forcefully terminated by master");
            return -2;
            
        }
        nT->frame_size = len;
        nT->rate = mrate / 1000.0;
        id2 = ntohl(*((uint32_t*)(packet_buf+len-4)));
        
        if(id!=id2) 
        {
            DEBUG_LOG(WARN, MISMATCH, "id1!=id2; packet is corrupt");
        }
        
        // check the packet is well formed; i.e. length matches
        if(n!=(int)(len))
        {
            DEBUG_LOG(WARN, PACKET_WRONG_LEN, "Packet with wrong length");
            return -1;
        }
        return id;
    }         
    return -1;
}


void recv_loop(network_test_t *nT)
{
    recv_open_socket(nT);
    struct timespec loop_start_time, actual_sleep_time;
    int period_ns=1000000;
    packet_timestamp packet_timing;
    
    // compute expected buffer time (-0.1 ms for overhead)
    if(!nT->blocking)
    {
        period_ns = nT->loop_time;
        DEBUG_LOG(STATUS, RECV_LOOP_TIME, "Entering non-blocking receive loop: %fms sleep",  period_ns/1e6);
        clock_gettime(CLOCK_MONOTONIC, &loop_start_time);         
    }
    // idle delay while waiting for first packet to arrive (non-blocking only)
    struct timespec wait_delay;
    wait_delay.tv_sec = 0;
    wait_delay.tv_nsec = 10000000;
    
    // reset stats
    nT->packet_number = -1;
    nT->n_packets_recorded = 0;
    nT->packets_lost = 0;
    nT->duplicate_packets = 0;
    nT->n_packets = 0;
    nT->timing_set = 0;
    nT->rate = 0.0;
    nT->frame_size = 0;
    int i = 0;
    printf("Starting recv loop\n");
    while(nT->packet_number!=0)
    {
        int packet_id = read_packet(nT, &packet_timing);
        if(packet_id>=0)
        {
            if((nT->packet_number<0 || nT->packet_number>packet_id) && nT->timing_set)
            {
                if(nT->packet_number>=0)
                    nT->packets_lost +=  (nT->packet_number-1) - packet_id;
                nT->packets[i].id = packet_id;
                nT->packets[i].time = packet_timing.sw_timestamp.tv_sec + packet_timing.sw_timestamp.tv_nsec/1e9;
                nT->packets[i].send_time = packet_timing.sent_timestamp.tv_sec + packet_timing.sent_timestamp.tv_nsec/1e9;
                nT->packet_number = packet_id;
                i++;
            }
            else
                nT->duplicate_packets++;
        }
        else
        {
            if(packet_id==-2)
            {
                // timing changed! quit now
                return;
            }
                
        }
        
        
        
        // once we lock onto a timing, reset the clocks
        if(!nT->timing_set && nT->frame_size>0)
        {
            printf("\n\n==========================================================================\n");
            printf("Recv: packet size: %d bytes / rate: %.0f packets/second\n", nT->frame_size, nT->rate);
            period_ns = (1e9/nT->rate);
            DEBUG_LOG(STATUS, RECV_LOOP_TIME, "Entering non-blocking receive loop: %fms sleep",  period_ns/1e6);
            clock_gettime(CLOCK_MONOTONIC, &loop_start_time);         
            nT->timing_set = 1;
        }
        
        if(!nT->blocking)
        {
            // idle until we have timing info
            if(!nT->timing_set)
            {
                clock_nanosleep(CLOCK_MONOTONIC, 0, &wait_delay, NULL);
                
            }
            else
            {
                 // Increment timer then wait        	
                loop_start_time.tv_nsec += period_ns;       
                tsnorm(&loop_start_time);
                clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &loop_start_time, &actual_sleep_time);
            }
        }
        
        
    }
    nT->n_packets_recorded = i;
    close(nT->sockfd);
 
}

/* Return the timestamp of the last packet recieved via ioctl */
double last_packet_timestamp(int sockfd)
{
    struct timespec ts;
    int err = ioctl(sockfd, SIOCGSTAMPNS, &ts);
    if(err<0) printf("%s\n", strerror(errno));
    return ts.tv_sec + ts.tv_nsec/1e9;
}


/* Transmit one packet as a UDP packet */
int transmit_packet(network_test_t *nT, int flush)
{
    int socket_bytes;
    unsigned char *msgptr =  (unsigned char *)nT->message_header->msg_iov[0].iov_base;
    struct timespec tstamp;
    
    
    clock_gettime(CLOCK_MONOTONIC, &tstamp);
    
    nT->timestamp = tstamp.tv_sec + tstamp.tv_nsec/1e9;
    
    uint32_t sec = (uint32_t) (tstamp.tv_sec);
    uint32_t nsec = (uint32_t) (tstamp.tv_nsec);
    // write in the prefix length, followed by crc and data block
    // [id] [len] [stamp_sec] [stamp_nsec] [databytes...] [id]
    
    if(flush)    
    {
        *((uint32_t *) (msgptr)) = htonl(0);
        *((uint32_t *) (msgptr+4)) = htonl(0);
        *((uint32_t *) (msgptr+16)) = htonl(0);    
        *((uint32_t *) (msgptr+nT->frame_size-4)) = htonl(0);
    }
    else
    {
        *((uint32_t *) (msgptr)) = htonl(nT->packet_number);
        *((uint32_t *) (msgptr+4)) = htonl(nT->frame_size);
        *((uint32_t *) (msgptr+16)) = htonl(nT->rate*1000);
        *((uint32_t *) (msgptr+nT->frame_size-4)) = htonl(nT->packet_number);        
    }
    *((uint32_t *) (msgptr+8)) = htonl(sec);
    *((uint32_t *) (msgptr+12)) = htonl(nsec);
        

    nT->message_header->msg_iov[0].iov_len = nT->frame_size;    
    socket_bytes = sendmsg(nT->sockfd, nT->message_header, 0);         
    
    if(socket_bytes<0)
    {   
        DEBUG_LOG(WARN, SOCKET_WRITE_ERROR, "Socket error %s", strerror(errno));
        return 0;
    }
    return 1;
}

/* 
    Start the transmit loop. Send n_packets packets out
*/
void transmit_loop(network_test_t *nT)
{
    printf("Sending %d packets in %d seconds.\n", nT->n_packets, nT->seconds);
    printf("Packet size: %d bytes. Packet rate: %.0f packets/second. Kb/s: %d\n", nT->frame_size, nT->rate, nT->kbps);
    printf("Corresponding minimum latency: %.2fms\n", 2*(nT->loop_time/1e6));
    printf("Total data to be sent: %d kbytes\n", (nT->n_packets*nT->frame_size)/1024);
    transmit_open_socket(nT);
    struct timespec loop_start_time, actual_sleep_time;
    int period_ns = nT->loop_time;
    
    DEBUG_LOG(STATUS, TRANSMIT_LOOP_TIME, "Entering transmit loop: %fms sleep",  period_ns/1e6);
    clock_gettime(CLOCK_MONOTONIC, &loop_start_time);         
    
    nT->packet_number = nT->n_packets;
    int i=0;
    double start_t = current_time();
    while(nT->packet_number>=0)
    {

        if(transmit_packet(nT,0))
        {
            nT->packets[i].id = nT->packet_number;
            nT->packets[i].time = nT->timestamp;
            nT->packets[i].send_time = start_t + ((nT->n_packets-nT->packet_number) * (period_ns/1e9));
            i++;
        }
        else
        {
            nT->packets_lost++;
        }
        nT->packet_number--;
        // Increment timer then wait        	 
        loop_start_time.tv_nsec += period_ns;      
        tsnorm(&loop_start_time);
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &loop_start_time, &actual_sleep_time);
    }
  
    nT->n_packets_recorded = i;
    
    // flush with a bunch of invalid packets (length=0)
    for(i=0;i<200;i++)
    {
        nT->frame_size = 256;
        loop_start_time.tv_nsec = 10000000;      
        loop_start_time.tv_sec = 0;        
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_start_time, NULL);    
        transmit_packet(nT,1);
    }
    
    close(nT->sockfd);   
}


int packet_order(const void *v1, const void *v2)
{
    packet_info *a = (packet_info*)v1;
    packet_info *b = (packet_info*)v2;
    if(a->id>b->id)
        return 1;
    if(a->id==b->id)
        return 0;
    return -1;
     
}

int time_order(const void *v1, const void *v2)
{
    packet_info *a = (packet_info*)v1;
    packet_info *b = (packet_info*)v2;
    if(a->dt>b->dt)
        return 1;
    if(a->dt==b->dt)
        return 0;
    return -1;
     
}


/*
   Write out a logfile to the given directory.
   File name will be of format log_<frame_size>_<rate*1000>_<blocking>.csv
   Format of log file is:
   <id>, <expected_time>, <recv_time>, <send_time>
   All times as integer microseconds since the first <recv_time>
   ids will be *decreasing* (the id counts down, not up)
*/
void write_log(network_test_t *nT, char *output_dir, char *output_tag)
{
    int err = mkdir(output_dir,0777);
    if(err<0 && errno==EEXIST)
    {
        DEBUG_LOG(STATUS, OUTPUT_DIR_EXISTS, "Output directory already exists, using %s", output_dir);
    }
    if(err<0 && errno!=EEXIST)
    {
           DEBUG_LOG(ERROR, OUTPUT_DIR_EXISTS, "Error opening output log directory %s: %s", output_dir, strerror(errno));
           return;
    }
    char fname[2048];
    if(output_tag != NULL) {
        if(nT->is_recv)
            sprintf(fname, "%s/recv_%04d_%04d_%d_%s.csv", output_dir, nT->frame_size, (int)(nT->rate*1000), nT->blocking, output_tag);
        else
            sprintf(fname, "%s/transmit_%04d_%04d_%d_%s.csv", output_dir, nT->frame_size, (int)(nT->rate*1000), nT->blocking, output_tag);
    } else {
        if(nT->is_recv)
            sprintf(fname, "%s/recv_%04d_%04d_%d.csv", output_dir, nT->frame_size, (int)(nT->rate*1000), nT->blocking);
        else
            sprintf(fname, "%s/transmit_%04d_%04d_%d.csv", output_dir, nT->frame_size, (int)(nT->rate*1000), nT->blocking);
    }
    FILE *logfile = fopen(fname, "w");
    if(!logfile)
    {
           DEBUG_LOG(ERROR, CANNOT_OPEN_LOG_FILE, "Cannot open %s for writing: %s", fname, strerror(errno));
           return;
    }
    
   
    double start_t, delta_t, expected_t, time_t, send_time_t, start_send_t;
    int first_packet;
    first_packet = nT->packets[0].id;
    start_t = nT->packets[0].time;
    delta_t = 1.0/nT->rate;   
    start_send_t = nT->packets[0].send_time;
    int i;
    for(i=0;i<nT->n_packets_recorded;i++)
    {
         expected_t = (first_packet-nT->packets[i].id) * delta_t;         
         time_t = nT->packets[i].time - start_t;
         send_time_t = nT->packets[i].send_time - start_send_t;         
         fprintf(logfile, "%07d,%d,%d,%d\n", nT->packets[i].id, (int)(expected_t*1e6), (int)(time_t*1e6), (int)(send_time_t*1e6+1));
    }        
    fclose(logfile);               
}

void print_stats(network_test_t *nT, FILE *fd, int absolute)
{
   // compute time differences
   int i, first_packet;
   double start_t, delta_t, expected_t;
   first_packet = nT->packets[0].id;
   start_t = nT->packets[0].time;
   delta_t = 1.0/nT->rate;

   
   if(nT->is_recv)
    fprintf(fd, "------------------- Receiver -------------------\n");
   else
    fprintf(fd, "------------------ Transmitter -----------------\n");

    
   
   qsort(nT->packets, nT->n_packets_recorded, sizeof(*nT->packets), packet_order);
   
   // compute difference between actual and expected
   if(absolute)
   {
       for(i=0;i<nT->n_packets_recorded;i++)
       {
            expected_t = start_t + (first_packet-nT->packets[i].id) * delta_t;
            nT->packets[i].dt =  nT->packets[i].time - expected_t;
       }
       fprintf(fd,"* Absolute \n");
   }
   // compute difference between actual and sent (only relevant for receiver)
   else
   {
       for(i=0;i<nT->n_packets_recorded;i++)
       {
            nT->packets[i].dt =  nT->packets[i].time - nT->packets[i].send_time;
       }
       fprintf(fd, "* Relative \n"); 
   }
   

    
   
   // get mean and std
   double total=0, mean=0, var=0, std=0;
   for(i=0;i<nT->n_packets_recorded;i++)
   {
        total += nT->packets[i].dt; 
   }
   mean = total / nT->n_packets_recorded;
   
   // Remove the mean
   for(i=0;i<nT->n_packets_recorded;i++)
   {
        nT->packets[i].dt -= mean;
   }
   
   // compute bad packets
   int bad_packets = 0;
   for(i=0;i<nT->n_packets_recorded;i++)
   {
        if(nT->packets[i].dt>delta_t)
            bad_packets++;
   }
    
   // compute std. dev.
   for(i=0;i<nT->n_packets_recorded;i++)
   {
        var += (nT->packets[i].dt)*(nT->packets[i].dt);
   }
   var = var / nT->n_packets_recorded;
   std = sqrt(var);
   
      // compute runs of errors (e.g. from burst errors)
   int worst_missing_run=0, worst_late_run=0;
   int packet_id=first_packet;
   int late_ctr=0;
   int loss;
   for(i=1;i<nT->n_packets_recorded;i++)
   {        
        loss = (packet_id-1)-nT->packets[i].id;
        if(loss>worst_missing_run)
            worst_missing_run = loss;
    
        if(nT->packets[i].dt>delta_t)
            late_ctr++;
        else
            late_ctr = 0;
        if(late_ctr>worst_late_run)
            worst_late_run = late_ctr;        
        packet_id = nT->packets[i].id;
   }
   
     // sort by time
   qsort(nT->packets, nT->n_packets_recorded, sizeof(*nT->packets), time_order);
  
   int q25, q50, q75, q2, q98;
   q25 = nT->n_packets_recorded * 0.25;
   q50 = nT->n_packets_recorded * 0.5;
   q75 = nT->n_packets_recorded * 0.75;
   q2 = nT->n_packets_recorded * 0.025;
   q98 = nT->n_packets_recorded * 0.975;
   
   
   // print results
   fprintf(fd,"Expected packet interval:       % 5.3fms\n", 1000.0/nT->rate);
   fprintf(fd,"Packets recorded:                %d\n", nT->n_packets_recorded);
   fprintf(fd,"Lost packets:                    %d (%.2f%%)\n", nT->packets_lost, 100*nT->packets_lost/(double)nT->n_packets_recorded);
   fprintf(fd,"Duplicate packets:               %d (%.2f%%)\n", nT->duplicate_packets, 100*nT->duplicate_packets/(double)nT->n_packets_recorded);
   fprintf(fd,"Too late packets:                %d (%.2f%%)\n", bad_packets, 100*bad_packets/(double)nT->n_packets_recorded);
   fprintf(fd,"Worst late run:                  %d (%.2fms)\n", worst_late_run, worst_late_run * (1000.0/nT->rate));
   fprintf(fd,"Worst lost run:                  %d (%.2fms)\n", worst_missing_run, worst_missing_run * (1000.0/nT->rate));

   // save the lost and late packet stats to a file for easy use by shell scripts
   if(nT->is_recv && absolute) {
        char statsname[100];
        sprintf(statsname, "/tmp/ntest_result");
        FILE* f = fopen(statsname, "w");
        fprintf(f, "%.2f", (100 * nT->packets_lost/(double)nT->n_packets_recorded));
        fprintf(f, ",%.2f\n", (100 * bad_packets/(double)nT->n_packets_recorded));
        fclose(f);
    }
   
   if(!nT->is_recv)
    fprintf(fd,"Mean offset                     % 5.3fms\n", mean*1000);
   fprintf(fd,"Jitter std. dev.:               % 5.3fms\n", std*1000);
   fprintf(fd,"Interquartile jitter range:     % 5.3fms\n", (nT->packets[q75].dt*1000)- 
   (nT->packets[q25].dt*1000));
   fprintf(fd,"95%% jitter range:               % 5.3fms\n", (nT->packets[q98].dt*1000)- 
   (nT->packets[q2].dt*1000));
  
   //printf("Trimean jitter:                 % 5.3fms\n", (2*nT->packets[q50].time*1000+ 
   // nT->packets[q25].time*1000 + nT->packets[q75].time*1000)/4);
    
  fprintf(fd,"|% 5.3fms  % 5.3fms [% 5.3fms ( % 5.3fms ) % 5.3fms] % 5.3fms  % 5.3fms|\n",
   nT->packets[0].dt*1000, nT->packets[q2].dt*1000, nT->packets[q25].dt*1000, nT->packets[q50].dt*1000, 
   nT->packets[q75].dt*1000,nT->packets[q98].dt*1000, nT->packets[nT->n_packets_recorded-1].dt*1000);
   
   fprintf(fd,"\n");
   fflush(fd);   
}

void print_stats_without_ignore(network_test_t *nT, FILE *fd, int absolute)
{
   // compute time differences
   int i, first_packet;
   double start_t, delta_t, expected_t;
   int packets_for_stats = nT->n_packets_recorded - nT->ignore;

   first_packet = nT->packets[nT->ignore].id;
   start_t = nT->packets[nT->ignore].time;
   delta_t = 1.0/nT->rate;

   
   if(nT->is_recv)
    fprintf(fd, "------------------- Receiver -------------------\n");
   else
    fprintf(fd, "------------------ Transmitter -----------------\n");

    
   
   qsort(&(nT->packets[nT->ignore]), packets_for_stats, sizeof(*nT->packets), packet_order);
   
   // compute difference between actual and expected
   if(absolute)
   {
       for(i=nT->ignore;i<nT->n_packets_recorded;i++)
       {
            expected_t = start_t + (first_packet-nT->packets[i].id) * delta_t;
            nT->packets[i].dt =  nT->packets[i].time - expected_t;
       }
       fprintf(fd,"* Absolute \n");
   }
   // compute difference between actual and sent (only relevant for receiver)
   else
   {
       for(i=nT->ignore;i<nT->n_packets_recorded;i++)
       {
            nT->packets[i].dt =  nT->packets[i].time - nT->packets[i].send_time;
       }
       fprintf(fd, "* Relative \n"); 
   }
    
   
   // get mean and std
   double total=0, mean=0, var=0, std=0;
   for(i=nT->ignore;i<nT->n_packets_recorded;i++)
   {
        total += nT->packets[i].dt; 
   }
   mean = total / packets_for_stats;
   
   // Remove the mean
   for(i=nT->ignore;i<nT->n_packets_recorded;i++)
   {
        nT->packets[i].dt -= mean;
   }
   
   // compute bad packets
   int bad_packets = 0;
   for(i=nT->ignore;i<nT->n_packets_recorded;i++)
   {
        if(nT->packets[i].dt>delta_t)
            bad_packets++;
   }
    
   // compute std. dev.
   for(i=nT->ignore;i<nT->n_packets_recorded;i++)
   {
        var += (nT->packets[i].dt)*(nT->packets[i].dt);
   }
   var = var / packets_for_stats;
   std = sqrt(var);
   
      // compute runs of errors (e.g. from burst errors)
   int worst_missing_run=0, worst_late_run=0;
   int packet_id=first_packet;
   int late_ctr=0;
   int loss;
   for(i=nT->ignore+1;i<nT->n_packets_recorded;i++)
   {        
        loss = (packet_id-1)-nT->packets[i].id;
        if(loss>worst_missing_run)
            worst_missing_run = loss;
    
        if(nT->packets[i].dt>delta_t)
            late_ctr++;
        else
            late_ctr = 0;
        if(late_ctr>worst_late_run)
            worst_late_run = late_ctr;        
        packet_id = nT->packets[i].id;
   }
   
     // sort by time
   qsort(&(nT->packets[nT->ignore]), packets_for_stats, sizeof(*nT->packets), time_order);
  
   int q25, q50, q75, q2, q98;
   q25 = nT->ignore + packets_for_stats * 0.25;
   q50 = nT->ignore + packets_for_stats * 0.5;
   q75 = nT->ignore + packets_for_stats * 0.75;
   q2  = nT->ignore + packets_for_stats * 0.025;
   q98 = nT->ignore + packets_for_stats * 0.975;
   
   
   // print results
   fprintf(fd,"Expected packet interval:       % 5.3fms\n", 1000.0/nT->rate);
   fprintf(fd,"Packets recorded:                %d\n", packets_for_stats);
   fprintf(fd,"Lost packets:                    %d (%.2f%%)\n", nT->packets_lost, 100*nT->packets_lost/(double)packets_for_stats);
   fprintf(fd,"Duplicate packets:               %d (%.2f%%)\n", nT->duplicate_packets, 100*nT->duplicate_packets/(double)packets_for_stats);
   fprintf(fd,"Too late packets:                %d (%.2f%%)\n", bad_packets, 100*bad_packets/(double)packets_for_stats);
   fprintf(fd,"Worst late run:                  %d (%.2fms)\n", worst_late_run, worst_late_run * (1000.0/nT->rate));
   fprintf(fd,"Worst lost run:                  %d (%.2fms)\n", worst_missing_run, worst_missing_run * (1000.0/nT->rate));

   // save the lost and late packet stats to a file for easy use by shell scripts
   if(nT->is_recv && absolute) {
        char statsname[100];
        sprintf(statsname, "/tmp/ntest_result");
        FILE* f = fopen(statsname, "w");
        fprintf(f, "%.2f", (100 * nT->packets_lost/(double)packets_for_stats));
        fprintf(f, ",%.2f\n", (100 * bad_packets/(double)packets_for_stats));
        fclose(f);
    }
   
   if(!nT->is_recv)
    fprintf(fd,"Mean offset                     % 5.3fms\n", mean*1000);
   fprintf(fd,"Jitter std. dev.:               % 5.3fms\n", std*1000);
   fprintf(fd,"Interquartile jitter range:     % 5.3fms\n", (nT->packets[q75].dt*1000)- 
   (nT->packets[q25].dt*1000));
   fprintf(fd,"95%% jitter range:               % 5.3fms\n", (nT->packets[q98].dt*1000)- 
   (nT->packets[q2].dt*1000));
  
   //printf("Trimean jitter:                 % 5.3fms\n", (2*nT->packets[q50].time*1000+ 
   // nT->packets[q25].time*1000 + nT->packets[q75].time*1000)/4);
    
  fprintf(fd,"|% 5.3fms  % 5.3fms [% 5.3fms ( % 5.3fms ) % 5.3fms] % 5.3fms  % 5.3fms|\n",
   nT->packets[0].dt*1000, nT->packets[q2].dt*1000, nT->packets[q25].dt*1000, nT->packets[q50].dt*1000, 
   nT->packets[q75].dt*1000,nT->packets[q98].dt*1000, nT->packets[nT->n_packets_recorded-1].dt*1000);
   
   fprintf(fd,"\n");
   fflush(fd);   
}




network_test_t *global_nT;


int main(int argc, char **argv)
{

    network_test_t network_test;
    network_test_t *nT = &network_test;
    global_nT = nT;
   
    DEBUG_LOG(STATUS, COMPILE_DATE, "Compiled on " __DATE__ " " __TIME__);
    printf("Version: " __DATE__ " " __TIME__"\n");
    // seed the RNG
    struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);  
    srand(start_time.tv_sec ^ start_time.tv_nsec);    
    
    memset(nT, 0, sizeof(nT));
    set_defaults(nT);
 
    // register signal handler
    //signal(SIGINT, closed); 
    //signal(SIGTERM, closed); 
                           
	// Print date & time when built
    set_options(argc, argv, nT);    
       
    
    realtime_priority(50);
    if(nT->is_recv)
    {
        yaml_network_test(stdout, nT);               
        initialise_network_test(nT);
        //free(nT->packets);
        //nT->packets = (packet_info*)(malloc(sizeof(*nT->packets) * (1000 + nT->n_packets)));
        recv_loop(nT);
        if(nT->output_dir)
            write_log(nT, nT->output_dir, nT->output_tag);
        print_stats_without_ignore(nT, stdout, 0);
        print_stats_without_ignore(nT, stdout, 1);                        
		
		//print_stats(nT, stdout, 0);
        //print_stats(nT, stdout, 1);                        
        sync();                
    }
    else
    {
        initialise_network_test(nT);  
        yaml_network_test(stdout, nT);    
        //free(nT->packets);
        //nT->packets = (packet_info*)(malloc(sizeof(*nT->packets) * (1000 + nT->n_packets)));
        transmit_loop(nT);
        if(nT->output_dir)
            write_log(nT, nT->output_dir, nT->output_tag);
        print_stats_without_ignore(nT, stdout, 1);                        
        //print_stats(nT,stdout,1);
        sync();
	}
    
}
