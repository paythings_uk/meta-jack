/*
 * =========================
 * CONFIDENTIAL INFORMATION
 * (c) Ingenious Audio 2015
 * =========================
*/

#include "networkaudio-packetbuffer.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

/*  Simple deque of fixed size buffers. There is a pool of unused buffers, ready to be used
    and a set of active buffers. The pool can be expanded as required. 
    
    The two key operations are enque() and deque().
    
    enque() takes a buffer from the pool and inserts it at the head of the active
    and returns a pointer to the data block in that buffer; this can then be filled with data.
    
    deque() takes a buffer from the end of the active buffer and returns a pointer to
    the data inside, which can then be passed to the audio device. The buffer is returned to the
    pool.
  
*/           
SGLIB_DEFINE_DL_LIST_PROTOTYPES(pbslot, PB_COMPARATOR, prev, next);
SGLIB_DEFINE_DL_LIST_FUNCTIONS(pbslot, PB_COMPARATOR,  prev, next);

/* Create a buffer queue with a given size */
packetbuffer *init_packetbuffer(int buffer_size_samples, int audio_size_samples, int initial_pool)
{
    packetbuffer *buffer = (packetbuffer*)malloc(sizeof(packetbuffer));
    buffer->buffer_size = buffer_size_samples;
    buffer->audio_size = audio_size_samples;
    buffer->activeq = NULL;    
    buffer->poolq = NULL;  
    buffer->id_ctr = 0;
    expandpool_packetbuffer(buffer, initial_pool);
    return buffer;
}

/* Free a buffer queue structure */
void free_packetbuffer(packetbuffer *buffer)
{
    clear_packetbuffer(buffer);
    free(buffer);       
}

/* Clear and free the entire queue */
void clear_packetbuffer(packetbuffer *buffer)
{    
    while(sglib_pbslot_len(buffer->activeq)!=0)
    {
        free(buffer->activeq->buffer);
        sglib_pbslot_delete(&buffer->activeq, buffer->activeq);                
    }
        
    while(sglib_pbslot_len(buffer->poolq)!=0)
    {
        free(buffer->poolq->buffer);
        sglib_pbslot_delete(&buffer->poolq, buffer->poolq);               
    }
}

/* (re)set the size of each buffer in the queue, clearing the
   queue if needed */
void set_packetbuffer_size(packetbuffer *buffer, int size, int audio_size)
{
    int total_pool = sglib_pbslot_len(buffer->poolq) + sglib_pbslot_len(buffer->activeq);
    // If size changed, clear the whole queue
    if(size!=buffer->buffer_size)
    {
        buffer->buffer_size = size;
        buffer->audio_size = audio_size;
        clear_packetbuffer(buffer);        
        // make sure total number of buffers does not change
        expandpool_packetbuffer(buffer, total_pool); 
    }           
}

/* Expand the pool by n buffers */
void expandpool_packetbuffer(packetbuffer *buffer, int n)
{
    int i;
    for(i=0;i<n;i++)
    {
        addpool_packetbuffer(buffer);        
    }
}

/* Push a newly allocated buffer onto the pool */
void addpool_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry;
    
    entry = (pbslot*)malloc(sizeof(*entry));   
    entry->packet_number = 0;
    entry->buffer = (buffer_type*)calloc(buffer->buffer_size, SAMPLE_BYTES);       
    entry->extra_size = entry->used = 0;
    pbslot *first;
    first = sglib_pbslot_get_first(buffer->poolq);
    sglib_pbslot_add_before(&first, entry);    
    buffer->poolq = first;       
}


/* Return the last buffer pushed onto the active queue */
buffer_type *last_added_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry;
    entry = sglib_pbslot_get_first(buffer->activeq);
    if(entry)   
        return entry->buffer;    
    
    // active queue exhausted, try the head of the pool queue
    entry = sglib_pbslot_get_first(buffer->poolq);
    if(entry)
        return entry->buffer;
        
    // nothing at all in any queue -- should *never* happen
    return NULL;
}



/* Return the ix'th buffer that previously came off the active queue;
  that is the ix'th buffer of the pool queue, starting from the head.
  0 is the last buffer played; 1 is the 2nd last, and so on.
  Returns NULL if no such buffer.
  */
buffer_type *previous_packetbuffer(packetbuffer *buffer, int ix)
{
    int i;
    pbslot *entry;
    entry = sglib_pbslot_get_first(buffer->poolq);
    if(entry!=NULL)
    {
        for(i=0;i<ix;i++)
        {
            entry = entry->next;
            if(!entry) break;
        }
    }
    if(!entry) return NULL;
    return entry->buffer;
}

/* Return the length of the active queue */
int activeq_length_packetbuffer(packetbuffer *buffer)
{
    return sglib_pbslot_len(buffer->activeq);
}

/* Move a pool buffer into the head of the active queue. Return the buffer of that queue element. */
buffer_type *enque_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry;
    entry = sglib_pbslot_get_last(buffer->poolq);    
    if(!entry)
    {
        // Pool queue exhausted; add another element
        addpool_packetbuffer(buffer);
        entry = sglib_pbslot_get_last(buffer->poolq);    
    }        
    
    sglib_pbslot_delete(&buffer->poolq, entry);           
    pbslot *first;
    first = sglib_pbslot_get_first(buffer->activeq);    
    sglib_pbslot_add_before(&first, entry);        
    buffer->activeq = first;
    return entry->buffer;
}


// Enque a complete buffer if possible
int push_packetbuffer(packetbuffer *buffer, buffer_type *in, int in_length, unsigned packet_number, int has_extra, unsigned extra_size)
{
    pbslot *entry = sglib_pbslot_get_last(buffer->poolq);
    if(entry == NULL) 
    {
        // full queue
        return 0;
    }
        
    memcpy(entry->buffer, in, buffer->audio_size * SAMPLE_BYTES);
    entry->used = 1;
    entry->packet_number = packet_number;
    entry->extra_size = has_extra ? extra_size : 0;
    if(has_extra) 
        memcpy(entry->buffer + in_length, in + in_length, extra_size);
    enque_packetbuffer(buffer);              
    return 1;
}

// Deque a complete buffer
int pop_packetbuffer(packetbuffer *buffer, buffer_type *out, int out_length, unsigned *packet_number, void *extra, unsigned *extra_size)
{
    pbslot *entry = sglib_pbslot_get_last(buffer->activeq);
    if(entry == NULL) 
        return 0;

    memcpy(out, entry->buffer, buffer->audio_size * SAMPLE_BYTES);
    if(entry->extra_size > 0 && extra != NULL && extra_size != NULL && *extra_size >= entry->extra_size) 
    {
        memcpy(extra, entry->buffer + buffer->audio_size, entry->extra_size);
        *extra_size = entry->extra_size;
    }
    entry->used = entry->extra_size = 0;
    *packet_number = entry->packet_number;
    deque_packetbuffer(buffer);  
    return 1;
}

int drop_packetbuffer(packetbuffer *buffer) {
    pbslot *entry = sglib_pbslot_get_last(buffer->activeq);
    if(entry == NULL)
        return 0;

    deque_packetbuffer(buffer);
    return 1;
}

/* Return the next *complete* (i.e. ptr==0) buffer from the active queue,
   or NULL, if there is no such one. */
buffer_type *pop_complete_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry;
    entry = sglib_pbslot_get_last(buffer->activeq);
    if(entry && entry->used == 0)
        return deque_packetbuffer(buffer);
    else    
        return NULL; 
}

/* Pop the tail of the queue. Return the buffer it points to. */
buffer_type *deque_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry;
    buffer_type *buf;
    
    entry = sglib_pbslot_get_last(buffer->activeq);
    if(entry)
    {
        buf=entry->buffer;
        sglib_pbslot_delete(&buffer->activeq, entry);        
        pbslot *first;
        first = sglib_pbslot_get_first(buffer->poolq);    
        sglib_pbslot_add_before(&first, entry);        
        buffer->poolq = first;   
    }
    else    
    {        
        buf = NULL;
        // Active queue exhausted
    }        
    return buf;
}


/* Undo an enque */
void undo_enque_packetbuffer(packetbuffer *buffer)
{
    pbslot *entry = sglib_pbslot_get_first(buffer->activeq);
    if(entry)
    {
        sglib_pbslot_delete(&buffer->activeq, entry);        
        pbslot *last;
        last = sglib_pbslot_get_last(buffer->poolq);    
        sglib_pbslot_add_after(&last, entry);        
    }
 
}
/* Return the latency of the queue (i.e. length of the active q, in samples) */
unsigned latency_packetbuffer_periods(packetbuffer *buffer)
{
    pbslot *foo = sglib_pbslot_get_first(buffer->activeq);
    if(!foo) 
        return 0;

    unsigned latency = 0;
    while(foo) {
        latency++;
        foo = foo->next;
    }
    return latency;
}


/* Return the latency of the queue (i.e. length of the active q, in samples) */
unsigned latency_packetbuffer(packetbuffer *buffer)
{
    pbslot *foo = sglib_pbslot_get_first(buffer->activeq);
    if(!foo) 
        return 0;

    unsigned latency_samples = 0;
    while(foo) {
        latency_samples += buffer->audio_size;
        foo = foo->next;
    }
    return latency_samples;
}


/* Add a buffer to the queue, setting the interpolated state, the frame number
    and the gain of the buffer. */
void add_packetbuffer(packetbuffer *buffer, buffer_type *in, unsigned packet_number)
{
    enque_packetbuffer(buffer);
    pbslot *entry = sglib_pbslot_get_first(buffer->activeq);
    if(entry)
    {
        entry->packet_number = packet_number;
        memcpy(entry->buffer, in, buffer->audio_size * SAMPLE_BYTES);
        // TODO (this isn't complete but isn't used either)
    }
}

/*
    Stuff an uninitialised buffer into the queue 
*/
void stuff_packetbuffer(packetbuffer *buffer)
{
    enque_packetbuffer(buffer);
    pbslot *entry = sglib_pbslot_get_first(buffer->activeq);
    if(entry)
    {
        entry->packet_number = 0;
        entry->used = 0;
    }
}


/* Print the active q, including the gains, and whether or not packets were received. */
void print_activeq(packetbuffer *buffer)
{
    pbslot *l;
    printf("In >>> ");
    for(l=sglib_pbslot_get_first(buffer->activeq); l!=NULL; l=l->next) 
    {   
            printf("| %u | ", l->packet_number);            
    }
    printf(" >>> Out \n");
}
/* Print out the state of the queue (for debugging only) */
void print_packetbuffer(packetbuffer *buffer)
{
    printf("Buffer size: %d\n", buffer->buffer_size);
    printf("Pool elements: %d\n", sglib_pbslot_len(buffer->poolq));
    printf("Active elements: %d\n", sglib_pbslot_len(buffer->activeq));
    pbslot *l;
    
    printf(" just played -> ");
    for(l=sglib_pbslot_get_first(buffer->poolq); l!=NULL; l=l->next) printf("%4u %4d |", l->packet_number, l->used);
    printf(" <- to be read \n");
    printf(" just read   -> ");
    for(l=sglib_pbslot_get_first(buffer->activeq); l!=NULL; l=l->next) printf("%4u %4d |", l->packet_number, l->used);
    printf(" <- to be played \n");
   
}
/*
void test_partial_packetbuffer()
{
    packetbuffer *buffer;
    buffer_type buf[2048];
    q = init_packetbuffer(480, 12);
    enque_packetbuffer(buffer);
    enque_packetbuffer(buffer);
    enque_packetbuffer(buffer);
    enque_packetbuffer(buffer);    
    push_packetbuffer(q, buf, 1024);
    print_packetbuffer(buffer);
    push_packetbuffer(q, buf, 1024);
    print_packetbuffer(buffer);
    pop_packetbuffer(q, buf, 1024);
    print_packetbuffer(buffer);    
    pop_packetbuffer(q, buf, 1024);
    print_packetbuffer(buffer);       
    pop_packetbuffer(q, buf, 48);
    print_packetbuffer(buffer);       
}



void test_packetbuffer()
{
    packetbuffer *buffer;
    q = init_packetbuffer(256, 4);
    print_packetbuffer(buffer);
    enque_packetbuffer(buffer);
    print_packetbuffer(buffer);    
    enque_packetbuffer(buffer);
    print_packetbuffer(buffer);
    deque_packetbuffer(buffer);
    print_packetbuffer(buffer);
    deque_packetbuffer(buffer);
    print_packetbuffer(buffer);
    set_packetbuffer_size(q, 512);
    addpool_packetbuffer(buffer);
    addpool_packetbuffer(buffer);
    addpool_packetbuffer(buffer);    
    print_packetbuffer(buffer);
    enque_packetbuffer(buffer);
    print_packetbuffer(buffer);    
    enque_packetbuffer(buffer);
    print_packetbuffer(buffer);
    deque_packetbuffer(buffer);
    deque_packetbuffer(buffer);
    print_packetbuffer(buffer);
}*/

/*
int main(int argc, char** argv) {
    packetbuffer *buffer = init_packetbuffer(128, 8);
    buffer_type *testb = (buffer_type*)malloc(SAMPLE_BYTES * 128);
    for(int i=0;i<128;i++)
        testb[i] = i;

    for(int i=0;i<10;i++)
        printf("Push [%u] = %d\n", i, push_packetbuffer(buffer, testb, 128, i));

    printf("\nCURRENT STATE\n");
    print_packetbuffer(buffer);

    unsigned packet = -1;
    for(int i=0;i<10;i++) {
        int result = pop_packetbuffer(buffer, testb, 128, &packet);
        printf("Pop result: %d [%u]\n", result, packet);
    }

    printf("\nNEW STATE\n");
    print_packetbuffer(buffer);
}
*/
