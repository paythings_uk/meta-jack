
DESCRIPTION = "IGAUDIO applications"
SECTION = "examples"
#DEPENDS = ""
LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

FILES_${PN} = "${bindir}"

#SRC_URI = "file://ntest"
SRC_URI = "file://igaudio/"

S = "${WORKDIR}/igaudio"

DEPENDS = "alsa-utils"

EXTRA_OECMAKE=""

inherit pkgconfig cmake

#do_preconfigure(){
#	
#}

do_install(){
	install -d ${D}${bindir}
	install -m 0755 ${B}/src/ntest ${D}${bindir}
	install -m 0755 ${B}/src/igaudio ${D}${bindir}
}
