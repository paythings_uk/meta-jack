#!/bin/bash

# IGAUDIO initialisation script

MODE=$(mode.sh -g)

p2p_connect.sh
audio_codec_init.sh


if [ "x${MODE}" == "xMASTER" ]; then
  echo "Starting IGAUDIO - master"
  igaudio --master -p 12345 -i 192.168.2.2
   
else
  if [ "x${MODE}" == "xSLAVE" ]; then
    echo "Starting IGAUDIO - slave"
    igaudio -s -p 12345
    
  else
    echo "ERROR: set the device as master or slave"
    exit -1
  fi
    
fi
