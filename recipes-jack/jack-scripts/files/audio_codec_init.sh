#!/bin/bash

# DA7218 initialisation script

MODE=$(mode.sh -g)
DEVICE="hw:0"
INPUT_DEVICE=$1


MIXIN1_VOLUME_guitar="15"
MIC1_VOLUME_guitar="0"
HEADPHONE_VOLUME_guitar="38"
MIXIN1_VOLUME_mic="11"
MIC1_VOLUME_mic="5"
HEADPHONE_VOLUME_mic="30"

MIXIN1_VOLUME=${MIXIN1_VOLUME_guitar}
MIC1_VOLUME=${MIC1_VOLUME_guitar}
HEADPHONE_VOLUME=${HEADPHONE_VOLUME_guitar}

case "${INPUT_DEVICE}" in
    guitar)
        echo "INPUT DEVICE - guitar"
        MIXIN1_VOLUME=${MIXIN1_VOLUME_guitar}
        MIC1_VOLUME=${MIC1_VOLUME_guitar}
        HEADPHONE_VOLUME=${HEADPHONE_VOLUME_guitar}
        ;;

    mic)
        echo "INPUT DEVICE - mic"
        MIXIN1_VOLUME=${MIXIN1_VOLUME_mic}
        MIC1_VOLUME=${MIC1_VOLUME_mic}
        HEADPHONE_VOLUME=${HEADPHONE_VOLUME_mic}
        ;;
    
    *)
        echo "Usage: $0 {guitar|mic}"
        exit 1
esac

echo 77 > /sys/class/gpio/export 
echo out > /sys/class/gpio/gpio77/direction


if [ "x${MODE}" == "xMASTER" ]; then
  echo "AUDIO TRANSMITTER - master"
  
  # Configure Line IN (MIC enabled)
  echo 1 > /sys/class/gpio/gpio77/value
  
  amixer -D ${DEVICE} cset name='Mic1 Mux' 'Analog'
  amixer -D ${DEVICE} cset name='Mic1 Volume' ${MIC1_VOLUME}
  amixer -D ${DEVICE} cset name='Mic1 Switch' 1
  amixer -D ${DEVICE} cset name='In Filter1L Volume' 115
  amixer -D ${DEVICE} cset name='In Filter1L Switch' 1
  amixer -D ${DEVICE} cset name='DMix In Filter1L Out1 DAIL Volume' 25
  amixer -D ${DEVICE} cset name='Mixer DAI1L In Filter1L Switch' 1
  amixer -D ${DEVICE} cset name='Mixin1 Volume' ${MIXIN1_VOLUME}
  amixer -D ${DEVICE} cset name='Mixin1 Switch' 1

else
  if [ "x${MODE}" == "xSLAVE" ]; then
    echo "AUDIO RECEIVER - slave"
    
    # Configure Line OUT (L/R out enabled)
    echo 0 > /sys/class/gpio/gpio77/value
    
  else
    echo "ERROR: set the device as master or slave"
    exit -1
  fi
    
fi


# Setup default volumes for audio output
# We do this for both SLAVE (line out + headphones) and MASTER (headphones only)
amixer -D ${DEVICE} cset name='DMix In DAIL Out FilterL Volume' 20
amixer -D ${DEVICE} cset name='DMix In DAIR Out FilterR Volume' 20
amixer -D ${DEVICE} cset name='DMix In Filter1L Out1 DAIL Volume' 28
amixer -D ${DEVICE} cset name='DMix In Filter1L Out1 DAIR Volume' 28
amixer -D ${DEVICE} cset name='Out Filter Switch' 1
amixer -D ${DEVICE} cset name='Mixer Out FilterL DAIL Switch' 1
amixer -D ${DEVICE} cset name='Mixer Out FilterL DAIR Switch' 1
amixer -D ${DEVICE} cset name='Mixer Out FilterR DAIL Switch' 1
amixer -D ${DEVICE} cset name='Mixer Out FilterR DAIR Switch' 1
amixer -D ${DEVICE} cset name='ST Mixer Out FilterL Out FilterL Switch' 1
amixer -D ${DEVICE} cset name='ST Mixer Out FilterR Out FilterR Switch' 1
amixer -D ${DEVICE} cset name='Mixout Volume' 3
amixer -D ${DEVICE} cset name='Headphone Volume' ${HEADPHONE_VOLUME}
amixer -D ${DEVICE} cset name='Headphone Switch' 1

