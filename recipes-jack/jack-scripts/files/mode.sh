#!/bin/bash

gpio_export() 
{
  echo $1 > /sys/class/gpio/export
  echo $2 > /sys/class/gpio/gpio$1/direction
}

set_as_slave()
{
  echo "set as slave"
  echo SLAVE > /home/root/current_mode
  mkdir -p /etc/profile.d
  echo "export PS1='\u@\h[SLAVE]:\w\$ '" > /etc/profile.d/current_ps1.sh
  cp /etc/wpa_supplicant.conf.slave /etc/wpa_supplicant.conf
  cp /etc/network/interfaces.slave /etc/network/interfaces
  /etc/init.d/networking restart
}

set_as_master()
{
  echo "set as master"
  echo MASTER > /home/root/current_mode
  mkdir -p /etc/profile.d
  echo "export PS1='\u@\h[MASTER]:\w\$ '" > /etc/profile.d/current_ps1.sh
  cp /etc/wpa_supplicant.conf.master /etc/wpa_supplicant.conf
  cp /etc/network/interfaces.master /etc/network/interfaces
  /etc/init.d/networking restart
}

while getopts "gsm" arg; do
  case $arg in
    g)
      
      if [ -f /home/root/current_mode ]; then
        value=$(cat /home/root/current_mode)
        echo "$value"
        exit 0
      else
        echo "ERROR: device not initialised. Setting the board as SLAVE"
        set_as_slave
        exit -1
      fi
      ;;
    s)
      set_as_slave
      ;;
    m)
      set_as_master
      ;;
    *)
      echo "usage: $0 [-smg]"
      echo "  -s      set board in slave mode (receiver)"
      echo "  -m      set board in master mode (transmitter)"
      echo "  -g      get board mode"
      ;;
  esac
done
