#!/bin/bash


RUN_TIME=$1

#master
PARAMS=" -t 192.168.2.2 -r 1500 -f 136 -s ${RUN_TIME} --ignore 1000 --non-blocking"

echo "############################################"  > ntest_results_master.txt
echo "ntest ${PARAMS}"  >> ntest_results_master.txt
ntest ${PARAMS} >> ntest_results_master.txt

sleep 5

PARAMS=" -t 192.168.2.2 -r 750 -f 264 -s ${RUN_TIME} --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_master.txt
echo "ntest ${PARAMS}"  >> ntest_results_master.txt
ntest ${PARAMS} >> ntest_results_master.txt

sleep 5

PARAMS=" -t 192.168.2.2 -r 376 -f 520 -s ${RUN_TIME} --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_master.txt
echo "ntest ${PARAMS}"  >> ntest_results_master.txt
ntest ${PARAMS} >> ntest_results_master.txt

sleep 5

PARAMS=" -t 192.168.2.2 -r 188 -f 1032 -s ${RUN_TIME} --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_master.txt
echo "ntest ${PARAMS}"  >> ntest_results_master.txt
ntest ${PARAMS} >> ntest_results_master.txt
