#!/bin/bash


RUN_TIME=$1

#SLAVE
PARAMS=" -l -r 1500 -f 136 -s ${RUN_TIME} -o samp32.csv --non-blocking"

echo "############################################"  
echo "ntest ${PARAMS}"
ntest ${PARAMS}
