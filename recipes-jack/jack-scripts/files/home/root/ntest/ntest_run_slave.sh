#!/bin/bash


RUN_TIME=$1

#SLAVE
PARAMS=" -l -r 1500 -f 136 -s ${RUN_TIME} -o samp32.csv --ignore 1000 --non-blocking"

echo "############################################"  > ntest_results_slave.txt
echo "ntest ${PARAMS}"  >> ntest_results_slave.txt
ntest ${PARAMS} >> ntest_results_slave.txt

sleep 2

PARAMS=" -l -r 750 -f 264 -s ${RUN_TIME} -o samp64.csv --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_slave.txt
echo "ntest ${PARAMS}"  >> ntest_results_slave.txt
ntest ${PARAMS} >> ntest_results_slave.txt

sleep 2

PARAMS=" -l -r 376 -f 520 -s ${RUN_TIME} -o samp128.csv --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_slave.txt
echo "ntest ${PARAMS}"  >> ntest_results_slave.txt
ntest ${PARAMS} >> ntest_results_slave.txt

sleep 2

PARAMS=" -l -r 188 -f 1032 -s ${RUN_TIME} -o samp256.csv --ignore 1000 --non-blocking"

echo "############################################"  >> ntest_results_slave.txt
echo "ntest ${PARAMS}"  >> ntest_results_slave.txt
ntest ${PARAMS} >> ntest_results_slave.txt
