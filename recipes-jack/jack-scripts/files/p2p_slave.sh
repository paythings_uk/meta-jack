#!/bin/bash

# Wi-Fi direct script for P2P initialisation

#IFACE=wlp2s0
#

N_RETRIES=5

IFACE="wlan0"
IP_ADDRESS="192.168.2.1"
SLAVE_PARAMS="pbc join freq=5745"
DEVICE_MASTER_NAME="JACK_MASTER"

# Common path for all GPIO access
GPIO_PATH=/sys/class/gpio
# Pair switch
PAIR=119

# Connect LED 
CONNECT=124
# Assign names to LED states
ON="1"
OFF="0"

CONNECT_TO_DEVICE=${DEVICE_MASTER_NAME}
CONNECT_TO_MAC_ADDRESS="5a:d5:0a:ac:9f:a3"

MODE=$(mode.sh -g)

getGpioState()
{
  state=$(cat $GPIO_PATH/gpio$1/value)
}

# Utility function to change state of output
setGpioState()
{
  echo $2 > $GPIO_PATH/gpio$1/value
}

echo "P2P CLIENT DEVICE - slave: Waiting to pair"
RET_CODE=0
while [ "${RET_CODE}" -eq "0"  ]
do      
        getGpioState $PAIR
        if  [ $state -eq $ON ]; then
            echo "P2P CLIENT DEVICE - slave: Pairing"
            IP_ADDRESS="192.168.2.2"
            CONNECT_TO_DEVICE=${DEVICE_MASTER_NAME}
            wpa_cli -i ${IFACE} p2p_flush
            wpa_cli -i ${IFACE} p2p_find
            sleep 5
            P2P_DEVICES=$(wpa_cli -i ${IFACE} p2p_peers)
            for mac_address in ${P2P_DEVICES}; do
              CURRENT_DEVICE_NAME=$(wpa_cli -i ${IFACE} p2p_peer ${mac_address} | grep device_name | awk -F '=' '{print $2}')
              echo "DISCOVERED DEVICE: ${CURRENT_DEVICE_NAME}"
              if [ "x${CURRENT_DEVICE_NAME}" == "x${CONNECT_TO_DEVICE}" ]; then
                CONNECT_TO_MAC_ADDRESS=${mac_address}
                echo "${CONNECT_TO_DEVICE} MAC ADDRESS: ${CONNECT_TO_MAC_ADDRESS}"
                            wpa_cli -i ${IFACE} p2p_connect ${CONNECT_TO_MAC_ADDRESS} ${SLAVE_PARAMS}
                sleep 5
                wpa_cli -i p2p-${IFACE}-0 status
                sleep 5
                ifconfig p2p-${IFACE}-0 ${IP_ADDRESS} up
                setGpioState $CONNECT $ON
                RET_CODE="1"
                exit
              fi
           done
          sleep 4

          N_RETRIES=$[${N_RETRIES} - 1]

          if  [ "${N_RETRIES}" -le "0" ]; then
                echo "Reached MAX number of retries"
                echo "Cannot connect to ${CONNECT_TO_DEVICE}. Exiting...."
                killall wpa_supplicant
                ifconfig ${IFACE} down
                exit
          else 
              if  [ "${RET_CODE}" -eq "0" ]; then
                 echo "Retrying. Remaining attempts ${N_RETRIES} ..."
              fi
          fi
        fi
        sleep 1
done
