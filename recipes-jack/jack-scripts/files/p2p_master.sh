#!/bin/bash

# Wi-Fi direct script for P2P initialisation


IFACE="wlan0"
IP_ADDRESS="192.168.2.1"
MASTER_PARAMS="freq=5745"
DEVICE_MASTER_NAME="JACK_MASTER"

# Common path for all GPIO access
GPIO_PATH=/sys/class/gpio
# Connect LED 
CONNECT=124
# Assign names to LED states
ON="1"
OFF="0"

CONNECT_TO_DEVICE=${DEVICE_SLAVE_NAME}
CONNECT_TO_MAC_ADDRESS=""

# Utility function to change state of output
setGpioState()
{
  echo $2 > $GPIO_PATH/gpio$1/value
}

#killall wpa_supplicant
#ifconfig ${IFACE} down
#ifconfig ${IFACE} up
#wpa_supplicant -d -Dnl80211 -c /etc/wpa_supplicant.conf -i${IFACE} -B

wpa_cli -i ${IFACE} remove_network all

echo "P2P GO DEVICE - master"
wpa_cli -i ${IFACE} p2p_group_add ${MASTER_PARAMS}
sleep 1
wpa_cli -i p2p-${IFACE}-0 wps_pbc
sleep 1
wpa_cli -i p2p-${IFACE}-0 status
sleep 1
ifconfig p2p-${IFACE}-0 ${IP_ADDRESS} up

setGpioState $CONNECT $ON