#!/bin/bash

# Common path for all GPIO access
GPIO_PATH=/sys/class/gpio

# Current state of jack

JACK_STATE="0"

# GPIO Pins - Inputs
PAIR=119
TX=121
RX=120
CHARGE=114
VOL_UP=117
VOL_DOWN=118

# GPIO Pins - Outputs
POWER_RED=32
POWER_GREEN=33
CONNECT=124
CNTRL=113

# Assign names to LED states
ON="1"
OFF="0"

# Assign name to control states
MASTER="1"
SLAVE="0"

# Utility function to set a pin as an output
setGpioOutput()
{
  if [ ! -e $GPIO_PATH/gpio$1 ]; then
    echo "$1" > $GPIO_PATH/export
  fi
  echo "out" > $GPIO_PATH/gpio$1/direction
}

# Utility function to set a pin as an input
setGpioInput()
{
  if [ ! -e $GPIO_PATH/gpio$1 ]; then
    echo "$1" > $GPIO_PATH/export
  fi
  echo "in" > $GPIO_PATH/gpio$1/direction
}

getGpioState()
{
  state=$(cat $GPIO_PATH/gpio$1/value)
}

# Utility function to change state of output
setGpioState()
{
  echo $2 > $GPIO_PATH/gpio$1/value
}

setJackState()
{
    getGpioState $TX

    if [ $state -eq $ON ];
    then
      JACK_STATE="Master"
      set_as_master
    else
      getGpioState $RX
      if [ $state -eq $ON ];
      then
        JACK_STATE="Slave"
        set_as_slave
      else
        JACK_STATE="Disabled"
      fi
    fi
    echo "Jack is in $JACK_STATE mode"
}

set_as_slave()
{
  echo "set as slave"
  echo SLAVE > /home/root/current_mode
  mkdir -p /etc/profile.d
  echo "export PS1='\u@\h[SLAVE]:\w\$ '" > /etc/profile.d/current_ps1.sh
  cp /etc/wpa_supplicant.conf.slave /etc/wpa_supplicant.conf
  cp /etc/network/interfaces.slave /etc/network/interfaces
  /etc/init.d/networking restart
  setGpioState $CNTRL $SLAVE
  /usr/bin/p2p_slave.sh
}

set_as_master()
{
  echo "set as master"
  echo MASTER > /home/root/current_mode
  mkdir -p /etc/profile.d
  echo "export PS1='\u@\h[MASTER]:\w\$ '" > /etc/profile.d/current_ps1.sh
  cp /etc/wpa_supplicant.conf.master /etc/wpa_supplicant.conf
  cp /etc/network/interfaces.master /etc/network/interfaces
  /etc/init.d/networking restart
  setGpioState $CNTRL $MASTER
  /usr/bin/p2p_master.sh
}

# Set up GPIO pins so they can be used
setGpioInput $PAIR
setGpioInput $TX
setGpioInput $RX
setGpioInput $CHARGE
setGpioInput $VOL_UP
setGpioInput $VOL_DOWN

setGpioOutput $POWER_RED
setGpioOutput $POWER_GREEN
setGpioOutput $CONNECT
setGpioOutput $CNTRL

# Turn on power LED as we have started up

setGpioState $POWER_RED $ON

setJackState

/usr/bin/audio_codec_init.sh guitar

