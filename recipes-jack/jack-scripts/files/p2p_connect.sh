#!/bin/bash

# Wi-Fi direct script for P2P initialisation

#IFACE=wlp2s0
#

N_RETRIES=5

IFACE="wlan0"
SLEEP_VAL="7"
IP_ADDRESS="192.168.2.1"
CONNECT_PARAMS="pbc auth go_intent=15 freq=5745"
DEVICE_MASTER_NAME="JACK_MASTER"
DEVICE_SLAVE_NAME="JACK_SLAVE"

CONNECT_TO_DEVICE=${DEVICE_SLAVE_NAME}
CONNECT_TO_MAC_ADDRESS=""

MODE=$(mode.sh -g)

if [ "x${MODE}" == "xMASTER" ]; then
   echo "P2P GO DEVICE - master"
   SLEEP_VAL="0"
else
  if [ "x${MODE}" == "xSLAVE" ]; then
    echo "P2P CLIENT DEVICE - slave"
    IP_ADDRESS="192.168.2.2"
    CONNECT_PARAMS="pbc freq=5745 go_intent=0"
    CONNECT_TO_DEVICE=${DEVICE_MASTER_NAME}
  else
    echo "ERROR: set the device as master or slave"
    exit -1
  fi
    
fi

killall wpa_supplicant
ifconfig ${IFACE} down

ifconfig ${IFACE} up
wpa_supplicant -d -Dnl80211 -c /etc/wpa_supplicant.conf -i${IFACE} -B


wpa_cli -i ${IFACE} remove_network all


wpa_cli -i ${IFACE} p2p_flush

wpa_cli -i ${IFACE} p2p_find

sleep 2

RET_CODE=0

while [ "${RET_CODE}" -eq "0"  ]
do
        echo "Connecting to ${CONNECT_TO_DEVICE} ..."

        P2P_DEVICES=$(wpa_cli -i ${IFACE} p2p_peers)

        for mac_address in ${P2P_DEVICES}; do
           CURRENT_DEVICE_NAME=$(wpa_cli -i ${IFACE} p2p_peer ${mac_address} | grep device_name | awk -F '=' '{print $2}')
           echo "DISCOVERED DEVICE: ${CURRENT_DEVICE_NAME}"
           if [ "x${CURRENT_DEVICE_NAME}" == "x${CONNECT_TO_DEVICE}" ]; then
              CONNECT_TO_MAC_ADDRESS=${mac_address}
              echo "${CONNECT_TO_DEVICE} MAC ADDRESS: ${CONNECT_TO_MAC_ADDRESS}"
              wpa_cli -i ${IFACE} p2p_peer ${mac_address} 
              RET_CODE="1"
              break
           fi
        done
        sleep 4

        N_RETRIES=$[${N_RETRIES} - 1]

        if  [ "${N_RETRIES}" -le "0" ]; then
                echo "Reached MAX number of retries"
                echo "Cannot connect to ${CONNECT_TO_DEVICE}. Exiting...."
                killall wpa_supplicant
                ifconfig ${IFACE} down
                exit
        else 
           if  [ "${RET_CODE}" -eq "0" ]; then
             echo "Retrying. Remaining attempts ${N_RETRIES} ..."
           fi
        fi
done

sleep ${SLEEP_VAL}

wpa_cli -i ${IFACE} p2p_connect ${CONNECT_TO_MAC_ADDRESS} ${CONNECT_PARAMS}

sleep 15

wpa_cli -i p2p-${IFACE}-0 status

sleep 5

ifconfig p2p-${IFACE}-0 ${IP_ADDRESS} up

