
DESCRIPTION = "KALPA scripts"
SECTION = "examples"

LICENSE = "CLOSED"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

FILES_${PN} = "${bindir}"
FILES_${PN} += "${sysconfdir}/profile.d/"
FILES_${PN} += "${sysconfdir}/init.d/"
FILES_${PN} += "/home/root/*"

SRC_URI += "file://mode.sh"
SRC_URI += "file://p2p_master.sh"
SRC_URI += "file://p2p_slave.sh"
SRC_URI += "file://jack.sh"
SRC_URI += "file://wl_optimisations.sh"
SRC_URI += "file://igaudio_init.sh"
SRC_URI += "file://audio_codec_init.sh"
SRC_URI += "file://current_ps1.sh"
SRC_URI += "file://update_ps1.sh"

SRC_URI += "file://home/root/"


RDEPENDS_${PN} = "bash"

do_install(){
    install -d ${D}${bindir}
    install -d ${D}${sysconfdir}/profile.d/
    install -d ${D}${sysconfdir}/init.d/
    install -d ${D}/home/root/

    install -m 0755 ${WORKDIR}/mode.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/p2p_master.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/p2p_slave.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/jack.sh ${D}${sysconfdir}/init.d/
    install -m 0755 ${WORKDIR}/wl_optimisations.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/igaudio_init.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/audio_codec_init.sh ${D}${bindir}
    install -m 0755 ${WORKDIR}/current_ps1.sh ${D}${sysconfdir}/profile.d/
    install -m 0755 ${WORKDIR}/update_ps1.sh ${D}${sysconfdir}/profile.d/
    
    cp -r ${WORKDIR}/home/root/* ${D}/home/root/
}


